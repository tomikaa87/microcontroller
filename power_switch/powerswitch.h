#include <16F690.h>

#device *=16
#device adc=8

#FUSES NOWDT                     //No Watch Dog Timer
#FUSES INTRC_IO                  //Internal RC Osc, no CLKOUT
#FUSES NOPROTECT                 //Code not protected from reading
#FUSES NOBROWNOUT                //No brownout reset
#FUSES MCLR                      //Master Clear pin enabled
#FUSES NOCPD                     //No EE protection
#FUSES PUT                       //Power Up Timer
#FUSES IESO                      //Internal External Switch Over mode enabled
#FUSES FCMEN                     //Fail-safe clock monitor enabled
#FUSES RESERVED                  //Used to set the reserved FUSE bits

#use delay(clock=8000000)
#use rs232(baud=9600,parity=N,xmit=PIN_B7,rcv=PIN_B5,bits=7)

