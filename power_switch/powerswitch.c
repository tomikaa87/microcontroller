#include "X:\!Projects\Hardware\PIC\PowerSwitch_690\powerswitch.h"
#include <stdlib.h>
#include <string.h>

#ZERO_RAM

#define CONFIG_INIT_STATE  0x00
#define CONFIG_PROFILE_1   0x01
#define CONFIG_PROFILE_2   0x02
#define CONFIG_PROFILE_3   0x03
#define CONFIG_PROFILE_4   0x04
#define FIRMWARE_VERSION   0x04

typedef struct
{
    int r1: 1;
    int r2: 1;
    int r3: 1;
    int r4: 1;
    int r5: 1;
    int r6: 1;
    int unused1: 1;
    int unused2: 1;
}
RelayPins;

void set_relay_state(int relay_num, int1 state);

RelayPins relay_pins;
#byte relay_pins = 0x07    // map relay_pins to PORTC

#int_TIMER2
void  TIMER2_isr(void) 
{
    return;
}

#int_RDA
void RDA_isr(void)
{
    char op = getch();
    
    // Reset CPU
    if (op == 0x88)
        reset_cpu();
        
    // Get FW version
    if (op == 0x77)
        putc(FIRMWARE_VERSION);
        
    // Enable relay    
    if (op == 0x01)
    {
        char val = getch();
        
        if (val >= 0x00 && val <= 0x05)
            set_relay_state(val, 1);
    }
    
    // Disable relay
    if (op == 0x02)
    {
        char val = getch();
        
        if (val >= 0x00 && val <= 0x05)
            set_relay_state(val, 0);
    }
    
    // Enable all
    if (op == 0x03)
        output_c(0xFF);
        
    // Disable all
    if (op == 0x04)
        output_c(0x00);
        
    // Save current state to init state    
    if (op == 0x05)
        write_eeprom(CONFIG_INIT_STATE, (unsigned int)relay_pins);
        
    // Load init state
    if (op == 0x06)
    {
        unsigned int data;
        data = read_eeprom(CONFIG_INIT_STATE);
        relay_pins = (RelayPins)data;
    }
    
    // Set Profile 1 state
    if (op == 0x07)
    {
        unsigned int val = getch();
        write_eeprom(CONFIG_PROFILE_1, val);
    }
    
    // Get Profile 1 state
    if (op == 0x08)
        putc(read_eeprom(CONFIG_PROFILE_1));
    
    // Set Profile 2 state
    if (op == 0x09)
    {
        unsigned int val = getch();
        write_eeprom(CONFIG_PROFILE_2, val);
    }
    
    // Get Profile 2 state
    if (op == 0x0A)
        putc(read_eeprom(CONFIG_PROFILE_2));
        
    // Set Profile 3 state
    if (op == 0x0B)
    {
        unsigned int val = getch();
        write_eeprom(CONFIG_PROFILE_3, val);
    }
    
    // Get Profile 3 state
    if (op == 0x0C)
        putc(read_eeprom(CONFIG_PROFILE_3));
        
    // Set Profile 4 state
    if (op == 0x0D)
    {
        unsigned int val = getch();
        write_eeprom(CONFIG_PROFILE_4, val);
    }
    
    // Get Profile 4 state
    if (op == 0x0E)
        putc(read_eeprom(CONFIG_PROFILE_4)); 
        
    // Set current state
    if (op == 0x0F)
    {
        unsigned int data;
        data = getch();
        relay_pins = (RelayPins)data;
    }
    
    // Get current state
    if (op == 0x10)
    {
        unsigned int data;
        data = (unsigned int)relay_pins;
        putc(data);
    }
    
    // Get init state
    if (op == 0x11)
    {
        unsigned int data = read_eeprom(CONFIG_INIT_STATE);
        putc(data);
    }
    
    return;
}

void set_relay_state(int relay_num, int1 state)
{
    switch (relay_num)
    {
        case 0: relay_pins.r1 = state; break;
        case 1: relay_pins.r2 = state; break;
        case 2: relay_pins.r3 = state; break;
        case 3: relay_pins.r6 = state; break;
        case 4: relay_pins.r5 = state; break;
        case 5: relay_pins.r4 = state; break;
    }
}

void main()
{
    unsigned int data;
    
    setup_adc_ports(NO_ANALOGS|VSS_VDD);
    setup_adc(ADC_CLOCK_DIV_2);
    setup_spi(SPI_SS_DISABLED);
    setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
    setup_timer_1(T1_DISABLED);
    setup_timer_2(T2_DIV_BY_4,99,5);
    setup_ccp1(CCP_OFF);
    setup_comparator(NC_NC_NC_NC);// This device COMP currently not supported by the PICWizard
    enable_interrupts(INT_TIMER2);
    enable_interrupts(INT_RDA);
    enable_interrupts(GLOBAL);
    setup_oscillator(OSC_8MHZ);
    set_tris_c(0);
    output_c(0);
    
    data = read_eeprom(CONFIG_INIT_STATE);
    relay_pins = (RelayPins)data;
    
    while (1)
    {

    }

}
