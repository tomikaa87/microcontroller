#include <htc.h>

/*** Configure CPU ************************************************************/
__CONFIG(INTIO & WDTDIS);    // Internal Oscillator

/*** Global variables *********************************************************/

unsigned long timer_ticks = 0;

/*** Interrupt Service Routine  ***********************************************/

void interrupt isr()
{
    if (T0IF && T0IE)
    {
        timer_ticks++;
        TMR0 = 131;
        T0IF = 0;
    }
}

/*** Utility functions ********************************************************/

/**
 * Configures the CCP module to PWM
 */
void setup_pwm()
{
    T2CON = 0b00000100;
    PR2 = 0b00011110;
    CCP1CON = 0b00101100;
    CCPR1L = 0;
    TRISIO &= 0b11111011;
}

/**
 * Changes the duty cycle of the PWM module.
 */
void set_pwm_duty(unsigned char duty_cycle)
{
    unsigned char ccprl, dcb0, dcb1;

    // Calculate register values from duty cycle
    ccprl = duty_cycle >> 2;
    dcb1 = duty_cycle & 0x02 ? 1 : 0;
    dcb0 = duty_cycle & 0x01;

    // Change register values
    CCPR1L = ccprl;
    DC1B1 = dcb1;
    DC1B0 = dcb0;
}

/**
 * Delays \p ms milliseconds.
 */
void delay_ms(unsigned long ms)
{
    timer_ticks = 0;
    while (timer_ticks <= ms);
}

/**
 * Main function.
 */
void main()
{
    unsigned char i;

    // Setup internal osc, 8 Mhz
    OSCCON = 0b0110001;

    // Setup interrupts
    GIE = 1;
    T0IE = 1;

    // Setup timer 0
    T0CS = 0;
    T0SE = 0;
    PSA = 1;
    PS2 = 0;
    PS1 = 0;
    PS0 = 0;
    TMR0 = 131;

    setup_pwm();

    // Silence - 30s
    set_pwm_duty(0);
    delay_ms(30000);

    // Very light signal - 30s
    i = 0;
    while (i++ < 15)
    {
        // 1024 Hz
        PR2 = 0b01111001;
        set_pwm_duty(244);
        delay_ms(125);

        set_pwm_duty(0);
        delay_ms(1875);
    }

    // Light signal - 30s
    i = 0;
    while (i++ < 15)
    {
        // 1024 Hz
        PR2 = 0b01111001;
        set_pwm_duty(244);
        delay_ms(250);

        // 3072 Hz
        PR2 = 0b00101000;
        set_pwm_duty(81);
        delay_ms(125);

        set_pwm_duty(0);
        delay_ms(1625);
    }

    // Strong signal
    while (1)
    {
        // 1024 Hz
        PR2 = 0b01111001;
        set_pwm_duty(244);
        delay_ms(500);

        // 1024 Hz -> 2048 Hz
        for (i = 0b01111001; i > 0b00111100; i--)
        {
            PR2 = i;
            set_pwm_duty(183);
            delay_ms(20);
        }

        // 2048 Hz
        PR2 = 0b00111100;
        set_pwm_duty(122);
        delay_ms(500);

        // 2048 Hz -> 3072 Hz
        for (i = 0b00111100; i > 0b00101000; i--)
        {
            PR2 = i;
            set_pwm_duty(102);
            delay_ms(40);
        }

        // 3072 Hz
        PR2 = 0b00101000;
        set_pwm_duty(81);
        delay_ms(500);

        // 3072 Hz -> 4096 Hz
        for (i = 0b00101000; i > 0b00011110; i--)
        {
            PR2 = i;
            set_pwm_duty(74);
            delay_ms(80);
        }

        // 4096 Hz
        PR2 = 0b00011110;
        set_pwm_duty(66);
        delay_ms(500);

        // 4096 Hz -> 1024 Hz
        for (i = 0b00011110; i < 0b01111001; i++)
        {
            PR2 = i;
            set_pwm_duty(155);
            delay_ms(10);
        }
    }
}