#include <htc.h>
#include <stdio.h>

#include "t6963c.h"
#include "HardwareProfile.h"

__CONFIG(1, HSPLL);                             // Oscillator config: HS + PLLx4 (4 * 10 MHz)
__CONFIG(2, WDTDIS & BOREN & BORV28 & PWRTEN);  // Watchdog off, BOR on at 2.8V, Power-on timer on
__CONFIG(3, MCLREN & LPT1DIS & PBDIGITAL);      // MCLR off, LP timer 1 on, PortB digital IO
__CONFIG(4, XINSTDIS & LVPDIS);                 // Extended inst. off, Low-voltage prgm off
__CONFIG(5, UNPROTECT);                         // Memory protect off
__CONFIG(6, UNPROTECT);                         // Write protect off
__CONFIG(7, UNPROTECT);                         // Table read protect off

/**
 * Sends the given @p byte through the USART port.
 */
void putch(unsigned char byte)
{
    // Wait for TX to become ready
    while (!TRMT)
        continue;

    // Write the byte
    TXREG = byte;
}

/*
 * Sets PWM1 Duty Cycle
 */
void pwm1_set_duty(unsigned int duty)
{
    // Write 8 MSbits of duty to CCPR1L
	CCPR1L = (duty >> 2) & 0xFF;
	// Write 2 LSBits of duty to DC1B0 and DC1B1
	DC1B0 = (duty & 1) ? 1 : 0;
	DC1B1 = (duty & 2) ? 1 : 0;
} 

void main()
{
    unsigned int i, j;

    // Disable the comparator
    CMCON = 0x07;

    // Disable ADC
    ADCON0 = 0;
    ADCON1 = 0x0F;
    ADCON2 = 0;

    // Disable Vref
    CVRCON = 0;

    // Configure PWM
    PR2 = 0b01111100 ;
    T2CON = 0b00000111 ;
    CCPR1L = 0b11111111 ;
    CCP1CON = 0b00111100 ;

    // Configure PWM pin as digital output
    TRISC2 = 0;
    LATC2 = 1;

    // Async, 8 bit, 115200 bps
    TX9 = 0;
    SYNC = 0;
    BRGH = 1;
    SPBRG = 20;
    SPBRGH = 0;
    SPEN = 1;
    CREN = 1;
    SREN = 0;
    TXEN = 1;
    TRISC7 = 1;
    TRISC6 = 0;

    pwm1_set_duty(300);
    glcd_initialize(GLCD_FONT_8x8);
    glcd_clear();

    for (i = 0; i < 128; i++)
        for (j = 0; j < 128; j++)
            glcd_set_pixel(j, i, 1);

    for (i = 0; i < 500; i++)
        for (j = 0; j < 500; j++)
            __delay_us(1);

    glcd_clear();
    
    

    for (i = 0; i < 128; i++)
        for (j = 0; j < 128; j++)
            glcd_set_pixel_unbuf(j, i, 1);
    glcd_flush_puxel_buf();

/*
    for (i = 0; i < 20; i++)
        glcd_set_pixel(i, 0, 1);

    for (i = 0; i < 20; i++)
        glcd_set_pixel_unbuf(i, 1, 1);

    glcd_flush_puxel_buf();
*/

    while (1)
    {

    }
}