#ifndef __HARDWAREPROFILE_H
#define __HARDWAREPROFILE_H

// Fosc setting for delay macros
#ifndef _XTAL_FREQ
#define _XTAL_FREQ 40000000
#endif

// T6963C LCD pin configuration
#define GLCD_DATA                   LATB
#define GLCD_DATA_READ              PORTB
#define GLCD_DATA_TRIS              TRISB
#define GLCD_WR                     LATD0
#define GLCD_WR_TRIS                TRISD0
#define GLCD_RD                     LATD1
#define GLCD_RD_TRIS                TRISD1
#define GLCD_CE                     LATD2
#define GLCD_CE_TRIS                TRISD2
#define GLCD_CD                     LATD3
#define GLCD_CD_TRIS                TRISD3
#define GLCD_RST                    LATD4
#define GLCD_RST_TRIS               TRISD4
#define GLCD_FS                     LATD5
#define GLCD_FS_TRIS                TRISD5

// T6963C LCD status pin map
#define GLCD_STA0                   RB0
#define GLCD_STA1                   RB1
#define GLCD_STA2                   RB2
#define GLCD_STA3                   RB3
#define GLCD_STA4                   RB4
#define GLCD_STA5                   RB5
#define GLCD_STA6                   RB6
#define GLCD_STA7                   RB7

// EEPROM settings storage configuration
#define SETTINGS_STRUCT_EEPROM_ADDR 0x0000

// LCD backlight configuration
#define LCD_MAX_PWM_DUTY            800         // Maximum duty cycle value for LCD backlight PWM

#endif //__HARDWAREPROFILE_H
