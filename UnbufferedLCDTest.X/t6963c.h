/*;
 * Weather Station LCD Board
 * T6963C LCD Display Driver
 *
 * Author: Tamas Karpati
 * Date: 2010-09-21
 */

#ifndef __T6963C_H
#define __T6963C_H

// Color Constants
#define GLCD_COLOR_BLACK            1
#define GLCD_COLOR_WHITE            0

// Font Constants
#define GLCD_FONT_8x8               0
#define GLCD_FONT_6x8               1

void
glcd_write_data(unsigned char glcd_data);

void
glcd_write_command(unsigned char glcd_command);

void
glcd_write_command1(unsigned char glcd_msb,
                    unsigned char glcd_command);

void
glcd_write_command2(unsigned int glcd_data,
                    unsigned char glcd_command);

void
glcd_screen_peek(unsigned int address);

unsigned char
glcd_read_data();

unsigned char
glcd_read_data_inc();

void 
glcd_initialize(unsigned char font);

void 
glcd_putc(char c);

void 
glcd_puts(char *str);

void
glcd_clear();

void 
glcd_gotoxy(int x, 
            int y, 
            unsigned char text);
            
void 
glcd_set_pixel(unsigned char x, 
              unsigned char y, 
              unsigned char on);
              
void 
glcd_draw_rect(unsigned char x1, 
               unsigned char y1, 
               unsigned char x2, 
               unsigned char y2, 
               unsigned char fill, 
               unsigned char color);
               
void 
glcd_draw_image(unsigned char *image, 
                unsigned char width, 
                unsigned char height, 
                unsigned char x, 
                unsigned char y, 
                unsigned char invert);
                
void 
glcd_draw_bar(unsigned char value, 
              unsigned char max, 
              unsigned char x, 
              unsigned char y, 
              unsigned char width, 
              unsigned char height, 
              unsigned char invert);
              
void 
glcd_set_pixel_buf(unsigned char *buf, 
                   unsigned char x,
                   unsigned char y,
                   unsigned char on);
                   
unsigned char 
glcd_get_pixel_buf(unsigned char *buf, 
                   unsigned char x,
                   unsigned char y);
                   
void 
glcd_draw_image_buf(unsigned char *buf,
                    unsigned char *image,
                    unsigned char width, 
                    unsigned char height, 
                    unsigned char x, 
                    unsigned char y, 
                    unsigned char invert);
                    
void 
glcd_draw_rect_buf(unsigned char *buf,
                   unsigned char x1,
                   unsigned char y1, 
                   unsigned char x2, 
                   unsigned char y2, 
                   unsigned char fill, 
                   unsigned char color);
                   
void glcd_write_buf(unsigned char *buf,
                    unsigned unsigned int size,
                    unsigned char invert);

void
glcd_draw_line_buf(unsigned char *buf,
                   unsigned char x1,
                   unsigned char y1,
                   unsigned char x2,
                   unsigned char y2,
                   unsigned char color);
                   
void
glcd_draw_rect_inv_buf(unsigned char *buf,
                       unsigned char x1,
                       unsigned char y1, 
                       unsigned char x2, 
                       unsigned char y2,  
                       unsigned char color);

unsigned char
glcd_get_pixel_block(unsigned char x,
                     unsigned char y);

void glcd_set_pixel_unbuf(unsigned char x, unsigned char y, unsigned char color);
void glcd_flush_puxel_buf();
                   
#endif // __T6963C_H
