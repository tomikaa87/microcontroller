/******************************************************************************
 * T6963C GLCD driver library                                                 *
 *                                                                            *
 * version: 1.1                                                               *
 * by Tamas Karpati - 2010                                                    *
 * written in CCS-C                                                           *
 * modified for HI-TECH PIC C - 2010-09-21                                    *
 *                                                                            *
 * ************************************************************************** *
 * GLCD pin configuration:                                                    *
 *                                                                            *
 *  GLCD        PIC                                                           *
 *  /WR         PIN_D0                                                        *
 *  /RD         PIN_D1                                                        *
 *  /CE         PIN_D2                                                        *
 *  C/D         PIN_D3                                                        *
 *  DATA[0..7]  PORT B                                                        *
 *  /RESET      PIN_D4                                                        *
 *  FS          PIN_D5                                                        *
 *                                                                            *
 ******************************************************************************/

#include <htc.h>
#include <stdlib.h>

#include "t6963c.h"
#include "HardwareProfile.h"

/*** CONSTANTS ****************************************************************/

// GLCD timing
#define GLCD_COMMAND_TIME           2
#define GLCD_DATA_TIME              10
#define GLCD_READ_TIME              10
#define GLCD_BUSY_CHECK_TIME        2

// GLCD addresses
#define GLCD_GRAPH_HOME_ADDR        0x0000u
#define GLCD_GRAPH_AREA_ADDR        0x0010u
#define GLCD_TEXT_HOME_ADDR         0x0800u
#define GLCD_TEXT_AREA_ADDR         0x0010u
#define GLCD_CGRAM_HOME_ADDR        0x1000u
#define GLCD_GRAPH_HOME_ADDR_6x8    0x0000u
#define GLCD_GRAPH_AREA_ADDR_6x8    0x0016u
#define GLCD_TEXT_HOME_ADDR_6x8     0x0B00u
#define GLCD_TEXT_AREA_ADDR_6x8     0x0016u
#define GLCD_CGRAM_HOME_ADDR_6x8    0x1600u

// GLCD sizes
#define GLCD_MEM_SIZE               2048u
#define GLCD_MEM_SIZE_6x8           2816u

// Register set commands
#define GLCD_CURSOR_PTR_SET         0x21   //Cursor Pointer Set
#define GLCD_OFFSET_REG_SET         0x22   //Offset Register Set
#define GLCD_ADDR_PTR_SET           0x24   //Address Pointer Set

// Control Word Set commands
#define GLCD_TEXT_HOME_SET          0x40   //Text Home Address Set
#define GLCD_TEXT_AREA_SET          0x41   //Text Area Set
#define GLCD_GRAPH_HOME_SET         0x42   //Graphics Home address Set
#define GLCD_GRAPH_AREA_SET         0x43   //Graphics Area Set

// Mode Set commands (OR with CG rom commands)
#define GLCD_OR_MODE                0x80   //OR mode
#define GLCD_XOR_MODE               0x81   //XOR mode
#define GLCD_AND_MODE               0x83   //AND mode
#define GLCD_TEXT_ATTR_MODE         0x84   //Text Attribute mode
#define GLCD_INT_CG_MODE            0x80   //Internal CG ROM mode
#define GLCD_EXT_CG_MODE            0x88   //External CG ROM mode

// Display Mode commands (OR together required bits)
#define GLCD_DISPLAY_OFF            0x90
#define GLCD_BLINK_ON               0x91
#define GLCD_CURSOR_ON              0x92
#define GLCD_TEXT_ON                0x94
#define GLCD_GRAPH_ON               0x98
#define GLCD_TEXT_AND_GRAPH_ON      0x9C

// Cursor Pattern Select
#define GLCD_CURSOR_1LINE           0xA0
#define GLCD_CURSOR_2LINE           0xA1
#define GLCD_CURSOR_3LINE           0xA2
#define GLCD_CURSOR_4LINE           0xA3
#define GLCD_CURSOR_5LINE           0xA4
#define GLCD_CURSOR_6LINE           0xA5
#define GLCD_CURSOR_7LINE           0xA6
#define GLCD_CURSOR_8LINE           0xA7

// Data Auto Read/Write
#define GLCD_DATA_AUTO_WR           0xB0
#define GLCD_DATA_AUTO_RD           0xB1
#define GLCD_AUTO_DATA_RESET        0xB2

// Data Read/Write
#define GLCD_DATA_WR_INC            0xC0   //Data write and increment addr
#define GLCD_DATA_RD_INC            0xC1   //Data read and increment addr
#define GLCD_DATA_WR_DEC            0xC2   //Data write and decrement addr
#define GLCD_DATA_RD_DEC            0xC3   //Data read and decrement addr
#define GLCD_DATA_WR                0xC4   //Data write - no addr change
#define GLCD_DATA_RD                0xC5   //Data read - no addr change

// Screen Peek
#define GLCD_SCREEN_PEEK            0xE0

// Screen Copy
#define GLCD_SCREEN_COPY            0xE8

// Bit Set/Reset (OR with bit number 0-7)
#define GLCD_BIT_RESET              0xF0
#define GLCD_BIT_SET                0xF8

unsigned char glcd_font = GLCD_FONT_8x8;
unsigned int glcd_graph_home = GLCD_GRAPH_HOME_ADDR;
unsigned int glcd_graph_area = GLCD_GRAPH_AREA_ADDR;
unsigned int glcd_text_home = GLCD_TEXT_HOME_ADDR;
unsigned int glcd_text_area = GLCD_TEXT_AREA_ADDR;

/*** ROUTINES *****************************************************************/

/**
 * Checks if the LCD is doing something or not
 */
void 
glcd_busy_check()
{
   unsigned char sta0 = 0, sta1 = 0;
   
   GLCD_DATA_TRIS = 0xFF;
   
   do
   {
      GLCD_CD = 1;
      GLCD_RD = 0;
      GLCD_WR = 1;
      GLCD_CE = 0;
      __delay_us(GLCD_BUSY_CHECK_TIME);
      sta0 = GLCD_STA0;
      sta1 = GLCD_STA1;
      GLCD_CE = 1;
   } 
   while (!sta0 && !sta1);
}


/**
 * Writes a data byte to the LCD
 *
 * @param glcd_data The data to be written
 */
void 
glcd_write_data(unsigned char glcd_data)
{
   glcd_busy_check();
   GLCD_DATA_TRIS = 0x00;
   GLCD_DATA = glcd_data;
   GLCD_CD = 0;
   GLCD_WR = 0;
   GLCD_RD = 1;
   GLCD_CE = 0;
   __delay_us(GLCD_DATA_TIME);
   GLCD_CE = 1;
}


/**
 * Writes a command to the LCD
 *
 * @param glcd_command The command code
 */
void 
glcd_write_command(unsigned char glcd_command)
{
   glcd_busy_check();
   GLCD_DATA_TRIS = 0x00;
   GLCD_DATA = glcd_command;
   GLCD_CD = 1;
   GLCD_WR = 0;
   GLCD_RD = 1;
   GLCD_CE = 0;
   __delay_us(GLCD_COMMAND_TIME);
   GLCD_CE = 1;
}


/**
 * Writes a command with TWO (2x8 bit) parameters to the LCD
 *
 * @param glcd_data The data parameter for the command (MSB, LSB)
 * @param glcd_command The command code
 */
void 
glcd_write_command2(unsigned int glcd_data, 
                    unsigned char glcd_command)
{
   glcd_write_data(glcd_data & 0xFF);
   glcd_write_data(glcd_data >> 8);
   glcd_write_command(glcd_command);
}


/**
 * Writes a command with ONE parameter to the LCD
 *
 * @param glcd_msb The data parameter for the command (MSB)
 * @param glcd_command The command code
 */
void 
glcd_write_command1(unsigned char glcd_msb, 
                    unsigned char glcd_command)
{
   glcd_write_data(glcd_msb);
   glcd_write_command(glcd_command);
}

void
glcd_screen_peek(unsigned int address)
{
    glcd_write_command2(address, GLCD_ADDR_PTR_SET);
    glcd_busy_check();
    glcd_write_command(GLCD_SCREEN_PEEK);

    GLCD_DATA_TRIS = 0xFF;
    while (1)
    {
        GLCD_CD = 1;
        GLCD_RD = 0;
        GLCD_WR = 1;
        GLCD_CE = 0;
        __delay_us(GLCD_BUSY_CHECK_TIME);
        
        if (GLCD_STA6 == 0)
            break;

        GLCD_CE = 1;
    }
}

/**
 * Reads a byte from the LCD
 *
 * @return The read data byte
 */
unsigned char 
glcd_read_data()
{
   unsigned char data;
   
   glcd_write_command(GLCD_DATA_RD);
   glcd_busy_check();
   GLCD_CD = 0;
   GLCD_RD = 0;
   GLCD_WR = 1;
   GLCD_CE = 0;
   __delay_us(GLCD_READ_TIME);
   data = GLCD_DATA_READ;
   GLCD_CE = 1;
   
   return data;
}


/**
 * Reads a data byte from the LCD and increment the LCD's data pointer
 *
 * @return The read data byte
 */
unsigned char 
glcd_read_data_inc()
{
   unsigned char data;
   
   glcd_write_command(GLCD_DATA_RD_INC);
   glcd_busy_check();
   GLCD_CD = 0;
   GLCD_RD = 0;
   GLCD_WR = 1;
   GLCD_CE = 0;
   __delay_us(GLCD_READ_TIME);
   data = GLCD_DATA;
   GLCD_CE = 1;
   
   return data;
}


/**
 * Initializes the LCD and clears its RAM
 */
void 
glcd_initialize(unsigned char font)
{
    unsigned int i;
    
    // Setup control line pins as digital output
    GLCD_WR_TRIS = 0;
    GLCD_RD_TRIS = 0;
    GLCD_CE_TRIS = 0;
    GLCD_CD_TRIS = 0;
    GLCD_RST_TRIS = 0;
    GLCD_FS_TRIS = 0;
    
    // Reset GLCD controller
    GLCD_RST = 0;
    __delay_ms(5);
    GLCD_RST = 1;
    __delay_ms(5);
    
    // Pull down FS pin to select 8*8 font
    GLCD_FS = font;
    glcd_font = font;
    
    // Adjust addresses for font
    if (font)
    {
        // If font size is 6x8
        glcd_graph_home = GLCD_GRAPH_HOME_ADDR_6x8;
        glcd_graph_area = GLCD_GRAPH_AREA_ADDR_6x8;
        glcd_text_home = GLCD_TEXT_HOME_ADDR_6x8;
        glcd_text_area = GLCD_TEXT_AREA_ADDR_6x8;
    }
    
    // Set graphics home address
    glcd_write_command2(glcd_graph_home, GLCD_GRAPH_HOME_SET);
    
    // Set graphics area
    glcd_write_command2(glcd_graph_area, GLCD_GRAPH_AREA_SET);
    
    // Set text home
    glcd_write_command2(glcd_text_home, GLCD_TEXT_HOME_SET);
    
    // Set text area
    glcd_write_command2(glcd_text_area, GLCD_TEXT_AREA_SET);
    
    // Set CGRAM offset
    glcd_write_command2(0x0300u, GLCD_OFFSET_REG_SET);
    
    // Set OR mode
    glcd_write_command(GLCD_XOR_MODE);
    
    // Set address pointer
    glcd_write_command2(0x0000u, GLCD_ADDR_PTR_SET);
    
    // Set graphic mode on
    glcd_write_command(GLCD_GRAPH_ON);
    
    // Clear the whole memory
    for (i = 0; i < 0x1FFFu; i++)
        glcd_write_command1(0x00, GLCD_DATA_WR_INC);
         
    // Set text mode
    glcd_write_command(GLCD_TEXT_AND_GRAPH_ON);
          
    // Set address pointer
    glcd_write_command2(0x0000u, GLCD_ADDR_PTR_SET);     
}


/**
 * Writes a character on the LCD and increments the RAM pointer
 *
 * @param c The character
 */
void 
glcd_putc(char c)
{
   glcd_write_command1(c - 0x20, GLCD_DATA_WR_INC);
}

/**
 * Writes a string on the LCD and increments the RAM pointer
 *
 * @param str The character
 */
void 
glcd_puts(char *str)
{
    while (*str)
    {
        glcd_write_command1((*str) - 0x20, GLCD_DATA_WR_INC);
        str++;
    }    
}

/**
 * Clears the whole memory of the display
 */
void
glcd_clear()
{
    unsigned int i;
    
    // Set address pointer
    glcd_write_command2(0x0000u, GLCD_ADDR_PTR_SET);
    
    for (i = 0; i < 0x1FFFu; i++)
        glcd_write_command1(0x00, GLCD_DATA_WR_INC);       
}    


/**
 * Sets the "cursor" to the given coordinates
 *
 * @param x The X coordinate
 * @param y The Y coordinate
 * @param text Set it True to adjust text cursor, else it adjusts the graphics cursor
 */
void 
glcd_gotoxy(int x, 
            int y, 
            unsigned char text) 
{
   unsigned unsigned int location, home, line;

   if (!text) 
   {
      home = glcd_graph_home;
      line = glcd_graph_area;
   }
   else 
   {
      home = glcd_text_home;
      line = glcd_text_area;
   }

   location = home + (((unsigned int)y) * line) + x;
   glcd_write_command2(location, GLCD_ADDR_PTR_SET);
}

/**
 * Sets a pixel's state by the given coordinates
 *
 * @param x The X coordinate
 * @param y The Y coordinate
 * @param on True to turn the pixel ON, false to turn OFF
 */
void 
glcd_set_pixel(unsigned char x, 
              unsigned char y, 
              unsigned char on)
{
   unsigned int addr;
   
   addr = glcd_graph_home + (((unsigned int)y) * glcd_graph_area) + x / 8;
   glcd_write_command2(addr, GLCD_ADDR_PTR_SET);
   
   if (on)
   {
      glcd_write_command(GLCD_BIT_SET | (7 - (x % 8)));
   }
   else
   {
      glcd_write_command(GLCD_BIT_RESET | (7 - (x % 8)));  
   }
}


/**
 * Draws a rectangle on the LCD
 *
 * @param x1 The X coordinate of the top-left corner
 * @param y1 The Y coordinate of the top-left corner
 * @param x2 The X coordinate of the bottom-right corner
 * @param y2 The Y coordinate of the bottom-right corner
 * @param fill If true, the rectangle will be filled with the given color
 * @param color True, for black, false for white rectangle
 */
void 
glcd_draw_rect(unsigned char x1, 
               unsigned char y1, 
               unsigned char x2, 
               unsigned char y2, 
               unsigned char fill, 
               unsigned char color)
{
   unsigned char i, j;
   
   if (fill)
   {
      for (i = x1; i <= x2; i++)
         for (j = y1; j <= y2; j++)
            glcd_set_pixel(i, j, color);
   }
   else
   {
      for (i = x1; i <= x2; i++)
      {
         glcd_set_pixel(i, y1, color);
         glcd_set_pixel(i, y2, color);
      }
      for (i = y1; i <= y2; i++)
      {
         glcd_set_pixel(x1, i, color);
         glcd_set_pixel(x2, i, color);
      }
   }
}


/**
 * Draws a picture on the LCD
 *
 * @param image A pointer to the memory area that contains the picture data
 * @param width The width of the picture
 * @param height The height of the picture
 * @param x The X coordinate of the picture's top-left corner
 * @param y The Y coordinate of the picture's top-left corner
 * @param invert Inverts the picture's pixels if True
 */
void 
glcd_draw_image(unsigned char *image, 
                unsigned char width, 
                unsigned char height, 
                unsigned char x, 
                unsigned char y, 
                unsigned char invert)
{
   unsigned char i, j, k;
   
   for (i = 0; i < height; i++)
      for (j = 0; j < width; j++)
      {
         k = j + i * width;
         glcd_set_pixel(j + x, i + y, invert ? 1 - image[k] : image[k]);
      }
}


/**
 * Draws a progess bar
 *
 * @param value The value
 * @param max The maximum possible value
 * @param x The X coordinate of the top-left corner
 * @param y The Y coordinate of the top-left corner
 * @param width The width of the progress bar
 * @param height The height of the progress bar
 * @param invert Inverts the colors of the progress bar if true
 */
void 
glcd_draw_bar(unsigned char value, 
              unsigned char max, 
              unsigned char x, 
              unsigned char y, 
              unsigned char width, 
              unsigned char height, 
              unsigned char invert)
{
   unsigned char length;
   
   length = (unsigned char)((float)value/(float)max*(float)width);
   
   glcd_draw_rect(x, y, length+x-1, height+y-1, 1, 1 - invert);
   
   if (length < width - 1)
      glcd_draw_rect(x + length, 
                     y,
                     x + width - 1,
                     height + y - 1,
                     1, 
                     invert);
}


/**
 * Sets a pixel in the given buffer
 *
 * @param buf The pointer to the memory area the contains the buffer
 * @param x The X coordinate of the pixel
 * @param y The y coordinate of the pixel
 * @param on True to set the pixel, false to reset
 */
void 
glcd_set_pixel_buf(unsigned char *buf, 
                   unsigned char x,
                   unsigned char y,
                   unsigned char on)
{
   unsigned int graph_addr;
   
   graph_addr = (((unsigned int)y) * glcd_graph_area) + 
                (unsigned int)x / (unsigned int)8;
   
   if (on)
   {
      buf[graph_addr] = buf[graph_addr] | (1 << (7 - (x % 8)));
   }
   else
   {
      buf[graph_addr] = buf[graph_addr] & (0xFF - (1 << (7 - (x % 8))));
   }
}


/**
 * Draws a picture to the given buffer
 *
 * @param buf The pointer to the memory area the contains the buffer
 * @param image A pointer to the memory area that contains the picture data
 * @param width The width of the picture
 * @param height The height of the picture
 * @param x The X coordinate of the picture's top-left corner
 * @param y The Y coordinate of the picture's top-left corner
 * @param invert Inverts the picture's pixels if True
 */
void 
glcd_draw_image_buf(unsigned char *buf,
                    unsigned char *image,
                    unsigned char width, 
                    unsigned char height, 
                    unsigned char x, 
                    unsigned char y, 
                    unsigned char invert)
{
   unsigned char i, j, k;
   
   for (i = 0; i < height; i++)
      for (j = 0; j < width; j++)
      {
         k = j + i * width;
         glcd_set_pixel_buf(buf, j + x, i + y, invert ? 1 - image[k] : image[k]);
      }
}


/**
 * Draws a rectangle to the given buffer
 *
 * @param buf The pointer to the memory area the contains the buffer
 * @param x1 The X coordinate of the top-left corner
 * @param y1 The Y coordinate of the top-left corner
 * @param x2 The X coordinate of the bottom-right corner
 * @param y2 The Y coordinate of the bottom-right corner
 * @param fill If true, the rectangle will be filled with the given color
 * @param color True, for black, false for white rectangle
 */
void 
glcd_draw_rect_buf(unsigned char *buf,
                   unsigned char x1,
                   unsigned char y1, 
                   unsigned char x2, 
                   unsigned char y2, 
                   unsigned char fill, 
                   unsigned char color)
{
   unsigned char i, j;
   
   if (fill)
   {
      for (i = x1; i <= x2; i++)
         for (j = y1; j <= y2; j++)
            glcd_set_pixel_buf(buf, i, j, color);
   }
   else
   {
      for (i = x1; i <= x2; i++)
      {
         glcd_set_pixel_buf(buf, i, y1, color);
         glcd_set_pixel_buf(buf, i, y2, color);
      }
      for (i = y1; i <= y2; i++)
      {
         glcd_set_pixel_buf(buf, x1, i, color);
         glcd_set_pixel_buf(buf, x2, i, color);
      }
   }
}


/**
 * Writes the contents of the given buffer to the LCD
 *
 * @param buf The pointer to the memory area the contains the buffer
 * @param size The size of the buffer
 * @param invert If true, inverts the contents of the buffer
 */
void glcd_write_buf(unsigned char *buf,
                    unsigned unsigned int size,
                    unsigned char invert)
{
   unsigned int i;
   
   glcd_write_command2(0, GLCD_ADDR_PTR_SET);
   
   if (invert)
      for (i = 0; i < size; i++)
      {
         glcd_write_command1(~buf[i], GLCD_DATA_WR_INC);
      }   
   else
      for (i = 0; i < size; i++)
      {
         glcd_write_command1(buf[i], GLCD_DATA_WR_INC);
      }
}


/**
 * Draws a line using Bresenham's line algorithm to the given buffer
 *
 * @param buf The pointer to the memory area the contains the buffer
 * @param x1 The X coordinate of line's start point
 * @param y1 The Y coordinate of line's start point
 * @param x2 The X coordinate of line's end point
 * @param y2 The Y coordinate of line's end point
 * @param color The color of the line, 1 = black, 0 = white
 */
void
glcd_draw_line_buf(unsigned char *buf,
                   unsigned char x1,
                   unsigned char y1,
                   unsigned char x2,
                   unsigned char y2,
                   unsigned char color)
{
   unsigned char steep;
   unsigned char tmp, delta_x, delta_y, x, y;
   signed char y_step, error;
   
   steep = abs(y2 - y1) > abs(x2 - x1);
   
   if (steep)
   {
       tmp = x1;
       x1 = y1;
       y1 = tmp;
       
       tmp = x2;
       x2 = y2;
       y2 = tmp;
   }
   
   if (x1 > x2)
   {
       tmp = x1;
       x1 = x2;
       x2 = tmp;
       
       tmp = y1;
       y1 = y2;
       y2 = tmp;   
   }
   
   delta_x = x2 - x1;
   delta_y = abs(y2 - y1);
   error = delta_x / 2;
   y = y1;
   
   if (y1 < y2)
      y_step = 1;
   else
      y_step = -1;
      
   for (x = x1; x <= x2; x++)
   {
      if (steep)
         glcd_set_pixel_buf(buf, y, x, color);
      else
         glcd_set_pixel_buf(buf, x, y, color);
         
      error = error - delta_y;
      if (error < 0)
      {
         y = y + y_step;
         error = error + delta_x;
      }
   }
}  

/*
 * Draws a filled rect with XOR
 */
void
glcd_draw_rect_inv_buf(unsigned char *buf,
                       unsigned char x1,
                       unsigned char y1, 
                       unsigned char x2, 
                       unsigned char y2,  
                       unsigned char color)
{
    unsigned char x, y;
    
    for (x = x1; x <= x2; x++)
        for (y = y1; y <= y2; y++)
        {
            unsigned char pixel;
            pixel = glcd_get_pixel_buf(buf, x, y);
            glcd_set_pixel_buf(buf, x, y, pixel);       
        }    
}    

/*
 * Gets the "color" of given pixel
 */
unsigned char 
glcd_get_pixel_buf(unsigned char *buf, 
                   unsigned char x,
                   unsigned char y)
{
    unsigned int graph_addr;
   
    graph_addr = (((unsigned int)y) * glcd_graph_area) + (unsigned int)x / (unsigned int)8;
    
    return (buf[graph_addr] & (1 << (7 - (x % 8)))) > 0 ? 0 : 1;        
}

#include <stdio.h>

unsigned char
glcd_get_pixel_block(unsigned char x,
                     unsigned char y)
{
    unsigned int graph_addr;

    graph_addr = glcd_graph_home + (((unsigned int)y) * glcd_graph_area) + x;

    printf("T6963C: graph addr = 0x%04X\r\n", graph_addr);

    glcd_write_command2(graph_addr, GLCD_ADDR_PTR_SET);
    
    return glcd_read_data();
}

unsigned int last_block_addr = 0xFFFFu;
unsigned char block_data_buf = 0x00;

void glcd_set_pixel_unbuf(unsigned char x, unsigned char y, unsigned char color)
{
    unsigned int block_addr;

    // Calculate block address
    block_addr = (unsigned int)y * glcd_graph_area + (x / 8) + glcd_graph_home;
    //printf("%d;%d @ %04X", x, y, block_addr);

    if (last_block_addr == 0xFFFFu)
    {
        last_block_addr = block_addr;
        
        // Set LCD address pointer
        glcd_write_command2(block_addr, GLCD_ADDR_PTR_SET);

        // Read block data
        block_data_buf = glcd_read_data();
    }
    else if (block_addr != last_block_addr)
    {
        //printf(" W (%02X @ %04X)", block_data_buf, last_block_addr);
        glcd_write_command2(last_block_addr, GLCD_ADDR_PTR_SET);
        glcd_write_command1(block_data_buf, GLCD_DATA_WR);
        last_block_addr = block_addr;

        // Set LCD address pointer
        glcd_write_command2(block_addr, GLCD_ADDR_PTR_SET);

        // Read block data
        block_data_buf = glcd_read_data();
    }

    //printf("\r\n");

    // Set the pixel in the buffer
    if (color)
        block_data_buf |= (1 << (7 - (x % 8)));
    else
        block_data_buf &= (0xFF - (1 << (7 - (x % 8))));
}

void glcd_flush_puxel_buf()
{
    //printf("W (%02X @ %04X)\r\n", block_data_buf, last_block_addr);
    glcd_write_command2(last_block_addr, GLCD_ADDR_PTR_SET);
    glcd_write_command1(block_data_buf, GLCD_DATA_WR);
}