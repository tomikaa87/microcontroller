#ifndef SERIALLCDDRIVER_H
#define SERIALLCDDRIVER_H

#include <QObject>

#define _TTY_POSIX_
#include "qextserialport.h"

class SerialLCDDriver : public QObject
{
    Q_OBJECT
public:
    explicit SerialLCDDriver(QObject *parent = 0);
    ~SerialLCDDriver();
    int openPort(const QString &portName);
    void closePort();

    /*** LCD API ***/
    void lcdInit();
    void lcdClear();
    void lcdSetAddress(quint8 address);
    void lcdGotoXY(quint8 x, quint8 y);
    void lcdSetBacklight(quint8 level);
    void lcdShiftCursor(bool direction = false);
    void lcdShiftScreen(bool direction = false);
    void lcdSetEntryMode(quint8 mode);
    void lcdSetCursorMode(quint8 mode);
    void lcdDefineCustomChar(QByteArray charData, quint8 place);
    void lcdPutCustomChar(quint8 place);
    void lcdResetBoard();
    void lcdPutStr(const QString &str);
    void lcdDrawProgressBar(quint8 x,
                            quint8 y,
                            quint8 width,
                            quint8 value,
                            quint8 firstCustomCharPlace);
    void lcdDrawVerticalProgressBar(quint8 x,
                                    quint8 y,
                                    quint8 value,
                                    quint8 customCharPlace,
                                    bool invert);
    void lcdDrawProgressBarAdv(quint8 x,
                               quint8 y,
                               quint8 width,
                               quint8 value,
                               quint8 blockCharPlace,
                               quint8 innerCharPlace,
                               quint8 progressCharPlace,
                               quint8 endCharPlace);

private:
    QextSerialPort *serialPort;

    void delayMs(quint16 ms);

signals:

public slots:

};

#endif // SERIALLCDDRIVER_H
