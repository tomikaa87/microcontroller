#include "lcdcontroller.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDateTime>

const QString versionStr = "0.5";

LCDController::LCDController(QObject *parent) :
    QObject(parent)
{
    // Setup main timer
    mainTimer = new QTimer(this);
    mainTimer->setInterval(250);
    connect(mainTimer, SIGNAL(timeout()),
            this, SLOT(mainTimerTick()));

    // Setup system info timer
    sysInfoTimer = new QTimer(this);
    sysInfoTimer->setInterval(5000);
    connect(sysInfoTimer, SIGNAL(timeout()),
            this, SLOT(updateSystemData()));

    // Setup ethernet info timer
    ethInfoTimer = new QTimer(this);
    ethInfoTimer->setInterval(1000);
    connect(ethInfoTimer, SIGNAL(timeout()),
            this, SLOT(updateNetworkData()));

    // Setup backlight controller timer
    backlightControllerTimer = new QTimer(this);
    backlightControllerTimer->setInterval(5000);
    connect(backlightControllerTimer, SIGNAL(timeout()),
            this, SLOT(backlightControllerTimerTick()));

    // Setup LCD Driver
    lcdDrv = new SerialLCDDriver(this);
}

void LCDController::start()
{
    if (lcdDrv->openPort("/dev/ttyS0") == -1)
    {
        qCritical() << "Cannot open serial port!";
        qApp->exit(-1);
    }

    // Initialize
    lcdDrv->lcdInit();
    lcdScrUpdated = false;
    lcdScrState = LSS_BANNER;
    ticksRemain = ticks[0];
    updateSystemData();
    updateNetworkData();
    backlightControllerTimerTick();

    // Start timers
    mainTimer->start();
    backlightControllerTimer->start();
    sysInfoTimer->start();
}

void LCDController::mainTimerTick()
{
    if (sysInfo.upsState >= 2)
    {
        lcdScrState = LSS_UPS;
    }
    else
    {
        if (ticksRemain > 0)
            ticksRemain--;
        else
        {
            if (lcdScrState < LSS_LAST - 1)
                lcdScrState += 1;
            else
                lcdScrState = LSS_DATE_TIME;

            ticksRemain = ticks[lcdScrState];
            lcdScrUpdated = false;
            mainTimer->setInterval(250);
            lcdDrv->lcdClear();
            sysInfoTimer->start();

            QString log = QString("Screen: #%1").arg(lcdScrState);
            log.append(QString(" [length: %1 ticks]").arg(ticksRemain));
            qDebug(log.toAscii());
        }
    }

    switch (lcdScrState)
    {
    case LSS_BANNER:
        if (!lcdScrUpdated)
        {
            lcdDrawBannerScreen(1);
            lcdScrUpdated = true;
        }
        if (ticksRemain == 1)
        {
            lcdDrawEmptyScreen(17);
            lcdDrawDateTimeScreen(17);
            ticksRemain = 16;
            lcdScrState = LSS_BANNER_TRANSITION;
            mainTimer->setInterval(100);
        }
        break;

    case LSS_BANNER_TRANSITION:
        lcdDrv->lcdShiftScreen();
        break;

    case LSS_DATE_TIME:
        lcdDrawDateTimeScreen(1);
        if (ticksRemain == 1)
        {
            lcdDrawEmptyScreen(17);
            lcdDrawSystemTempScreen(17);
            ticksRemain = 16;
            lcdScrState = LSS_DATE_TIME_TRANSITION;
            sysInfoTimer->start();
            mainTimer->setInterval(100);
        }
        break;

    case LSS_DATE_TIME_TRANSITION:
        lcdDrv->lcdShiftScreen();
        break;

    case LSS_SYSTEM_TEMP:
        if (ticksRemain % 4 == 0)
            lcdScrUpdated = false;
        if (!lcdScrUpdated)
        {
            lcdDrawSystemTempScreen(1);
            lcdScrUpdated = true;
        }
        if (ticksRemain == 1)
        {
            lcdDrawEmptyScreen(17);
            lcdDrawNetworkScreen(17);
            ticksRemain = 16;
            lcdScrState = LSS_SYSTEM_TEMP_TRANSITION;
            sysInfoTimer->stop();
            mainTimer->setInterval(100);
        }
        break;

    case LSS_SYSTEM_TEMP_TRANSITION:
        lcdDrv->lcdShiftScreen();
        break;

    case LSS_NETWORK:
        if (!ethInfoTimer->isActive())
            ethInfoTimer->start();
        lcdDrawNetworkScreen(1);
        if (ticksRemain == 1)
        {
            lcdDrawEmptyScreen(17);
            lcdDrawUPSScreen(17);
            ticksRemain = 16;
            lcdScrState = LSS_NETWORK_TRANSITION;
            ethInfoTimer->stop();
            sysInfoTimer->start();
            mainTimer->setInterval(100);
        }
        break;

    case LSS_NETWORK_TRANSITION:
        lcdDrv->lcdShiftScreen();
        break;

    case LSS_UPS:
        lcdDrawUPSScreen(1);
        if (ticksRemain == 1)
        {
            lcdDrawEmptyScreen(17);
            lcdDrawDateTimeScreen(17);
            ticksRemain = 16;
            lcdScrState = LSS_UPS_TRANSITION;
            sysInfoTimer->stop();
            mainTimer->setInterval(100);
        }
        break;

    case LSS_UPS_TRANSITION:
        lcdDrv->lcdShiftScreen();
        break;

    }
}

/*** LCD Backlight control ****************************************************/

void LCDController::backlightControllerTimerTick()
{
    static enum
    {
        LCD_BACKLIGHT_NORMAL,
        LCD_BACKLIGHT_DIM,
        LCD_BACKLIGHT_UNKNOWN
    }
    lcdBacklightState = LCD_BACKLIGHT_UNKNOWN;

    if (lcdBacklightState == LCD_BACKLIGHT_UNKNOWN)
    {
        if (QTime::currentTime() >= QTime(22, 00) ||
            QTime::currentTime() < QTime(8, 00))
        {
            lcdBacklightState = LCD_BACKLIGHT_DIM;
            lcdDrv->lcdSetBacklight(1);
            qDebug() << "BACKLIGHT: dim LCD backlight";
        }
        else
        {
            lcdBacklightState = LCD_BACKLIGHT_NORMAL;
            lcdDrv->lcdSetBacklight(50);
            qDebug() << "BACKLIGHT: set LCD backlight to normal";
        }
    }

    // Dim the LCD backlight at 22:00
    if (lcdBacklightState == LCD_BACKLIGHT_NORMAL)
    {
        if (QTime::currentTime() >= QTime(22, 00) ||
            QTime::currentTime() < QTime(8, 00))
        {
            lcdBacklightState = LCD_BACKLIGHT_DIM;
            lcdDrv->lcdSetBacklight(1);
            qDebug() << "BACKLIGHT: dim LCD backlight";
        }
    }

    // Set LCD backlight back to normal at 08:00
    if (lcdBacklightState == LCD_BACKLIGHT_DIM)
    {
        if (QTime::currentTime() >= QTime(8, 00) &&
            QTime::currentTime() < QTime(22, 00))
        {
            lcdBacklightState = LCD_BACKLIGHT_NORMAL;
            lcdDrv->lcdSetBacklight(50);
            qDebug() << "BACKLIGHT: set LCD backlight to normal";
        }
    }
}

/*** System information functions *********************************************/

void LCDController::updateSystemData()
{
    char str[50];

    // Get system temperature 1
    FILE *p = popen("cat /sys/class/hwmon/hwmon1/device/temp1_input", "r");
    fgets(str, 8, p);
    pclose(p);
    sysInfo.sysTemp1 = (float)atoi(str) / 1000.0;

    // Get system temperature 2
    p = popen("cat /sys/class/hwmon/hwmon1/device/temp2_input", "r");
    fgets(str, 8, p);
    pclose(p);
    sysInfo.sysTemp2 = (float)atoi(str) / 1000.0;

    // Get system temperature 3
    p = popen("cat /sys/class/hwmon/hwmon1/device/temp3_input", "r");
    fgets(str, 8, p);
    pclose(p);
    sysInfo.sysTemp3 = (float)atoi(str) / 1000.0;

    // Get UPS load percentage
    system("apcaccess > /var/tmp/__lcd_ctrl_ups_data");
    p = popen("cat /var/tmp/__lcd_ctrl_ups_data | grep -e '^LOADPCT' | cut -c 12-14", "r");
    fgets(str, 4, p);
    pclose(p);
    sysInfo.upsLoadPct = (quint8)atoi(str);
    sysInfo.upsLoadWatts = (quint8)(sysInfo.upsLoadPct / 100.0 * 300.0);

    // Get UPS battery percentage
    p = popen("cat /var/tmp/__lcd_ctrl_ups_data | grep -e '^BCHARGE' | cut -c 12-14", "r");
    fgets(str, 4, p);
    pclose(p);
    sysInfo.upsBatteryPct = (quint8)atoi(str);

    // Get UPS battery time left
    p = popen("cat /var/tmp/__lcd_ctrl_ups_data | grep -e '^TIMELEFT' | cut -c 12-16", "r");
    fgets(str, 6, p);
    pclose(p);
    sysInfo.upsMinutesLeft = (float)atof(str);

    // Get UPS battery voltage
    p = popen("cat /var/tmp/__lcd_ctrl_ups_data | grep -e '^BATTV' | cut -c 12-16", "r");
    fgets(str, 6, p);
    pclose(p);
    sysInfo.upsBatteryVoltage = (float)atof(str);

    // Get UPS status
    p = popen("cat /var/tmp/__lcd_ctrl_ups_data | grep -e '^STATUS' | cut -c 12-", "r");
    fgets(str, 50, p);
    pclose(p);
    if (strncmp(str, "ONLINE", 6) == 0)
        sysInfo.upsState = 1;
    else if (strncmp(str, "ONBATT", 6) == 0)
        sysInfo.upsState = 2;
    else if (strncmp(str, "SHUTTING DOWN", 13) == 0)
        sysInfo.upsState = 3;
    else
        sysInfo.upsState = 0;

    /*qDebug() << "UPS: loadpct =" << sysInfo.upsLoadPct;
    qDebug() << "UPS: load watts =" << sysInfo.upsLoadWatts;
    qDebug() << "UPS: battery level =" << sysInfo.upsBatteryPct;
    qDebug() << "UPS: battery minutes left =" << sysInfo.upsMinutesLeft;
    qDebug() << "UPS: battery voltage =" << sysInfo.upsBatteryVoltage;
    qDebug() << "UPS: state =" << sysInfo.upsState;*/

}

void LCDController::updateNetworkData()
{
    char str[16];

    system("bwm-ng -o csv -c 1 | grep eth1 > /var/tmp/__lcd_ctrl_bw_data");

    // Get Ethernet upload speed
    FILE *p = popen("cat /var/tmp/__lcd_ctrl_bw_data | cut -d ';' -f 3", "r");
    fgets(str, 16, p);
    pclose(p);
    sysInfo.ethUploadBps = atof(str);

    // Get Ethernet upload speed
    p = popen("cat /var/tmp/__lcd_ctrl_bw_data | cut -d ';' -f 4", "r");
    fgets(str, 16, p);
    pclose(p);
    sysInfo.ethDownloadBps = atof(str);
}

/*** LCD Screen functions *****************************************************/

void LCDController::lcdDrawEmptyScreen(quint8 x)
{
    lcdDrv->lcdGotoXY(x, 1);
    lcdDrv->lcdPutStr("                ");
    lcdDrv->lcdGotoXY(x, 2);
    lcdDrv->lcdPutStr("                ");
}

void LCDController::lcdDrawBannerScreen(quint8 x)
{
    lcdDrv->lcdClear();
    lcdDrv->lcdGotoXY(x, 1);
    lcdDrv->lcdPutStr(" Qt LCD Driver  ");
    lcdDrv->lcdGotoXY(x, 2);
    lcdDrv->lcdPutStr(QString("  Version: %1").arg(versionStr));
}

void LCDController::lcdDrawDateTimeScreen(quint8 x)
{
    lcdDrv->lcdGotoXY(x, 1);
    lcdDrv->lcdPutStr(QDateTime::currentDateTime()
                      .toString("yyyy.MM.dd.  ddd"));
    lcdDrv->lcdGotoXY(x, 2);
    lcdDrv->lcdPutStr(QDateTime::currentDateTime()
                      .toString("    hh:mm ss"));
}

void LCDController::lcdDrawSystemTempScreen(quint8 x)
{
    lcdDrv->lcdGotoXY(x, 1);

    lcdDrv->lcdPutStr(QString("CPU/NBR/SYS Temp"));
                      //;
    lcdDrv->lcdGotoXY(x, 2);
    lcdDrv->lcdPutStr(QString("%1 /%2 /%3 �C")
                      .arg(sysInfo.sysTemp1, 2, 'f', 0, QChar('0'))
                      .arg(sysInfo.sysTemp2, 2, 'f', 0, QChar('0'))
                      .arg(sysInfo.sysTemp3, 2, 'f', 0, QChar('0')));
}

void LCDController::lcdDrawNetworkScreen(quint8 x)
{
    lcdDrv->lcdDrawProgressBarAdv(x + 10, 1, 6,
                       (quint8)(sysInfo.ethUploadBps / 3072000.0 * 100),
                       0, 1, 2, 3);

    lcdDrv->lcdDrawProgressBarAdv(x + 10, 2, 6,
                       (quint8)(sysInfo.ethDownloadBps / 5120000.0 * 100),
                       0, 1, 4, 3);

    lcdDrv->lcdGotoXY(x, 1);
    lcdDrv->lcdPutStr("\xC5 "); // ^
    lcdDrv->lcdPutStr(QString("%1 K")
                      .arg(sysInfo.ethUploadBps / 1024, 5, 'f', 0));
    lcdDrv->lcdGotoXY(x, 2);
    lcdDrv->lcdPutStr("\xC6 "); // v
    lcdDrv->lcdPutStr(QString("%1 K")
                      .arg(sysInfo.ethDownloadBps / 1024, 5, 'f', 0));
}

void LCDController::lcdDrawUPSScreen(quint8 x)
{
    lcdDrv->lcdGotoXY(x, 1);
    lcdDrv->lcdPutStr(QString("UPS  %1W  ")
                      .arg(sysInfo.upsLoadWatts, 3));

    lcdDrv->lcdDrawProgressBarAdv(x + 11, 1, 5, sysInfo.upsLoadPct, 0, 1, 2, 3);
    lcdDrv->lcdGotoXY(x, 2);
    lcdDrv->lcdPutStr(QString("%1% %2m ")
                      .arg(sysInfo.upsBatteryPct, 3)
                      .arg(sysInfo.upsMinutesLeft, 2, 'f', 1));
    lcdDrv->lcdDrawProgressBarAdv(x + 11, 2, 5, sysInfo.upsBatteryPct, 0, 1, 4, 3);

    /*const char upsOnlineIconData[8] = {10, 10, 31, 31, 31, 14, 4, 4};
    const char upsOnBattIconData[8] = {14, 31, 17, 19, 23, 31, 31, 31};
    const char upsFailureIconData[8] = {0, 4, 14, 21, 17, 14, 0, 0};
    const char upsUnknownStatusIconData[8] = {0, 14, 17, 1, 2, 4, 0, 4};

    QByteArray upsOnlineIcon(upsOnlineIconData, 8);
    QByteArray upsOnBattIcon(upsOnBattIconData, 8);
    QByteArray upsFailureIcon(upsFailureIconData, 8);
    QByteArray upsUnknownStatusIcon(upsUnknownStatusIconData, 8);



    lcdDrv->lcdGotoXY(x, 5);
    switch (sysInfo.upsState)
    {
    case 1: lcdDrv->lcdDefineCustomChar(QByteArray(upsOnlineIcon), 7); break;
    case 2: lcdDrv->lcdDefineCustomChar(QByteArray(upsOnBattIcon), 7); break;
    case 3: lcdDrv->lcdDefineCustomChar(QByteArray(upsFailureIcon), 7); break;
    default:
        lcdDrv->lcdDefineCustomChar(QByteArray(upsUnknownStatusIcon), 7);
    }
    lcdDrv->lcdPutCustomChar(7);*/
}
