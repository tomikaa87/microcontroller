#include "seriallcddriver.h"

#include <ctime>
#include <QDebug>

SerialLCDDriver::SerialLCDDriver(QObject *parent) :
    QObject(parent)
{
    serialPort = new QextSerialPort();
}

SerialLCDDriver::~SerialLCDDriver()
{
    if (serialPort->isOpen())
        serialPort->close();
    delete serialPort;
}

/*** Public methods ***********************************************************/

int SerialLCDDriver::openPort(const QString &portName)
{
    // Setup serial port
    serialPort->setPortName(portName);
    serialPort->setBaudRate(BAUD9600);
    serialPort->setDataBits(DATA_8);
    serialPort->setFlowControl(FLOW_OFF);
    serialPort->setStopBits(STOP_1);
    serialPort->setParity(PAR_NONE);

    if (!serialPort->open(QIODevice::ReadWrite))
    {
        return -1;
    }

    QByteArray data;
    data.append(0x1F);
    data.append(0xF1);
    data.append(0x03);
    serialPort->write(data);

    serialPort->setBaudRate(BAUD38400);

    delayMs(5000);

    return 0;
}

void SerialLCDDriver::closePort()
{
    serialPort->close();
}

/*** LCD API ******************************************************************/

void SerialLCDDriver::lcdInit()
{
    QByteArray data;

    data.append(0x1F);
    data.append(0x01);

    serialPort->write(data);

    //qDebug() << "LCDDRV: Init";
}

void SerialLCDDriver::lcdClear()
{
    QByteArray data;

    data.append(0x1F);
    data.append(0x02);

    serialPort->write(data);
    //serialPort->flush();

    //qDebug() << "LCDDRV: Clear";
}

void SerialLCDDriver::lcdSetAddress(quint8 address)
{
    QByteArray data;

    data.append(0x1F);
    data.append(0x03);
    data.append(address);

    serialPort->write(data);

    //qDebug() << "LCDDRV: SetAddress [" << address << "]";
}

void SerialLCDDriver::lcdGotoXY(quint8 x, quint8 y)
{
    QByteArray data;

    data.append(0x1F);
    data.append(0x04);
    data.append(x - 1);
    data.append(y - 1);

    serialPort->write(data);

    //qDebug() << "LCDDRV: GotoXY [" << x << y << "]";
}

void SerialLCDDriver::lcdSetBacklight(quint8 level)
{
    QByteArray data;

    data.append(0x1F);
    data.append(0xF0);
    data.append(level);

    serialPort->write(data);

    //qDebug() << "LCDDRV: SetBacklight [" << level << "]";
}

void SerialLCDDriver::lcdShiftCursor(bool direction)
{
    QByteArray data;

    data.append(0x1F);

    if (direction)
        data.append(0x06);
    else
        data.append(0x05);

    serialPort->write(data);

    //qDebug() << "LCDDRV: ShiftCursor";
}

void SerialLCDDriver::lcdShiftScreen(bool direction)
{
    QByteArray data;

    data.append(0x1F);

    if (direction)
        data.append(0x08);
    else
        data.append(0x07);

    serialPort->write(data);

    //qDebug() << "LCDDRV: ShiftScreen";
}

void SerialLCDDriver::lcdSetEntryMode(quint8 mode)
{
    QByteArray data;

    data.append(0x1F);
    data.append(0x09);
    data.append(mode);

    serialPort->write(data);

    //qDebug() << "LCDDRV: SetEntryMode";
}

void SerialLCDDriver::lcdSetCursorMode(quint8 mode)
{
    QByteArray data;

    data.append(0x1F);
    data.append(0x0A);
    data.append(mode);

    serialPort->write(data);

    //qDebug() << "LCDDRV: SetCursorMode";
}

void SerialLCDDriver::lcdDefineCustomChar(QByteArray charData, quint8 place)
{
    if (place > 7 || charData.length() > 8)
        return;

    QByteArray data;

    data.append(0x1F);
    data.append(0x0B);
    data.append(place);
    data.append(charData);

    serialPort->write(data);

    //qDebug() << "LCDDRV: DefineCustomChar";
}

void SerialLCDDriver::lcdPutCustomChar(quint8 place)
{
    QByteArray data;

    data.append(place);

    serialPort->write(data);

    //qDebug() << "LCDDRV: PutCustomChar";
}

void SerialLCDDriver::lcdResetBoard()
{
    //const char data[] = {0x1F, 0xFF};
    QByteArray data;

    data.append(0x1F);
    data.append(0xFF);

    serialPort->write(data);
    //serialPort->flush();

    //qDebug() << "LCDDRV: ResetBoard";
}

void SerialLCDDriver::lcdPutStr(const QString &str)
{
    serialPort->write(str.toAscii());
}

void SerialLCDDriver::lcdDrawProgressBar(quint8 x,
                                         quint8 y,
                                         quint8 width,
                                         quint8 value,
                                         quint8 firstCustomCharPlace)
{
    QByteArray data;

    data.append(0x1F);
    data.append(0x0C);
    data.append(x - 1);
    data.append(y - 1);
    data.append(width);
    data.append(value);
    data.append(firstCustomCharPlace);

    serialPort->write(data);

    //qDebug() << "LCDDRV: DrawProgressBar";
}

void SerialLCDDriver::lcdDrawVerticalProgressBar(quint8 x,
                                                 quint8 y,
                                                 quint8 value,
                                                 quint8 customCharPlace,
                                                 bool invert)
{
    QByteArray data;

    data.append(0x1F);
    data.append(0x0D);
    data.append(x - 1);
    data.append(y - 1);
    data.append(value);
    data.append(customCharPlace);
    data.append(invert ? 0x01 : 0x00);

    serialPort->write(data);

    //qDebug() << "LCDDRV: DrawVerticalProgressBar";
}

void SerialLCDDriver::lcdDrawProgressBarAdv(quint8 x,
                                            quint8 y,
                                            quint8 width,
                                            quint8 value,
                                            quint8 blockCharPlace,
                                            quint8 innerCharPlace,
                                            quint8 progressCharPlace,
                                            quint8 endCharPlace)
{
    QByteArray data;

    data.append(0x1F);
    data.append(0x0E);
    data.append(x - 1);
    data.append(y - 1);
    data.append(width);
    data.append(value);
    data.append(blockCharPlace);
    data.append(innerCharPlace);
    data.append(progressCharPlace);
    data.append(endCharPlace);

    serialPort->write(data);

    //qDebug() << "LCDDRV: DrawProgressBarAdv";
}

/*** Private methods **********************************************************/

void SerialLCDDriver::delayMs(quint16 ms)
{
    clock_t start_time;

    start_time = clock();
    while((clock() - start_time) < ms);
}
