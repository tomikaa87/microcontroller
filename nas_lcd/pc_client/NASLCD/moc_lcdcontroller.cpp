/****************************************************************************
** Meta object code from reading C++ file 'lcdcontroller.h'
**
** Created: Sat Oct 30 22:51:54 2010
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "lcdcontroller.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'lcdcontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LCDController[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      31,   14,   14,   14, 0x08,
      50,   14,   14,   14, 0x08,
      70,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_LCDController[] = {
    "LCDController\0\0mainTimerTick()\0"
    "updateSystemData()\0updateNetworkData()\0"
    "backlightControllerTimerTick()\0"
};

const QMetaObject LCDController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LCDController,
      qt_meta_data_LCDController, 0 }
};

const QMetaObject *LCDController::metaObject() const
{
    return &staticMetaObject;
}

void *LCDController::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LCDController))
        return static_cast<void*>(const_cast< LCDController*>(this));
    return QObject::qt_metacast(_clname);
}

int LCDController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: mainTimerTick(); break;
        case 1: updateSystemData(); break;
        case 2: updateNetworkData(); break;
        case 3: backlightControllerTimerTick(); break;
        }
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
