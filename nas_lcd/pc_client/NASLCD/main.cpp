#include <QtCore/QCoreApplication>

#include "lcdcontroller.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    LCDController lcdCtrl;

    lcdCtrl.start();

    return a.exec();
}
