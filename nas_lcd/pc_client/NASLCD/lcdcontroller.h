#ifndef LCDCONTROLLER_H
#define LCDCONTROLLER_H

#include <QObject>
#include <QTimer>

#include "seriallcddriver.h"

typedef enum
{
    LSS_BANNER = 0,
    LSS_BANNER_TRANSITION,
    LSS_DATE_TIME,
    LSS_DATE_TIME_TRANSITION,
    LSS_SYSTEM_TEMP,
    LSS_SYSTEM_TEMP_TRANSITION,
    LSS_NETWORK,
    LSS_NETWORK_TRANSITION,
    LSS_UPS,
    LSS_UPS_TRANSITION,
    LSS_LAST
}
LCDScreensState;

const unsigned int ticks[LSS_LAST] = {
    20, 0, 40, 0, 40, 0, 80, 0, 40, 0
};

typedef struct
{
    float sysTemp1;
    float sysTemp2;
    float sysTemp3;

    float ethUploadBps;
    float ethDownloadBps;

    quint8 upsLoadPct;
    quint8 upsLoadWatts;
    quint8 upsBatteryPct;
    float upsBatteryVoltage;
    float upsMinutesLeft;
    quint8 upsState;
}
SystemInfo;

class LCDController : public QObject
{
    Q_OBJECT
public:
    explicit LCDController(QObject *parent = 0);
    void start();

private:
    SerialLCDDriver *lcdDrv;
    QTimer *mainTimer;
    QTimer *sysInfoTimer;
    QTimer *ethInfoTimer;
    QTimer *backlightControllerTimer;
    unsigned int lcdScrState;
    unsigned int ticksRemain;
    bool lcdScrUpdated;
    SystemInfo sysInfo;

    void lcdDrawEmptyScreen(quint8 x);
    void lcdDrawBannerScreen(quint8 x);
    void lcdDrawDateTimeScreen(quint8 x);
    void lcdDrawSystemTempScreen(quint8 x);
    void lcdDrawNetworkScreen(quint8 x);
    void lcdDrawUPSScreen(quint8 x);

private slots:
    void mainTimerTick();
    void updateSystemData();
    void updateNetworkData();
    void backlightControllerTimerTick();

signals:

public slots:

};

#endif // LCDCONTROLLER_H
