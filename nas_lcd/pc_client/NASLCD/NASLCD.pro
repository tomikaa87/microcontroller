#-------------------------------------------------
#
# Project created by QtCreator 2010-10-20T20:59:50
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = NASLCD
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../qextserialport

LIBS += ../qextserialport/build/libqextserialport.so.1.0.0

SOURCES += main.cpp \
    lcdcontroller.cpp \
    seriallcddriver.cpp

HEADERS += \
    lcdcontroller.h \
    ../qextserialport/qextserialport.h \
    seriallcddriver.h
