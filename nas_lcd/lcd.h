#ifndef __LCD_H
#define __LCD_H

#define LCD_ENTRY_MODE_DEC_NOSHIFT     0x04
#define LCD_ENTRY_MODE_DEC_SHIFT       0x05
#define LCD_ENTRY_MODE_INC_NOSHIFT     0x06
#define LCD_ENTRY_MODE_INC_SHIFT       0x07

#define LCD_CURSOR_MODE_OFF            0x0C
#define LCD_CURSOR_MODE_BLINK          0x0D
#define LCD_CURSOR_MODE_UNDERLINE      0x0E

#define LCD_SHIFT_LEFT                 0x00
#define LCD_SHIFT_RIGHT                0x01


void lcd_init();
unsigned char lcd_read_byte();
void lcd_send_byte(unsigned char cmd,
                   unsigned char data);
void lcd_putc(unsigned char c);
void lcd_gotoxy(unsigned char x,
                unsigned char y);
void lcd_clear();
void lcd_puts(char *s);
unsigned char lcd_getc(unsigned char x,
                       unsigned char y);
void lcd_shift_screen(unsigned char direction);
void lcd_shift_cursor(unsigned char direction);
void lcd_set_entry_mode(unsigned char mode);
void lcd_set_cursor_mode(unsigned char mode);
void lcd_define_custom_char(unsigned char place,
                            unsigned char *char_bytes);
                                                          
void lcd_draw_progress_bar(unsigned char x,
                           unsigned char y,
                           unsigned char width,
                           unsigned char value,
                           unsigned char first_custom_char_place);
                           
void lcd_draw_vertical_progress_bar(unsigned char x,
                                    unsigned char y,
                                    unsigned char value,
                                    unsigned char custom_char_place,
                                    unsigned char invert);
                                    
void lcd_draw_progress_bar_adv(unsigned char x,
                               unsigned char y,
                               unsigned char width,
                               unsigned char value,
                               unsigned char block_char_place,
                               unsigned char inner_char_place,
                               unsigned char progress_char_place,
                               unsigned char end_char_place);

#endif // __LCD_H
