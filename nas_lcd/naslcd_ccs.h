#include <16F690.h>
#device adc=8
#device *=16

#FUSES WDT                       //No Watch Dog Timer
#FUSES HS                        //High speed Osc (> 4mhz for PCM/PCH) (>10mhz for PCD)
#FUSES NOPROTECT                 //Code not protected from reading
#FUSES BROWNOUT                  //Reset when brownout detected
#FUSES MCLR                      //Master Clear pin enabled
#FUSES NOCPD                     //No EE protection
#FUSES PUT                       //Power Up Timer
#FUSES IESO                      //Internal External Switch Over mode enabled
#FUSES FCMEN                     //Fail-safe clock monitor enabled

#use delay(clock=20000000)
#use rs232(baud=9600,parity=N,xmit=PIN_B7,rcv=PIN_B5,bits=8)

