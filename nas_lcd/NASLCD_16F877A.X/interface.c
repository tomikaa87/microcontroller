/*
 * File:   interface.c
 * Author: Tam�s
 *
 * Created on 2014. m�rcius 23., 0:48
 */

#include "config.h"
#include "interface.h"
#include "lcd.h"
#include "serial.h"

#include <stdint.h>
#include <xc.h>

//------------------------------------------------------------------------------
// Utility functions
//------------------------------------------------------------------------------

// LCD backlight
void pwm1_set_duty_cycle(uint8_t dc)
{
    CCPR1L = dc >> 2;
    CCP1CON = 0b1100 | ((dc & 0b11) << 5);
}

// LCD contrast
void pwm2_set_duty_cycle(uint8_t dc)
{
    // Inversion for contrast control
    dc = ~dc;

    CCPR2L = dc >> 2;
    CCP2CON = 0b1100 | ((dc & 0b11) << 5);
}

//------------------------------------------------------------------------------
// Globals
//------------------------------------------------------------------------------

static uint8_t last_pwm_duty = 128;

//------------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------------

void interface_init()
{
    lcd_init();
    pwm1_set_duty_cycle(128);
    pwm2_set_duty_cycle(128);
}

void interface_task()
{
    // if the received byte is not a command start byte, continue
    // or if it is a >= 0x20 byte, put it on the LCD
    // or if it is a <= 0x07 byte, put the custom char on the LCD
    char c = serial_read_from_buf();
    if (c != 0x1F)
    {
        if (c >= 0x20 || c <= 0x07)
            lcd_putc(c);

        return;
    }

    // Get command code
    uint8_t command = serial_read_from_buf();

    // Process received command
    switch (command)
    {
        case 0x01:      // INIT LCD
            lcd_init();
            break;

        case 0x02:      // CLEAR LCD
            lcd_clear();
            break;

        case 0x03:      // SET LCD ADDRESS
        {
            unsigned char address;

            address = serial_read_from_buf();
            lcd_send_byte(0, 0x80 | address);

            break;
        }

        case 0x04:      // GOTO XY
        {
            unsigned char x, y;

            x = serial_read_from_buf();
            y = serial_read_from_buf();

            lcd_gotoxy(x, y);

            break;
        }

        case 0x05:      // LCD CURSOR SHIFT LEFT
            lcd_shift_cursor(LCD_SHIFT_CURSOR_LEFT);
            break;

        case 0x06:      // LCD CURSOR SHIFT RIGHT
            lcd_shift_cursor(LCD_SHIFT_CURSOR_RIGHT);
            break;

        case 0x07:      // LCD SCREEN SHIFT LEFT
            lcd_shift_screen(LCD_SHIFT_SCREEN_LEFT);
            break;

        case 0x08:      // LCD SCREEN SHIFT RIGHT
            lcd_shift_screen(LCD_SHIFT_SCREEN_RIGHT);
            break;

        case 0x09:      // SET LCD ENTRY MODE
        {
            unsigned char mode;

            mode = serial_read_from_buf();
            lcd_set_entry_mode(mode);

            break;
        }

        case 0x0A:      // SET LCD CURSOR MODE
        {
            unsigned char mode;

            mode = serial_read_from_buf();
            lcd_set_cursor_mode(mode);

            break;
        }

        case 0x0B:      // DEFINE CUSTOM CHARACTER
        {
            unsigned char place, i, bytes[8];

            place = serial_read_from_buf();

            if (place < 8)
            {
                for (i = 0; i < 8; i++)
                    bytes[i] = serial_read_from_buf();

                lcd_define_custom_char(place, bytes);
            }

            break;
        }

        case 0x0C:      // DRAW PROGRESS BAR
        {
            unsigned char x, y, width, value, first_custom_char_place;

            x = serial_read_from_buf();
            y = serial_read_from_buf();
            width = serial_read_from_buf();
            value = serial_read_from_buf();
            first_custom_char_place = serial_read_from_buf();

            lcd_draw_progress_bar(x, y, width, value, first_custom_char_place);

            break;
        }

        case 0x0D:      // DRAW VERTICAL PROGRESS BAR
        {
            unsigned char x, y, value, custom_char_place, invert;

            x = serial_read_from_buf();
            y = serial_read_from_buf();
            value = serial_read_from_buf();
            custom_char_place = serial_read_from_buf();
            invert = serial_read_from_buf();

            lcd_draw_vertical_progress_bar(x, y, value, custom_char_place, invert);

            break;
        }

        case 0x0E:      // DRAW PROGRESS BAR (ADVANCED)
        {
            unsigned char x, y, width, value;
            unsigned char block_char_place, inner_char_place, progress_char_place, end_char_place;

            x = serial_read_from_buf();
            y = serial_read_from_buf();
            width = serial_read_from_buf();
            value = serial_read_from_buf();
            block_char_place = serial_read_from_buf();
            inner_char_place = serial_read_from_buf();
            progress_char_place = serial_read_from_buf();
            end_char_place = serial_read_from_buf();

            lcd_draw_progress_bar_ex(x, y, width, value,
                            block_char_place, inner_char_place, progress_char_place,
                            end_char_place);

            break;
        }

        case 0xF0:      // SET LCD BACKLIGHT
        {
            uint8_t duty = serial_read_from_buf();
            last_pwm_duty = duty;
            pwm1_set_duty_cycle(duty);

            break;
        }

        // TODO implement baud rate switch
//        case 0xF1:      // SET USART BAUD RATE
//        {
//            unsigned char baud;
//
//            baud = serial_read_from_buf();
//
//            switch (baud)
//            {
//                case 0: set_uart_speed(9600); break;
//                case 1: set_uart_speed(14400); break;
//                case 2: set_uart_speed(19200); break;
//                case 3: set_uart_speed(38400); break;
//                case 4: set_uart_speed(57600); break;
//                case 5: set_uart_speed(115200); break;
//                default: set_uart_speed(9600);
//            }
//
//            break;
//        }

        // TODO implement LCD backlight timeout
//        case 0xF2:      // SET LCD BACKLIGHT TIMEOUT
//        {
//            unsigned char timeout_seconds;
//
//            timeout_seconds = serial_read_from_buf();
//
//            if (timeout_seconds >= 1)
//                lcd_backlight_timeout = timeout_seconds;
//
//            break;
//        }

        case 0xF3:      // SET LCD CONTRAST
        {
            pwm2_set_duty_cycle(serial_read_from_buf());
            break;
        }

        case 0xF4:      // SWITCH DISPLAY ON/OFF
        {
            lcd_switch_display(serial_read_from_buf() > 0);
            break;
        }

        case 0xFF:      // RESET CPU - using WDT
            OPTION_REG = 0b11111000;
            for (;;);

        default:
            break;
    }            
}