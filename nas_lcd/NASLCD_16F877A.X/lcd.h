/* 
 * File:   lcd.h
 * Author: Tam�s
 *
 * Created on 2014. m�rcius 22., 21:04
 */

#ifndef LCD_H
#define	LCD_H

#include <stdint.h>

//#define LCD_ENTRY_MODE_DEC_NOSHIFT     0x04
//#define LCD_ENTRY_MODE_DEC_SHIFT       0x05
//#define LCD_ENTRY_MODE_INC_NOSHIFT     0x06
//#define LCD_ENTRY_MODE_INC_SHIFT       0x07
//
//#define LCD_CURSOR_MODE_OFF            0x0C
//#define LCD_CURSOR_MODE_BLINK          0x0D
//#define LCD_CURSOR_MODE_UNDERLINE      0x0E

typedef enum
{
    LCD_ENTRY_MODE_DEC_NO_SHIFT     = 0x00,
    LCD_ENTRY_MODE_DEC_SHIFT        = 0x01,
    LCD_ENTRY_MODE_INC_NO_SHIFT     = 0x02,
    LCD_ENTRY_MODE_INC_SHIFT        = 0x03
} lcd_entry_mode_t;

typedef enum
{
    LCD_CURSOR_OFF                  = 0x00,
    LCD_CURSOR_BLINK                = 0x01,
    LCD_CURSOR_UNDERLINE            = 0x02,
    LCD_CURSOR_BLINK_UNDERLINE      = 0x03
} lcd_cursor_mode_t;

typedef enum
{
    LCD_SHIFT_SCREEN_LEFT           = 0x18,
    LCD_SHIFT_SCREEN_RIGHT          = 0x1C
} lcd_screen_shift_dir_t;

typedef enum
{
    LCD_SHIFT_CURSOR_LEFT           = 0x10,
    LCD_SHIFT_CURSOR_RIGHT          = 0x14
} lcd_cursor_shift_dir_t;


void lcd_init();
void lcd_clear();

uint8_t lcd_read_byte();
uint8_t lcd_getc(uint8_t x, uint8_t y);

void lcd_send_byte(uint8_t is_data_byte, uint8_t data);
void lcd_putc(uint8_t c);
void lcd_puts(char *s);

void lcd_gotoxy(uint8_t x, uint8_t y);

void lcd_shift_screen(lcd_screen_shift_dir_t direction);
void lcd_shift_cursor(lcd_cursor_shift_dir_t direction);
void lcd_set_entry_mode(lcd_entry_mode_t mode);
void lcd_set_cursor_mode(lcd_cursor_mode_t mode);

void lcd_switch_display(uint8_t on);

void lcd_define_custom_char(uint8_t place, uint8_t *char_data);

void lcd_draw_progress_bar(uint8_t x,
                           uint8_t y,
                           uint8_t width,
                           uint8_t value,
                           uint8_t first_custom_char_place);

void lcd_draw_vertical_progress_bar(uint8_t x,
                                    uint8_t y,
                                    uint8_t value,
                                    uint8_t custom_char_place,
                                    uint8_t invert);

void lcd_draw_progress_bar_ex(uint8_t x,
                              uint8_t y,
                              uint8_t width,
                              uint8_t value,
                              uint8_t block_char_place,
                              uint8_t inner_char_place,
                              uint8_t progress_char_place,
                              uint8_t end_char_place);

#endif	/* LCD_H */

