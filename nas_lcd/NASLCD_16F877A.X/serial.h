/* 
 * File:   serial.h
 * Author: Tam�s
 *
 * Created on 2014. m�rcius 23., 0:30
 */

#ifndef SERIAL_H
#define	SERIAL_H

void serial_init();
char serial_read_from_buf();
void serial_write_to_buf(char c);

#endif	/* SERIAL_H */

