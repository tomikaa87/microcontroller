/*
 * File:   config.h
 * Author: Tam�s
 *
 * Created on 2014. m�rcius 22., 21:21
 */

#ifndef CONFIG_H
#define CONFIG_H

#ifndef _XTAL_FREQ
#define _XTAL_FREQ                      20000000
#endif

#define UART_RX_BUF_SIZE                80

#ifdef DO_PIC_CONFIGURATION
#pragma config FOSC = HS
#pragma config WDTE = ON
#pragma config PWRTE = OFF
#pragma config BOREN = ON
#pragma config LVP = OFF
#pragma config CPD = OFF
#pragma config WRT = OFF
#pragma config CP = OFF
#endif

#ifdef USE_LCD_CONFIGURATION
#include <xc.h>

#define LCD_PIN_RS                      PORTDbits.RD1
#define LCD_PIN_R_W                     PORTDbits.RD2
#define LCD_PIN_EN                      PORTDbits.RD0

#define LCD_PIN_RS_TRIS                 TRISDbits.TRISD1
#define LCD_PIN_R_W_TRIS                TRISDbits.TRISD2
#define LCD_PIN_EN_TRIS                 TRISDbits.TRISD0

#define LCD_PIN_D4                      PORTDbits.RD4
#define LCD_PIN_D5                      PORTDbits.RD5
#define LCD_PIN_D6                      PORTDbits.RD6
#define LCD_PIN_D7                      PORTDbits.RD7

#define LCD_PIN_D4_TRIS                 TRISDbits.TRISD4
#define LCD_PIN_D5_TRIS                 TRISDbits.TRISD5
#define LCD_PIN_D6_TRIS                 TRISDbits.TRISD6
#define LCD_PIN_D7_TRIS                 TRISDbits.TRISD7

#define LCD_DATA_DELAY                  _delay(1);
#define LCD_READ_STROBE_DELAY           __delay_us(2);
#define LCD_NIBBLE_STROBE_DELAY         __delay_us(10);
#define LCD_INIT_DELAY                  __delay_ms(15);
#define LCD_CLEAR_DELAY                 __delay_ms(1);
#endif


#endif /* CONFIG_H */