/* 
 * File:   lcd.c
 * Author: Tam�s
 * 
 * Created on 2014. m�rcius 22., 21:04
 */

#define USE_LCD_CONFIGURATION

#include "config.h"
#include "lcd.h"

//------------------------------------------------------------------------------
// Forward declarations
//------------------------------------------------------------------------------

static void send_nibble(uint8_t nibble)
{
    LCD_PIN_D4_TRIS = 0;
    LCD_PIN_D5_TRIS = 0;
    LCD_PIN_D6_TRIS = 0;
    LCD_PIN_D7_TRIS = 0;

    LCD_PIN_D4 = (nibble & 1) ? 1 : 0;
    LCD_PIN_D5 = (nibble & 2) ? 1 : 0;
    LCD_PIN_D6 = (nibble & 4) ? 1 : 0;
    LCD_PIN_D7 = (nibble & 8) ? 1 : 0;

    LCD_DATA_DELAY
    LCD_PIN_EN = 1;
    LCD_NIBBLE_STROBE_DELAY
    LCD_PIN_EN = 0;
    LCD_NIBBLE_STROBE_DELAY
}

// Checks state of "busy" flag
#define wait_for_device()       while (lcd_read_byte() & 0b10000000)
//#define wait_for_device()       __delay_ms(1)

#define TRUE                    1
#define FALSE                   0

#define LINE_1_ADDR             0x00
#define LINE_2_ADDR             0x40
#define LINE_3_ADDR             0x10
#define LINE_4_ADDR             0x50

static uint8_t display_settings = 0b00001100;

//------------------------------------------------------------------------------
// API functions
//------------------------------------------------------------------------------

void lcd_init()
{
    LCD_PIN_RS = 0;
    LCD_PIN_R_W = 0;
    LCD_PIN_EN = 0;

    LCD_PIN_RS_TRIS = 0;
    LCD_PIN_R_W_TRIS = 0;
    LCD_PIN_EN_TRIS = 0;

    LCD_INIT_DELAY

    // Prepare for 4-bit mode
    for (uint8_t i = 0; i < 3; ++i)
    {
        send_nibble(0x03);
        __delay_ms(5);
    }
    send_nibble(0x02);

    // Initialize the controller and switch to 4-bit mode
    const uint8_t INIT_DATA[] = { 0x28, 0x0C, 0x01, 0x06 };
    for (uint8_t i = 0; i < sizeof (INIT_DATA); ++i)
        lcd_send_byte(FALSE, INIT_DATA[i]);
    lcd_send_byte(FALSE, 0x01);
}

void lcd_clear()
{
    lcd_send_byte(FALSE, 0x01);
    LCD_CLEAR_DELAY
}

uint8_t lcd_read_byte()
{
    uint8_t data;

    LCD_PIN_D4_TRIS = 1;
    LCD_PIN_D5_TRIS = 1;
    LCD_PIN_D6_TRIS = 1;
    LCD_PIN_D7_TRIS = 1;

    LCD_PIN_R_W = 1;
    LCD_DATA_DELAY
    LCD_PIN_EN = 1;
    LCD_READ_STROBE_DELAY

    data = LCD_PIN_D4 << 4;
    data |= LCD_PIN_D5 << 5;
    data |= LCD_PIN_D6 << 6;
    data |= LCD_PIN_D7 << 7;
    
    LCD_PIN_EN = 0;
    LCD_DATA_DELAY
    LCD_PIN_EN = 1;
    LCD_READ_STROBE_DELAY

    data |= LCD_PIN_D4;
    data |= LCD_PIN_D5 << 1;
    data |= LCD_PIN_D6 << 2;
    data |= LCD_PIN_D7 << 3;

    LCD_PIN_EN = 0;

    return data;
}

uint8_t lcd_getc(uint8_t x, uint8_t y)
{
    uint8_t data;

    lcd_gotoxy(x, y);

    LCD_PIN_RS = 0;

    wait_for_device();

    LCD_PIN_RS = 1;
    data = lcd_read_byte();
    LCD_PIN_RS = 0;

    return data;
}

void lcd_send_byte(uint8_t is_data_byte, uint8_t data)
{
    LCD_PIN_RS = 0;

    wait_for_device();

    LCD_PIN_RS = is_data_byte;
    LCD_DATA_DELAY
    LCD_PIN_R_W = 0;
    LCD_DATA_DELAY
    LCD_PIN_EN = 0;

    send_nibble(data >> 4);
    send_nibble(data & 0x0F);
}

void lcd_putc(uint8_t c)
{
    lcd_send_byte(TRUE, c);
}

void lcd_puts(char *s)
{
    while (*s)
    {
        lcd_send_byte(TRUE, *s);
        s++;
    }
}

void lcd_gotoxy(uint8_t x, uint8_t y)
{
    uint8_t addr;

    switch (y)
    {
        case 0:
            addr = LINE_1_ADDR;
            break;
        case 1:
            addr = LINE_2_ADDR;
            break;
        case 2:
            addr = LINE_3_ADDR;
            break;
        case 3:
            addr = LINE_4_ADDR;
            break;
    }
    addr += x;
    
    lcd_send_byte(FALSE, 0x80 | addr);
}

void lcd_shift_screen(lcd_screen_shift_dir_t direction)
{
    lcd_send_byte(FALSE, 0b10000 | direction);
}

void lcd_shift_cursor(lcd_cursor_shift_dir_t direction)
{
    lcd_send_byte(FALSE, 0b10000 | direction);
}

void lcd_set_entry_mode(lcd_entry_mode_t mode)
{
    lcd_send_byte(FALSE, 0b100 | mode);
}

void lcd_set_cursor_mode(lcd_cursor_mode_t mode)
{
    // Preserve display setting and set cursor mode
    display_settings &= 0b11111100;
    display_settings |= mode & 0b11;

    lcd_send_byte(FALSE, 0b1000 | (display_settings & 0b111));
}

void lcd_switch_display(uint8_t on)
{
    // Preserve cursor setting and switch display state
    display_settings &= 0b11111011;
    display_settings |= on ? 0b100 : 0;

    lcd_send_byte(FALSE, 0b1000 | (display_settings & 0b111));
}

void lcd_define_custom_char(uint8_t place, uint8_t *char_data)
{
    if (place <= 7)
    {
        lcd_send_byte(FALSE, 0x40 + place * 8);
        for (uint8_t i = 0; i < 8; ++i)
            lcd_putc(char_data[i] | 0x20);
    }
}

void lcd_draw_progress_bar(uint8_t x,
                           uint8_t y,
                           uint8_t width,
                           uint8_t value,
                           uint8_t first_custom_char_place)
{
    lcd_draw_progress_bar_ex(x, y, width, value, 
                             first_custom_char_place,
                             first_custom_char_place + 1,
                             first_custom_char_place + 2,
                             first_custom_char_place + 3);
}

void lcd_draw_vertical_progress_bar(uint8_t x,
                                    uint8_t y,
                                    uint8_t value,
                                    uint8_t custom_char_place,
                                    uint8_t invert)
{
    uint8_t height_px;
    uint8_t custom_char_data[8];
    uint8_t custom_char_ptr;
    uint8_t i;

    if (value > 100)
        value = 100;

    // FIXME
    height_px = (uint8_t)((float)value / 100.0 * 8.0);
    
    custom_char_ptr = 0;
    for (i = 8; i > 0; i--)
    {
        if (i > height_px)
            custom_char_data[custom_char_ptr++] = invert > 0 ? 0x1F : 0x00;
        else
            custom_char_data[custom_char_ptr++] = invert > 0 ? 0x00 : 0x1F;
    }

    lcd_define_custom_char(custom_char_place, custom_char_data);
    lcd_gotoxy(x, y);
    lcd_putc(custom_char_place);
}

void lcd_draw_progress_bar_ex(uint8_t x,
                              uint8_t y,
                              uint8_t width,
                              uint8_t value,
                              uint8_t block_char_place,
                              uint8_t inner_char_place,
                              uint8_t progress_char_place,
                              uint8_t end_char_place)
{
    uint8_t width_px;
    uint8_t progress_px;
    uint8_t custom_char_data[8];
    uint8_t custom_char_ptr;
    uint8_t i;
    uint8_t line_pattern;
    uint8_t block_count;

    width_px = width * 5 - 2;

    // FIXME - try to use only integers
    progress_px = (uint8_t)((float)value / 100.0 * (float)width_px);

    if (value > 100)
        value = 100;

    if (progress_px < 5)
    {
        /*** First char ***/
        line_pattern = 0x10;
        for (i = 0; i < progress_px; ++i)
            line_pattern |= (1 << (3 - i));

        /*** First character ***/
        custom_char_data[0] = 0x1F; // first row
        custom_char_ptr = 1;
        for (i = 0; i < 6; ++i)
            custom_char_data[custom_char_ptr++] = line_pattern;
        custom_char_data[custom_char_ptr] = 0x1F; // last row
        lcd_define_custom_char(progress_char_place, custom_char_data);

        /*** Inner characters ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; ++i)
            custom_char_data[custom_char_ptr++] = 0;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(inner_char_place, custom_char_data);

        /*** Ending character ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; ++i)
            custom_char_data[custom_char_ptr++] = 0x01;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(end_char_place, custom_char_data);

        /*** Put characters ***/
        lcd_gotoxy(x, y);
        lcd_putc(progress_char_place);
        for (i = 0; i < width - 2; ++i)
            lcd_putc(inner_char_place);
        lcd_putc(end_char_place);
    }
    else if (progress_px < width_px - 4)
    {
        /*** 'block' characters ***/
        for (i = 0; i < 8; ++i)
            custom_char_data[i] = 0x1F;
        lcd_define_custom_char(block_char_place, custom_char_data);

        /*** 'progress' character ***/
        line_pattern = 0;
        for (i = 0; i < (progress_px - 4) % 5; ++i)
            line_pattern |= (1 << (4 - i));
        custom_char_data[0] = 0x1F; // first row
        custom_char_ptr = 1;
        for (i = 0; i < 6; ++i)
            custom_char_data[custom_char_ptr++] = line_pattern;
        custom_char_data[custom_char_ptr] = 0x1F; // last row
        lcd_define_custom_char(progress_char_place, custom_char_data);

        /*** Inner characters ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; ++i)
            custom_char_data[custom_char_ptr++] = 0;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(inner_char_place, custom_char_data);

        /*** Ending character ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; ++i)
            custom_char_data[custom_char_ptr++] = 0x01;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(end_char_place, custom_char_data);

        /*** Put characters ***/
        lcd_gotoxy(x, y);
        lcd_putc(block_char_place);
        block_count = 0;
        for (i = 0; i < (progress_px - 4) / 5; ++i)
        {
            lcd_putc(block_char_place);
            block_count++;
        }
        lcd_putc(progress_char_place);
        for (i = 0; i < width - 2 - block_count - 1; ++i)
            lcd_putc(inner_char_place);
        lcd_putc(end_char_place);
    }
    else
    {
        /*** 'block' character ***/
        custom_char_ptr = 0;
        for (i = 0; i < 8; ++i)
            custom_char_data[custom_char_ptr++] = 0x1F;
        lcd_define_custom_char(block_char_place, custom_char_data);

        /*** 'progress' character ***/
        line_pattern = 0x01;
        custom_char_ptr = 0;
        for (i = 0; i < 4 - (width_px - progress_px); ++i)
            line_pattern |= (1 << (4 - i));
        custom_char_data[custom_char_ptr++] = 0x1F; // first row
        for (i = 0; i < 6; ++i)
            custom_char_data[custom_char_ptr++] = line_pattern;
        custom_char_data[custom_char_ptr] = 0x1F; // last row
        lcd_define_custom_char(progress_char_place, custom_char_data);

        /*** Put characters ***/
        lcd_gotoxy(x, y);
        for (i = 0; i < width - 1; ++i)
            lcd_putc(block_char_place);
        lcd_putc(progress_char_place);
    }
}

#include <xc.h>
