/*
 * File:   main.c
 * Author: Tam�s
 *
 * Created on 2014. m�rcius 22., 21:04
 */

#define DO_PIC_CONFIGURATION
#define USE_LCD_CONFIGURATION

#include <xc.h>

#include "config.h"
#include "serial.h"
#include "interface.h"

void interrupt isr()
{
    if (RCIE && RCIF)
    {
        serial_write_to_buf(RCREG);
    }
}

void setup_interrupts()
{
    GIE = 1;
    PEIE = 1;
    RCIE = 1;
}

void setup_pwm()
{
    PR2 = 0b01111100;
    T2CON = 0b100;
    CCPR1L = 0;
    CCP1CON = 0b1100;
    CCPR2L = 0;
    CCP2CON = 0b1100;

    TRISC1 = 0;
    TRISC2 = 0;
}

void main()
{
    setup_pwm();
    serial_init();
    setup_interrupts();

    interface_init();

    for (;;)
    {
        interface_task();
        CLRWDT();
    }
}
