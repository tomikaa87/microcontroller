/*
 * File:   serial.c
 * Author: Tam�s
 *
 * Created on 2014. m�rcius 23., 0:30
 */

#include "serial.h"
#include "config.h"

#include <xc.h>
#include <stdint.h>

volatile char rx_buf[UART_RX_BUF_SIZE] = {0, };
volatile uint8_t rx_buf_next_in = 0;
volatile uint8_t rx_buf_next_out = 0;
#define rx_buf_has_char     (rx_buf_next_in != rx_buf_next_out)

void serial_init()
{
    // 9600 bps @20 MHz
    BRGH = 1;
    SPBRG = 129;
    CREN = 1;
    SPEN = 1;
}

char serial_read_from_buf()
{
    // Wait untit a byte is ready to read from the USART buffer
    while (!rx_buf_has_char)
        CLRWDT();
    
    char c = rx_buf[rx_buf_next_out];

    // Step to next index
    rx_buf_next_out = (rx_buf_next_out + 1) % UART_RX_BUF_SIZE;
    
    return c;
}

void serial_write_to_buf(char c)
{
    rx_buf[rx_buf_next_in] = c;
    uint8_t next_in = rx_buf_next_in;
    rx_buf_next_in = (rx_buf_next_in + 1) % UART_RX_BUF_SIZE;
    if (rx_buf_next_in == rx_buf_next_out)
        rx_buf_next_in = next_in;
}