/***************************************************************************************************
 * Serial LCD Driver for Network Attached Storage
 *
 * Written by ToMikaa for CCS PIC C
 * Date: 2010-10-25
 *
 ***************************************************************************************************
 * Instruction set 
 *
 * A command package is like shown below:
 *
 *    [0x1F][CommandCode][AdditionalParams]
 *
 * List of possible command codes and parameters:
 *
 *    0x01 : Init LCD
 *    0x02 : Clear LCD
 *    0x03 : Set LCD Address
 *      Param 1: 0x00 - 0xFF : LCD Address
 *    0x04 : Set X and Y Coordinates of LCD cursor
 *      Param 2: 0x00 - 0xFF : X Coordinate
 *      Param 3: 0x00 - 0xFF : Y Coordinate
 *    0x05 : Shift LCD Cursor Left
 *    0x06 : Shift LCD Cursor Right
 *    0x07 : Shift LCD Screen Left
 *    0x08 : Shift LCD Screen Right
 *    0x09 : Set LCD Entry Mode
 *      Param 1: 0x04 - 0x07 : Entry Mode
 *    0x0A : Set LCD Cursor Mode
 *      Param 1: 0x0C - 0x0F : Cursor Mode
 *    0x0B : Define Custom Character
 *      Param 1 : 0x00 - 0x07 : Custom Character Place
 *      Param 2-9: 0x00 - 0xFF : Custom Character Byte 1-8
 *    0x0C : Draw Progress Bar
 *      Param 1 : 0x00 - 0xFF : X Coordinate
 *      Param 2 : 0x00 - 0xFF : Y Coordinate
 *      Param 3 : 0x00 - 0xFF : Width
 *      Param 4 : 0x00 - 0x64 : Value (Percentage)
 *      Param 5 : 0x00 - 0xF7 : First Custom Char Place
 *    0x0D : Draw Vertical Progress Bar
 *      Param 1 : 0x00 - 0xFF : X Coordinate
 *      Param 2 : 0x00 - 0xFF : Y Coordinate
 *      Param 3 : 0x00 - 0x64 : Value (Percentage)
 *      Param 4 : 0x00 - 0x07 : Custom Char Place
 *      Param 5 : 0x00 - 0x01 : Invert
 *    0x0E : Draw Progress Bar (Advanced, custom character places)
 *      Param 1 : 0x00 - 0xFF : X Coordinate
 *      Param 2 : 0x00 - 0xFF : Y Coordinate
 *      Param 3 : 0x00 - 0xFF : Width
 *      Param 4 : 0x00 - 0x64 : Value (Percentage)
 *      Param 5 : 0x00 - 0x07 : 'Block' Character Place
 *      Param 6 : 0x00 - 0x07 : 'Inner' Character Place
 *      Param 7 : 0x00 - 0x07 : 'Progress' Character Place
 *      Param 8 : 0x00 - 0x07 : 'End' Character Place
 *    0xF0 : Set LCD Backlight Level
 *      Param 1 : 0x00 - 0xFF : Level
 *    0xF1 : Set USART Baud Rate
 *      Param 1 : 0x00 - 0x05 : Baud Rate (9600 (default, 0), 14400, 19200, 38400, 57600, 115200)
 *    0xFF : Reset CPU
 *
 **************************************************************************************************/

#include <naslcd_ccs.h>
#include "lcd.c"

/*** Constants ************************************************************************************/
#define FIRMWARE_VERSION            "1.53"
#define LCD_BL_MAX_PWM              400
#define LCD_BL_DEFAULT_PWM          100
#define LCD_BL_NIGHT_PWM            10
#define TIMER0_TICKS_PER_SECOND     76

/*** Globals **************************************************************************************/
#define USART_BUF_SIZE 64
unsigned char usart_buf[USART_BUF_SIZE];
unsigned char usart_buf_next_in = 0;
unsigned char usart_buf_next_out = 0;

#define usart_buf_byte_ready (usart_buf_next_in != usart_buf_next_out)

unsigned long epoch = 0;                        // Unix epoch time
unsigned int timer_ticks = 0;                   // RTC timer tick counter
unsigned char epoch_tick_flag = 0;              // Indicates if a second has elapsed
unsigned char lcd_backlight_timeout = 30;       // Timeout for LCD backlight
unsigned long last_command_time = 0;            // Epoch time of the last command
unsigned int lcd_last_pwm = LCD_BL_DEFAULT_PWM;
unsigned char lcd_backlight_state = 1;          // Current state of the LCD backlight

/*** Interrupt Service Routines *******************************************************************/
#int_RDA
void RDA_isr(void) 
{
    unsigned char t;
    
    disable_interrupts(INT_TIMER0);
    
    // Get a byte from the USART buffer register and store it
    usart_buf[usart_buf_next_in] = getch();
    t = usart_buf_next_in;
    usart_buf_next_in = (usart_buf_next_in + 1) % USART_BUF_SIZE;
    if (usart_buf_next_in == usart_buf_next_out)
        usart_buf_next_in = t;
        
    enable_interrupts(INT_TIMER0);
}

#int_TIMER0
void TIMER0_isr(void)
{
    // Timer0 interrupt (RTC timer)
    // Increment tick counter
    timer_ticks++;
    
    // If we have reached the number of ticks per second,
    // increment epoch time counter
    if (timer_ticks == TIMER0_TICKS_PER_SECOND)
    {
        epoch++;
        timer_ticks = 0;  
        epoch_tick_flag = 1; 
    }   
}    

/*** USART buffer handling functions **************************************************************/
unsigned char usart_buf_getch()
{
    unsigned char c;
    
    // Wait untit a byte is ready to read from the USART buffer
    while (!usart_buf_byte_ready)
        restart_wdt();       
    
    c = usart_buf[usart_buf_next_out];
    usart_buf_next_out = (usart_buf_next_out + 1) % USART_BUF_SIZE;
    
    return c;
}    

/*** Main *****************************************************************************************/
void main()
{
    unsigned char c, cmd_code, i;
    //unsigned char prev_char, reset_counter;

    // Setup PIC
    setup_wdt(WDT_2304MS | WDT_TIMES_128);
    setup_adc_ports(NO_ANALOGS|VSS_VDD);
    setup_adc(ADC_CLOCK_DIV_2);
    //setup_spi(SPI_SS_DISABLED);
    setup_timer_0(RTCC_INTERNAL|RTCC_DIV_256);
    setup_timer_1(T1_DISABLED);
    setup_timer_2(T2_DIV_BY_4,249,1);
    setup_ccp1(CCP_PWM);
    set_pwm1_duty(LCD_BL_DEFAULT_PWM);
    setup_comparator(NC_NC_NC_NC);
    enable_interrupts(INT_TIMER0);
    enable_interrupts(GLOBAL);
    
    // Init the LCD
    lcd_init();
    
    // Show the banner
    lcd_gotoxy(1, 1);
    lcd_putc(":ToMikaa's  NAS:");
    lcd_gotoxy(1, 2);
    lcd_putc(" Ver: ");
    lcd_putc(FIRMWARE_VERSION);
    lcd_putc(" 2010");
    
    //prev_char = 0;
    //reset_counter = 0;
    
    enable_interrupts(INT_RDA);
    
    // Main loop
    for (;;)
    {
        restart_wdt();
        
        if (epoch_tick_flag)
        {
            epoch_tick_flag = 0; 
            
            if (epoch - last_command_time > lcd_backlight_timeout && lcd_backlight_state == 1)
            {
                lcd_backlight_state = 0;
                set_pwm1_duty(0);
                
                // Init the LCD
                lcd_init();
            }    
        }
        
        // if there is no byte in the buffer, continue with next iteration
        if (!usart_buf_byte_ready)
            continue;
            
        last_command_time = epoch;
        if (lcd_backlight_state == 0)
        {
            lcd_backlight_state = 1;
            set_pwm1_duty(lcd_last_pwm);
        }
            
        c = usart_buf_getch();
        
        // Check if the received char is the same of the previous one.
        // If this occours 30 times, the MCU will be restarted
        /*if (c == prev_char)
            reset_counter++;
        else
            reset_counter = 0;
        if (reset_counter == 29)
            reset_cpu();
        prev_char = c;*/
        
        // if the received byte is not a command start byte, continue
        // or if it is a >= 0x20 byte, put it on the LCD
        // or if it is a <= 0x07 byte, put the custom char on the LCD
        if (c != 0x1F)
        {
            if (c >= 0x20 || c <= 0x07)
                lcd_putc(c);
                
            continue;
        }    
       
        // Get command code
        cmd_code = usart_buf_getch();
        
        // Process received command
        switch (cmd_code)
        {
            case 0x01:      // INIT LCD
                lcd_init();
                break;
                
            case 0x02:      // CLEAR LCD
                lcd_clear();    
                break;
                
            case 0x03:      // SET LCD ADDRESS
            {
                unsigned char address;
                
                address = usart_buf_getch();
                lcd_send_byte(0, 0x80 | address);
                
                break;
            }    
            
            case 0x04:      // GOTO XY
            {
                unsigned char x, y;
                
                x = usart_buf_getch();
                y = usart_buf_getch();
                
                lcd_gotoxy(x + 1, y + 1);  
                
                break; 
            }    
            
            case 0x05:      // LCD CURSOR SHIFT LEFT
                lcd_shift_cursor(LCD_SHIFT_LEFT);
                break;
                
            case 0x06:      // LCD CURSOR SHIFT RIGHT
                lcd_shift_cursor(LCD_SHIFT_RIGHT);
                break;
                
            case 0x07:      // LCD SCREEN SHIFT LEFT
                lcd_shift_screen(LCD_SHIFT_LEFT);
                break;
                
            case 0x08:      // LCD SCREEN SHIFT RIGHT
                lcd_shift_screen(LCD_SHIFT_RIGHT);
                break;
                
            case 0x09:      // SET LCD ENTRY MODE
            {
                unsigned char mode;
                
                mode = usart_buf_getch();
                lcd_set_entry_mode(mode);
                
                break;   
            }
            
            case 0x0A:      // SET LCD CURSOR MODE
            {
                unsigned char mode;
                
                mode = usart_buf_getch();
                lcd_set_cursor_mode(mode);
                
                break;   
            }   
            
            case 0x0B:      // DEFINE CUSTOM CHARACTER
            {
                unsigned char place, i, bytes[8];
                
                place = usart_buf_getch();
                
                if (place < 8)
                {
                    for (i = 0; i < 8; i++)
                        bytes[i] = usart_buf_getch();
                        
                    lcd_define_custom_char(place, bytes);
                }
                
                break;    
            }     
            
            case 0x0C:      // DRAW PROGRESS BAR
            {
                unsigned char x, y, width, value, first_custom_char_place;
                
                x = usart_buf_getch();
                y = usart_buf_getch();
                width = usart_buf_getch();
                value = usart_buf_getch();
                first_custom_char_place = usart_buf_getch();
                
                lcd_draw_progress_bar(x + 1, y + 1, width, value, first_custom_char_place);
                
                break; 
            }
            
            case 0x0D:      // DRAW VERTICAL PROGRESS BAR    
            {
                unsigned char x, y, value, custom_char_place, invert;
                
                x = usart_buf_getch();
                y = usart_buf_getch();
                value = usart_buf_getch();
                custom_char_place = usart_buf_getch();
                invert = usart_buf_getch(); 

                lcd_draw_vertical_progress_bar(x + 1, y + 1, value, custom_char_place, invert);
                
                break;
            }       
            
            case 0x0E:      // DRAW PROGRESS BAR (ADVANCED)
            {
                unsigned char x, y, width, value;
                unsigned char block_char_place, inner_char_place, progress_char_place, end_char_place;
                
                x = usart_buf_getch();
                y = usart_buf_getch();
                width = usart_buf_getch();
                value = usart_buf_getch();
                block_char_place = usart_buf_getch();
                inner_char_place = usart_buf_getch();
                progress_char_place = usart_buf_getch();
                end_char_place = usart_buf_getch();
                
                lcd_draw_progress_bar_adv(x + 1, y + 1, width, value,
                                block_char_place, inner_char_place, progress_char_place,
                                end_char_place);
                
                break; 
            } 
            
            case 0xF0:      // SET LCD BACKLIGHT
            {
                unsigned char bl;
                unsigned int duty;
                
                bl = usart_buf_getch();
                duty = (unsigned int)((float)bl / 255.0 * (float)LCD_BL_MAX_PWM);
                lcd_last_pwm = duty;
                set_pwm1_duty(duty);
                
                break;   
            } 
            
            case 0xF1:      // SET USART BAUD RATE
            {
                unsigned char baud;
                
                baud = usart_buf_getch();
                
                switch (baud)
                {
                    case 0: set_uart_speed(9600); break;
                    case 1: set_uart_speed(14400); break;
                    case 2: set_uart_speed(19200); break;
                    case 3: set_uart_speed(38400); break;
                    case 4: set_uart_speed(57600); break;
                    case 5: set_uart_speed(115200); break;
                    default: set_uart_speed(9600);   
                }    
                
                break;   
            }
            
            case 0xF2:      // SET LCD BACKLIGHT TIMEOUT
            {
                unsigned char timeout_seconds;
                
                timeout_seconds = usart_buf_getch();
                
                if (timeout_seconds >= 1)
                    lcd_backlight_timeout = timeout_seconds;
                
                break;   
            }   
            
            case 0xFF:      // RESET CPU
                reset_cpu();
                break;  // maybe unnecessary
                
            default:
                break;
        }            
        
        restart_wdt();
    }    
}
