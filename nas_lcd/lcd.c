//#include "lcd.h"
//#include <htc.h>

// PIC configuration
/*#ifndef _XTAL_FREQ
#define _XTAL_FREQ  20000000
#endif*/

#define LCD_ENTRY_MODE_DEC_NOSHIFT     0x04
#define LCD_ENTRY_MODE_DEC_SHIFT       0x05
#define LCD_ENTRY_MODE_INC_NOSHIFT     0x06
#define LCD_ENTRY_MODE_INC_SHIFT       0x07

#define LCD_CURSOR_MODE_OFF            0x0C
#define LCD_CURSOR_MODE_BLINK          0x0D
#define LCD_CURSOR_MODE_UNDERLINE      0x0E

#define LCD_SHIFT_LEFT                 0x00
#define LCD_SHIFT_RIGHT                0x01


// LCD pin configuration
#define LCD_TRIS    *0x87

typedef struct
{
    char LCD_RS : 1;
    char LCD_RW : 1;
    char LCD_EN : 1;
    char LCD_D4 : 1;
    char LCD_D5 : 1;
    char unused : 1;
    char LCD_D6 : 1;
    char LCD_D7 : 1;
}
LCDPinMap;

LCDPinMap lcd;
#byte lcd = 0x07

#define LCD_WRITE   0b00000000
#define LCD_READ    0b11011000

#ifndef LCD_TYPE
   #define LCD_TYPE 2           // 0=5x7, 1=5x10, 2=2 lines
#endif

#define LCD_LINE_2  0x40    // LCD RAM address for the second line
#define LCD_LINE_3  0x10
#define LCD_LINE_4  0x50

const char LCD_INIT_STRING[4] = {0x20 | (LCD_TYPE << 2), 0x0C, 1, 6};

void lcd_send_nibble(unsigned char nibble)
{
    LCD_TRIS = LCD_WRITE;
    
    lcd.LCD_D4 = (nibble & 0b00000001) ? 1 : 0;
    lcd.LCD_D5 = (nibble & 0b00000010) ? 1 : 0;
    lcd.LCD_D6 = (nibble & 0b00000100) ? 1 : 0;
    lcd.LCD_D7 = (nibble & 0b00001000) ? 1 : 0;
    
    delay_cycles(1);
    lcd.LCD_EN = 1;
    delay_us(10);
    lcd.LCD_EN = 0;
    delay_us(10);
}   

unsigned char lcd_read_byte()
{
    unsigned char lo, hi;
    
    LCD_TRIS = LCD_READ;
    
    lcd.LCD_RW = 1;
    delay_cycles(1);
    lcd.LCD_EN = 1;
    delay_us(2);
    
    hi = lcd.LCD_D4;
    hi |= (lcd.LCD_D5 << 1);
    hi |= (lcd.LCD_D6 << 2);
    hi |= (lcd.LCD_D7 << 3);
    
    lcd.LCD_EN = 0;
    delay_cycles(1);
    lcd.LCD_EN = 1;
    delay_us(2);
    
    lo = lcd.LCD_D4;
    lo |= (lcd.LCD_D5 << 1);
    lo |= (lcd.LCD_D6 << 2);
    lo |= (lcd.LCD_D7 << 3);
    
    lcd.LCD_EN = 0;
    
    LCD_TRIS = LCD_WRITE;
    
    return ((hi << 4) | lo);   
}     

void lcd_send_byte(unsigned char cmd,
                   unsigned char data)
{
    lcd.LCD_RS = 0;
       
    while (lcd_read_byte() & 0b10000000);
    
    lcd.LCD_RS = cmd;
    delay_cycles(1);
    lcd.LCD_RW = 0;
    delay_cycles(1);
    lcd.LCD_EN = 0;
    
    lcd_send_nibble(data >> 4);
    lcd_send_nibble(data & 0x0F);
}    

void lcd_init()
{
    unsigned char i;
    
    LCD_TRIS = LCD_WRITE;
    lcd.LCD_RS = 0;
    lcd.LCD_RW = 0;
    lcd.LCD_EN = 0;
    delay_ms(15);       
    
    for (i = 0; i < 3; i++)
    {
        lcd_send_nibble(3);
        delay_ms(5);
    }    
    
    lcd_send_nibble(2);
    
    for (i = 0; i < 4; i++)
    {
        lcd_send_byte(0, LCD_INIT_STRING[i]);
    }    
    
    lcd_send_byte(0, 0x01);
}    

void lcd_putc(unsigned char c)
{
    if (c == '\r')
    {
        unsigned char addr;
        
        lcd.LCD_RS = 0;
        addr = lcd_read_byte();        
        
        if (addr < 0x10)                        // LINE 1
            addr = 0x00;                
        else if (addr >= 0x10 && addr < 0x40)   // LINE_3
            addr = 0x10;
        else if (addr >= 0x40 && addr < 0x50)   // LINE 2
            addr = 0x40;
        else                                    // LINE 4
            addr = 0x50;
           
        lcd_send_byte(0, 0x80 | addr);
    }    
    else if (c == '\n')
    {
        unsigned char addr;
        
        lcd.LCD_RS = 0;
        addr = lcd_read_byte();        
        
        if (addr < 0x10)                        // LINE 1
            addr = 0x40 + addr;                
        else if (addr >= 0x10 && addr < 0x40)   // LINE_3
            addr = 0x50 + (addr - 0x10);
        else if (addr >= 0x40 && addr < 0x50)   // LINE 2
            addr = 0x10 + (addr - 0x40);
        else                                    // LINE 4
            addr = 0x00 + (addr - 0x50);
            
        lcd_send_byte(0, 0x80 | addr);
    }
    else
        lcd_send_byte(1, c);  
}    

void lcd_gotoxy(unsigned char x,
                unsigned char y)
{
    unsigned char addr;
    
    switch (y)
    {
        case 1:
            addr = 0;
            break;
        case 2:
            addr = LCD_LINE_2;
            break;  
        case 3:
            addr = LCD_LINE_3;
            break;
        case 4:
            addr = LCD_LINE_4;
            break;
    }    
    
    addr += x - 1;
    lcd_send_byte(0, 0x80 | addr);
}    

void lcd_clear()
{
    lcd_send_byte(0, 0x01);   
    delay_ms(1);
}    

void lcd_puts(char *s) 
{
    while (*s) 
    {
        lcd_putc(*s++);
    }   
}

unsigned char lcd_getc(unsigned char x,
                       unsigned char y)
{
    unsigned char data;
    
    lcd_gotoxy(x, y);
    
    lcd.LCD_RS = 0;
    
    while (lcd_read_byte() & 0b10000000);    
    
    lcd.LCD_RS = 1;
    data = lcd_read_byte();
    lcd.LCD_RS = 0;
    
    return data;
}    

void lcd_shift_screen(unsigned char direction)
{
    if (direction == LCD_SHIFT_LEFT)
        lcd_send_byte(0, 16 | 8);
    else if (direction == LCD_SHIFT_RIGHT)
        lcd_send_byte(0, 16 | 8 | 4);
}    

void lcd_shift_cursor(unsigned char direction)
{
    if (direction == LCD_SHIFT_LEFT)
        lcd_send_byte(0, 16);
    else if (direction == LCD_SHIFT_RIGHT)
        lcd_send_byte(0, 16 | 4);    
}
    
void lcd_set_entry_mode(unsigned char mode)
{
    if (mode >= 0x04 && mode <= 0x07) 
    {
        lcd_send_byte(0, mode);   
    }    
}
    
void lcd_set_cursor_mode(unsigned char mode)
{
    if (mode >= 0x0C && mode <= 0x0F) 
    {
        lcd_send_byte(0, mode);   
    }     
}
    
void lcd_define_custom_char(unsigned char place,
                            unsigned char char_bytes[])
{
    unsigned char cgram_addr, i;
    
    if (place <= 7) 
    {
        cgram_addr = 0x40 + place * 8;
        lcd_send_byte(0, cgram_addr);
        //for (i = 0; i < 8; i++) 
        //    lcd_putc(0);
        //lcd_send_byte(0, cgram_addr);
        for (i = 0; i < 8; i++) 
            lcd_putc(char_bytes[i] | 0x20);
        //lcd_send_byte(0, 0);
    }    
}                                                                

void lcd_draw_progress_bar(unsigned char x,
                           unsigned char y,
                           unsigned char width,
                           unsigned char value,
                           unsigned char first_custom_char_place)
{
    unsigned char width_px;
    unsigned char progress_px;
    unsigned char custom_char_data[8];
    unsigned char custom_char_ptr;
    unsigned char i;
    unsigned char line_pattern;
    unsigned char block_count;
    
    width_px = width * 5 - 2;
    progress_px = (unsigned char)((float)value / 100.0 * (float)width_px);
    
    if (value > 100)
        value = 100;
    
    if (progress_px < 5)
    {
        /*** First char ***/
        line_pattern = 0x10;
        for (i = 0; i < progress_px; i++)
            line_pattern |= (1 << (3 - i));

        /*** First character ***/
        custom_char_data[0] = 0x1F; // first row
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = line_pattern;
        custom_char_data[custom_char_ptr] = 0x1F; // last row
        lcd_define_custom_char(first_custom_char_place, custom_char_data);

        /*** Inner characters ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = 0;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(first_custom_char_place + 1, custom_char_data);

        /*** Ending character ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = 0x01;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(first_custom_char_place + 2, custom_char_data);
        
        /*** Put characters ***/
        lcd_gotoxy(x, y);
        lcd_putc(first_custom_char_place);
        for (i = 0; i < width - 2; i++)
            lcd_putc(first_custom_char_place + 1);
        lcd_putc(first_custom_char_place + 2);
    }
    else if (progress_px < width_px - 4)
    {
        /*** 'block' characters ***/
        for (i = 0; i < 8; i++)
            custom_char_data[i] = 0x1F;
        lcd_define_custom_char(first_custom_char_place, custom_char_data);

        /*** 'progress' character ***/
        line_pattern = 0;
        for (i = 0; i < (progress_px - 4) % 5; i++)
            line_pattern |= (1 << (4 - i));
        custom_char_data[0] = 0x1F; // first row
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = line_pattern;
        custom_char_data[custom_char_ptr] = 0x1F; // last row
        lcd_define_custom_char(first_custom_char_place + 1, custom_char_data);

        /*** Inner characters ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = 0;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(first_custom_char_place + 2, custom_char_data);

        /*** Ending character ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = 0x01;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(first_custom_char_place + 3, custom_char_data);

        /*** Put characters ***/
        lcd_gotoxy(x, y);
        lcd_putc(first_custom_char_place);
        block_count = 0;
        for (i = 0; i < (progress_px - 4) / 5; i++)
        {
            lcd_putc(first_custom_char_place);
            block_count++;
        }
        lcd_putc(first_custom_char_place + 1);
        for (i = 0; i < width - 2 - block_count - 1; i++)
            lcd_putc(first_custom_char_place + 2);
        lcd_putc(first_custom_char_place + 3);
    }
    else
    {
        /*** 'block' character ***/
        custom_char_ptr = 0;
        for (i = 0; i < 8; i++)
            custom_char_data[custom_char_ptr++] = 0x1F;
        lcd_define_custom_char(first_custom_char_place, custom_char_data);

        /*** 'progress' character ***/
        line_pattern = 0x01;
        custom_char_ptr = 0;
        for (i = 0; i < 4 - (width_px - progress_px); i++)
            line_pattern |= (1 << (4 - i));
        custom_char_data[custom_char_ptr++] = 0x1F; // first row
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = line_pattern;
        custom_char_data[custom_char_ptr] = 0x1F; // last row
        lcd_define_custom_char(first_custom_char_place + 1, custom_char_data);

        /*** Put characters ***/
        lcd_gotoxy(x, y);
        for (i = 0; i < width - 1; i++)
            lcd_putc(first_custom_char_place);
        lcd_putc(first_custom_char_place + 1);
    }
}           

void lcd_draw_vertical_progress_bar(unsigned char x,
                                    unsigned char y,
                                    unsigned char value,
                                    unsigned char custom_char_place,
                                    unsigned char invert)
{
    unsigned char height_px;
    unsigned char custom_char_data[8];
    unsigned char custom_char_ptr;
    unsigned char i;
    
    if (value > 100)
        value = 100;
    
    height_px = (unsigned char)((float)value / 100.0 * 8.0);
    custom_char_ptr = 0;
    for (i = 8; i > 0; i--)
    {
        if (i > height_px)
            custom_char_data[custom_char_ptr++] = invert > 0 ? 0x1F : 0x00;
        else
            custom_char_data[custom_char_ptr++] = invert > 0 ? 0x00 : 0x1F;   
    }    
    
    lcd_define_custom_char(custom_char_place, custom_char_data);
    lcd_gotoxy(x, y);
    lcd_putc(custom_char_place);
}                                 

void lcd_draw_progress_bar_adv(unsigned char x,
                               unsigned char y,
                               unsigned char width,
                               unsigned char value,
                               unsigned char block_char_place,
                               unsigned char inner_char_place,
                               unsigned char progress_char_place,
                               unsigned char end_char_place)
{
    unsigned char width_px;
    unsigned int progress_px;
    unsigned char custom_char_data[8];
    unsigned char custom_char_ptr;
    unsigned char i;
    unsigned char line_pattern;
    unsigned char block_count;
    
    width_px = width * 5 - 2;
    progress_px = (unsigned char)((float)value / 100.0 * (float)width_px);
    
    if (value > 100)
        value = 100;
    
    if (progress_px < 5)
    {
        /*** First char ***/
        line_pattern = 0x10;
        for (i = 0; i < progress_px; i++)
            line_pattern |= (1 << (3 - i));

        /*** First character ***/
        custom_char_data[0] = 0x1F; // first row
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = line_pattern;
        custom_char_data[custom_char_ptr] = 0x1F; // last row
        lcd_define_custom_char(progress_char_place, custom_char_data);

        /*** Inner characters ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = 0;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(inner_char_place, custom_char_data);

        /*** Ending character ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = 0x01;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(end_char_place, custom_char_data);
        
        /*** Put characters ***/
        lcd_gotoxy(x, y);
        lcd_putc(progress_char_place);
        for (i = 0; i < width - 2; i++)
            lcd_putc(inner_char_place);
        lcd_putc(end_char_place);
    }
    else if (progress_px < width_px - 4)
    {
        /*** 'block' characters ***/
        for (i = 0; i < 8; i++)
            custom_char_data[i] = 0x1F;
        lcd_define_custom_char(block_char_place, custom_char_data);

        /*** 'progress' character ***/
        line_pattern = 0;
        for (i = 0; i < (progress_px - 4) % 5; i++)
            line_pattern |= (1 << (4 - i));
        custom_char_data[0] = 0x1F; // first row
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = line_pattern;
        custom_char_data[custom_char_ptr] = 0x1F; // last row
        lcd_define_custom_char(progress_char_place, custom_char_data);

        /*** Inner characters ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = 0;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(inner_char_place, custom_char_data);

        /*** Ending character ***/
        custom_char_data[0] = 0x1F;
        custom_char_ptr = 1;
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = 0x01;
        custom_char_data[custom_char_ptr] = 0x1F;
        lcd_define_custom_char(end_char_place, custom_char_data);

        /*** Put characters ***/
        lcd_gotoxy(x, y);
        lcd_putc(block_char_place);
        block_count = 0;
        for (i = 0; i < (progress_px - 4) / 5; i++)
        {
            lcd_putc(block_char_place);
            block_count++;
        }
        lcd_putc(progress_char_place);
        for (i = 0; i < width - 2 - block_count - 1; i++)
            lcd_putc(inner_char_place);
        lcd_putc(end_char_place);
    }
    else
    {
        /*** 'block' character ***/
        custom_char_ptr = 0;
        for (i = 0; i < 8; i++)
            custom_char_data[custom_char_ptr++] = 0x1F;
        lcd_define_custom_char(block_char_place, custom_char_data);

        /*** 'progress' character ***/
        line_pattern = 0x01;
        custom_char_ptr = 0;
        for (i = 0; i < 4 - (width_px - progress_px); i++)
            line_pattern |= (1 << (4 - i));
        custom_char_data[custom_char_ptr++] = 0x1F; // first row
        for (i = 0; i < 6; i++)
            custom_char_data[custom_char_ptr++] = line_pattern;
        custom_char_data[custom_char_ptr] = 0x1F; // last row
        lcd_define_custom_char(progress_char_place, custom_char_data);

        /*** Put characters ***/
        lcd_gotoxy(x, y);
        for (i = 0; i < width - 1; i++)
            lcd_putc(block_char_place);
        lcd_putc(progress_char_place);
    }
}                                                                                         