#include <xc.h>
#include <stdint.h>
#include <stdlib.h>

// CONFIG1H
//#pragma config FOSC = INTIO67   // Oscillator Selection bits (Internal oscillator block, port function on RA6 and RA7)
#pragma config FOSC = HSPLL     // Oscillator Selection bits (HS oscillator, PLL enabled (Clock Frequency = 4 x FOSC1))
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 18        // Brown Out Reset Voltage bits (VBOR set to 1.8 V nominal)

// CONFIG2H
#pragma config WDTEN = OFF      // Watchdog Timer Enable bit (WDT is controlled by SWDTEN bit of the WDTCON register)
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
#pragma config PBADEN = ON      // PORTB A/D Enable bit (PORTB<4:0> pins are configured as analog input channels on Reset)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config HFOFST = ON      // HFINTOSC Fast Start-up (HFINTOSC starts clocking the CPU without waiting for the oscillator to stablize.)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RE3 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection Block 0 (Block 0 (000800-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection Block 3 (Block 3 (00C000-00FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection Block 0 (Block 0 (000800-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection Block 3 (Block 3 (00C000h-00FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection Block 0 (Block 0 (000800-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-0007FFh) not protected from table reads executed in other blocks)

//#define _XTAL_FREQ  44236800ul
#define _XTAL_FREQ  64000000ul


#define PIN_RS      LATDbits.LATD2
#define PIN_RST     LATDbits.LATD1
#define PIN_CS      LATDbits.LATD0

#define SPI_Write(byte) { \
    SSPIF = 0; \
    SSPBUF = byte; \
    while (!SSPIF); \
    SSPIF = 0; \
}

const unsigned char init_array_0[20]=
{0xEF, 0x00, 0xEE, 0x04, 0x1B, 0x04, 0xFE, 0xFE,
0xFE, 0xFE, 0xEF, 0x90, 0x4A, 0x04, 0x7F, 0x3F,
0xEE, 0x04, 0x43, 0x06};

const unsigned char init_array_1[46]=
{0xEF, 0x90, 0x09, 0x83, 0x08, 0x00, 0x0B, 0xAF,
0x0A, 0x00, 0x05, 0x00, 0x06, 0x00, 0x07, 0x00,
0xEF, 0x00, 0xEE, 0x0C, 0xEF, 0x90, 0x00, 0x80,
0xEF, 0xB0, 0x49, 0x02, 0xEF, 0x00, 0x7F, 0x01,
0xE1, 0x81, 0xE2, 0x02, 0xE2, 0x76, 0xE1, 0x83,
0x80, 0x01, 0xEF, 0x90, 0x00, 0x00};

typedef struct {
    unsigned char color;
    unsigned char is_empty;
    unsigned char is_dirty;
} block_t;

#define TABLE_WIDTH                     22
#define TABLE_HEIGHT                    16
block_t table[TABLE_WIDTH][TABLE_HEIGHT];

void lcd_draw_rect(char x1, char y1, char x2, char y2, char color);
void table_init()
{
    unsigned char i, j;

    block_t empty;
    empty.is_empty = 1;

    for (i = 0; i < TABLE_WIDTH; i++)
        for (j = 0; j < TABLE_HEIGHT; j++)
            table[i][j] = empty;
}

void table_draw()
{
    unsigned char i, j;
    for (i = 0; i < TABLE_WIDTH; i++)
        for (j = 0; j < TABLE_HEIGHT; j++) {
            block_t *b = &(table[i][j]);
            if (b->is_empty || !b->is_dirty)
                continue;
            unsigned char x = i * 8;
            unsigned char y = j * 8;
            b->is_dirty = 0;
            lcd_draw_rect(x, y, x + 6, y + 6, b->color);
            /*printf("%sblock %d;%d = 0x%02X\n", (b->is_empty ? "empty" : ""), j, i, b->color);
            delay(100);*/
        }
}

void send_command_byte(unsigned char cmd)
{
    PIN_RS = 1;
    PIN_CS = 0;

    SPI_Write(cmd);

    PIN_CS = 1;

//    digitalWrite(PIN_RS, HIGH);
//    digitalWrite(PIN_CS, LOW);
//    wiringPiSPIDataRW(0, &cmd, 1);
//    digitalWrite(PIN_CS, HIGH);
}

void send_command_word(unsigned int cmd)
{
    send_command_byte((unsigned char)((cmd >> 8) & 0xFF));
    send_command_byte((unsigned char)(cmd & 0xFF));
}

void send_data_byte(unsigned char data)
{
    PIN_RS = 0;
    PIN_CS = 0;

    SPI_Write(data);

    PIN_CS = 1;

//    digitalWrite(PIN_RS, LOW);
//    digitalWrite(PIN_CS, LOW);
//    wiringPiSPIDataRW(0, &data, 1);
//    digitalWrite(PIN_CS, HIGH);
}

void lcd_init()
{
    // Reset the LCD controller
//    digitalWrite(PIN_RST, LOW);
//    delay(1);
//    digitalWrite(PIN_RST, HIGH);

    PIN_RST = 0;
    __delay_ms(1);
    PIN_RST = 1;

    send_command_word(0xFDFD);
    send_command_word(0xFDFD);

    for (uint8_t i = 0; i < 8; ++i)
        __delay_ms(10);
    unsigned int i;
    for (i = 0; i < 20; i++)
        send_command_byte(init_array_0[i]);
    __delay_ms(10);
    for (i = 0; i < 46; i++)
        send_command_byte(init_array_1[i]);
}

void lcd_set_8bit_mode(char bgr)
{
    send_command_word(0xE800 + (unsigned int)(bgr & 1) * 0x0040);
}

void lcd_set_window(char x1, char y1, char x2, char y2)
{
    send_command_word(0x0500);
    send_command_word(0x0A00 + x1);
    send_command_word(0x0B00 + x2);
    send_command_word(0x0800 + y1);
    send_command_word(0x0900 + y2);
}

void lcd_draw_rect(char x1, char y1, char x2, char y2, char color)
{
    char x, y;

    lcd_set_window(x1, y1, x2, y2);
    for (y = y1; y <= y2; y++) {
        for (x = x1; x <= x2; x++) {
            send_data_byte(color);
        }
    }
}

void lcd_draw_rect_fast(char x1, char y1, char x2, char y2, char color)
{
    char x, y;

//    lcd_set_window(x1, y1, x2, y2);
    for (y = y1; y <= y2; y++) {
        for (x = x1; x <= x2; x++) {
            send_data_byte(color);
        }
    }
}

int main(int argc, char *argv[])
{
//    OSCCON = 0b01110000;
//    PLLEN = 1;

    LATD = 0;
    TRISD = 0;

    TRISC3 = 0;
    TRISC4 = 1;
    TRISC5 = 0;

    SSPCON1 = 0b00100001;
    CKE = 1;
    CKP = 0;

//    if (wiringPiSetup() < 0) {
//        printf("Failed to setup wiringPi\n");
//        return 1;
//    }
//
//    pinMode(PIN_CS, OUTPUT);
//    pinMode(PIN_RST, OUTPUT);
//    pinMode(PIN_RS, OUTPUT);
//
//    if (wiringPiSPISetup(0, 16000000) < 0) {
//        printf("Failed to setup SPI\n");
//        return 1;
//    }

    lcd_init();
    lcd_set_8bit_mode(0);
    lcd_draw_rect(0, 0, 175, 131, 0x00);

    send_command_word(0x24FF);

    uint8_t color;
    for (;;)
        lcd_draw_rect_fast(0, 0, 175, 131, color++);

    
    /*
    unsigned char color = 0;
    unsigned char line = 0;
    while (color < 255) {
        unsigned char x = (color % 22) * 8 + 1;
        unsigned char y = line * 8 + 1;
        lcd_draw_rect(x, y, x + 6, y + 6, color);
        color++;
        if (color % 22 == 0)
        line++;
    }

    lcd_draw_rect(0, 0, 175, 131, 0x00);
    */
//    time_t t;
//    time(&t);
    srand(12312312ul);

    /*table_init();
    unsigned char i, j;
    for (i = 0; i < TABLE_WIDTH; i++)
        for (j = 0; j < TABLE_HEIGHT; j++) {
            block_t *b = &(table[i][j]);
            b->color = 0xab;
            b->x = i;
            b->y = j;
            b->is_empty = 0;
        }
    table_draw();

    lcd_draw_rect(0, 0, 175, 131, 0x00);*/

    table_init();


    unsigned char cycles = rand() % 5;
    while (1) {
        //if (--cycles == 0) {
            //cycles = rand() % 10;

            // Add random block
            block_t *b = &(table[rand() % TABLE_WIDTH][rand() % TABLE_HEIGHT]);
            b->color = rand() % 256;
            b->is_empty = 0;
            b->is_dirty = 1;

            table_draw();
        //}
    }

    /*
    srand(12312545);
    while (1) {
        unsigned char x = rand() % 170;
        unsigned char y = rand() % 130;
        unsigned char d = rand() % 100;
        if (171 - x > 131 - y) {
            if (x + d > 171)
                d -= 171 - x;
        } else {
            if (y + d > 131)
                d -= 131 - y;
        }
        lcd_draw_rect(x, y, x + d, y + d, rand() % 255);
    }
    */

    return 0;
}