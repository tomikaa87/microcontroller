#ifndef _DHT1122_H
#define _DHT1122_H

// Error codes for readSensor()
#define DHT_OK                          0
#define DHT_TIMEOUT_ERROR               1
#define DHT_TEMP_RANGE_ERROR            2
#define DHT_CHECKSUM_ERROR              3
#define DHT_BIT_THRESHOLD_ERROR         4

char DHT_readSensor();

signed int DHT_relativeHumidityMul10();
signed int DHT_temperatureMul10();

unsigned char DHT_dataByte(unsigned char index);

#endif // _DHT1122_H