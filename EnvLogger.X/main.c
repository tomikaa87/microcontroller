/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using MPLAB(c) Code Configurator

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - v3.00
        Device            :  PIC18F26K22
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.20
*/

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

#include "mcc_generated_files/mcc.h"
#include "DHT1122.h"
#include "DS18X20.h"

#include <stdio.h>

#define MAX_SENSORS_COUNT 1

/*
                         Main application
 */
void main(void)
{
    ROMBits deviceRomBits[MAX_SENSORS_COUNT];
    ROMBytes deviceRomBytes[MAX_SENSORS_COUNT];
    
    SYSTEM_Initialize();
    
    // DS18X20 test code
    uint8_t deviceCount = DS18X20_SearchDevices(deviceRomBits, MAX_SENSORS_COUNT);
    // ---
    
    printf("Environment Logger initialized.\r\n");
    
    while (1)
    {
        SD_CS_Toggle();
        
        // DHT11 test code
        uint8_t dhtResult = DHT_readSensor();
        printf("DHT11 read result: %d; ", dhtResult);
        printf("Temperature (x10): %d; ", DHT_temperatureMul10());
        printf("Humidity (x10): %d\r\n", DHT_relativeHumidityMul10());
        // ---
        
        // DS18X20 test code
        printf("DS18B20 temperature: "); 
        int16_t temp = DS18X20_ReadSpecific(deviceRomBits[0]);
        printf("%d.%02d C\r\n", temp / 100, temp % 100);   
        // ---
        
        for (uint8_t i = 0; i < 100; ++i)
            __delay_ms(10);
    }
}
/**
 End of File
*/