/*
 * Weather Station Ethernet Board
 * 1-Wire driver for on-board DS18X20 sensor
 *
 * Author: Tamas Karpati
 * Date: 2010-09-15
 */
 
// #include "HardwareProfile.h"
#include "mcc_generated_files/pin_manager.h"

#ifndef __ONEWIRE_H
#define __ONEWIRE_H

#define ONE_WIRE_PIN    OW_DQ_PORT
#define ONE_WIRE_TRIS   OW_DQ_TRIS

#ifndef ONE_WIRE_PIN
    #error "OneWire I/O Pin undefined! You must define ONE_WIRE_PIN to make 1-Wire work."
#endif

#ifndef ONE_WIRE_TRIS
    #error "OneWire I/O Pin TRIS undefined! You must define ONE_WIRE_TRIS to make 1-Wire work."
#endif

#define OW_FLOAT()  (ONE_WIRE_TRIS = 1)
#define OW_LOW()    (ONE_WIRE_PIN = 0, ONE_WIRE_TRIS = 0)
#define OW_HIGH()   (ONE_WIRE_PIN = 1, ONE_WIRE_TRIS = 0)

unsigned char OneWireReset();
void OneWireWriteBit(unsigned char b);
void OneWireWriteByte(unsigned char b);
unsigned char OneWireReadBit();
unsigned char OneWireReadByte();

#endif // _ONEWIRE_H
