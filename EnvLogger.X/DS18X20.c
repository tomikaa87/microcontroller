/*
 * Weather Station Ethernet Board
 * 1-Wire driver for on-board DS18X20 sensor
 *
 * Author: Tamas Karpati
 * Date: 2010-09-15
 */

#include "DS18X20.h"
#include "OneWire.h"
//#include "HardwareProfile.h"
#include "mcc_generated_files/mcc.h"

#include <stdio.h>
#include <stdint.h>

#define Delay10us(x) { for (uint8_t i = 0; i < x; ++i) __delay_us(10); }

unsigned char LastDiscrepancy;
bit DoneFlag;

/*
 * Reads the current temperature value from
 * the DS18X20 1-wire sensor
 */
signed int DS18X20Read()
{
    unsigned char hi, lo, tempWhole;
    unsigned int iTemp, tempFraction;
    signed int temp;
    //float fTemp;
    
    OneWireReset();
    OneWireWriteByte(0xCC);
    OneWireWriteByte(0x44);
    
    Delay10us(20);
    
    OneWireReset();
    OneWireWriteByte(0xCC);
    OneWireWriteByte(0xBE);
    lo = OneWireReadByte();
    hi = OneWireReadByte();
    iTemp = (hi << 8) + lo;
    
    if (iTemp & 0x8000)
    {
        iTemp = ~iTemp + 1;
    }
    
    temp = 0;
    
    tempWhole = iTemp >> (TEMP_RESOLUTION - 8);
    temp += tempWhole * 100;
    
    tempFraction = iTemp << (4 - (TEMP_RESOLUTION - 8));
    tempFraction &= 0x000F;
    tempFraction *= 625;
    temp += tempFraction / 100;
    
    if (iTemp & 0x8000)
    {
        temp *= -1;
    }

    return temp;
} 

signed int DS18X20_ReadSpecific(unsigned char *romBits)
{
    unsigned char hi, lo, tempWhole, i;
    unsigned int iTemp, tempFraction;
    signed int temp;
    //float fTemp;
    
    OneWireReset();
    
    OneWireWriteByte(0x55);
    // Send 64 bit ROM code
    for (i = 0; i < 64; i++)
    {
        OneWireWriteBit(romBits[i]);   
    }    
    
    OneWireWriteByte(0x44);
    
    Delay10us(20);
    
    OneWireReset();
    
    OneWireWriteByte(0x55);
    // Send 64 bit ROM code
    for (i = 0; i < 64; i++)
    {
        OneWireWriteBit(romBits[i]);   
    } 
    
    OneWireWriteByte(0xBE);
    lo = OneWireReadByte();
    hi = OneWireReadByte();
    iTemp = (hi << 8) + lo;
    
    if (iTemp & 0x8000)
    {
        iTemp = ~iTemp + 1;
    }
    
    temp = 0;
    
    tempWhole = iTemp >> (TEMP_RESOLUTION - 8);
    temp += tempWhole * 100;
    
    tempFraction = iTemp << (4 - (TEMP_RESOLUTION - 8));
    tempFraction &= 0x000F;
    tempFraction *= 625;
    temp += tempFraction / 100;
    
    if (iTemp & 0x8000)
    {
        temp *= -1;
    }

    return temp;
}    

unsigned char DS18X20_NextDevice(unsigned char *romBits)
{
    unsigned char result;
    unsigned char presenceBit;
    unsigned char romBitIndex;
    unsigned char discrepancyMarker;
    unsigned char bit1, bit2;
    //unsigned char romBits[64];
    
    result = 0;
    
    if (DoneFlag)
    {
        DoneFlag = 0;
        return result;
    }
    
    #ifdef DEBUG
    printf("\r\n[SRCHROM] **** Looking for new device ****\r\n");
    #endif

    // Send reset signal
    presenceBit = OneWireReset() == 0;
    
    #ifdef DEBUG
    printf("[SRCHROM] Presence bit: %d\r\n", presenceBit);
    #endif
    
    if (presenceBit == 0)
    {
        #ifdef DEBUG
        printf("[SRCHROM] Presence bit is '0'\r\n");   
        #endif
        
        LastDiscrepancy = 0;
        return result;
    }    
    
    romBitIndex = 0;
    discrepancyMarker = 0;
    
    // Send Search ROM command
    #ifdef DEBUG
    printf("[SRCHROM] Sending SEARCH ROM command...\r\n");
    #endif
    
    OneWireWriteByte(0xF0);
    
    while (romBitIndex < 64)
    {
        #ifdef DEBUG
        printf("[SRCHROM] ROM bit index: %d\r\n", romBitIndex);
        printf("[SRCHROM] LastDiscrepancy: %d\r\n", LastDiscrepancy);
        #endif
        
        bit1 = OneWireReadBit();
        bit2 = OneWireReadBit();
        
        #ifdef DEBUG
        printf("[SRCHROM] Read bits: %d, %d\r\n", bit1, bit2);
        #endif
        
        if (bit1 == 1 && bit2 == 1)
        {
            LastDiscrepancy = 0; 
            
            #ifdef DEBUG
            printf("[SRCHROM] bit1 == bit2 == 1, result = %d\r\n", result);
            #endif
            
            return result;  
        }    
        
        if (bit1 == 0 && bit2 == 0)
        {
            #ifdef DEBUG
            printf("[SRCHROM] bit1 == bit2 == 0, result = %d\r\n", result);
            #endif
            
            if (romBitIndex == LastDiscrepancy)
            {
                #ifdef DEBUG
                printf("[SRCHROM] romBitIndex == LastDiscrepancy\r\n");
                #endif
                
                romBits[romBitIndex] = 1;
                
                #ifdef DEBUG
                printf("[SRCHROM] Sending bit '%d'\r\n", romBits[romBitIndex]);
                #endif
                
                OneWireWriteBit(romBits[romBitIndex]);
                romBitIndex++;
            }
            else if (romBitIndex > LastDiscrepancy)
            {
                #ifdef DEBUG
                printf("[SRCHROM] romBitIndex > LastDiscrepancy\r\n");
                #endif
                
                romBits[romBitIndex] = 0;
                discrepancyMarker = romBitIndex;
                
                #ifdef DEBUG
                printf("[SRCHROM] Sending bit '%d'\r\n", romBits[romBitIndex]);
                #endif
                
                OneWireWriteBit(0);
                romBitIndex++;
            }
            else
            {
                if (romBits[romBitIndex] == 0)
                {
                    #ifdef DEBUG
                    printf("[SRCHROM] romBits[romBitIndex] == 0\r\n");
                    #endif
                    
                    discrepancyMarker = romBitIndex;
                }    
                
                #ifdef DEBUG
                printf("[SRCHROM] Sending bit '%d'\r\n", romBits[romBitIndex]);
                #endif
                
                OneWireWriteBit(romBits[romBitIndex]);
                romBitIndex++;
            }          
        }
        else
        {
            #ifdef DEBUG
            printf("[SRCHROM] bit1 != bit2\r\n");
            #endif
            
            romBits[romBitIndex] = bit1;
            
            #ifdef DEBUG
            printf("[SRCHROM] Sending bit '%d'\r\n", romBits[romBitIndex]);
            #endif
            
            OneWireWriteBit(bit1);
            romBitIndex++;
        }        
    } 
    
    #ifdef DEBUG
    printf("[SRCHROM] Discrepancy marker: %d\r\n", discrepancyMarker);
    #endif
    
    LastDiscrepancy = discrepancyMarker;
    
    if (LastDiscrepancy == 0)
    {
        #ifdef DEBUG
        printf("[SRCHROM] Setting DONE flag\r\n");
        #endif
        
        DoneFlag = 1;   
        result = 1;
    }
    else
    {
        result = 1;
    }        

    return result;       
}    

unsigned char DS18X20_FirstDevice(unsigned char *romBits)
{
    LastDiscrepancy = 0;
    DoneFlag = 0;
    
    return DS18X20_NextDevice(romBits);
}    

void DS18X20_ROMBitsToROMBytes(unsigned char *romBits, unsigned char *romBytes)
{
    unsigned char i, j;
    
    for (i = 0; i < 8; i++)
    {
        romBytes[i] = 0;
        
        for (j = 0; j < 8; j++)
        {
            if (romBits[i * 8 + j])
            {
                romBytes[i] += (1 << j);
            }       
        }       
    }     
}    

unsigned char DS18X20_SearchDevices(ROMBits *romBits, unsigned char maxDevices)
{
    unsigned int path;
    unsigned int next, pos, count;
    unsigned char bit1, bit2;
    unsigned char deviceCount;
    
    path = 0;
    deviceCount = 0;

    do
    {
        if (OneWireReset())
        {
            #ifdef DEBUG
            printf("[SRCHDEV] Presence signal error\r\n");
            #endif
            
            return 0;   
        }   
            
        #ifdef DEBUG
        printf("[SRCHDEV] Sending SEARCH ROM command\r\n");
        #endif
        OneWireWriteByte(0xF0);
    
        next = 0;
        pos = 1;
        count = 0;
    
        do
        {
            bit1 = OneWireReadBit();
            bit2 = OneWireReadBit();
            
            if (bit1 == 0 && bit2 == 0)
            {
                if (pos & path)
                    bit1 = 1;
                else
                    next = (path & (pos - 1)) | pos;
                
                pos <<= 1;   
            }    
            
            OneWireWriteBit(bit1);
            // Save bit1 as ROM value bit
            romBits[deviceCount][count] = bit1;
            
            count++;
        }
        while (count < 64); 
        
        path = next;
        
        #ifdef DEBUG
        printf("[SRCHDEV] Device found.\r\n"); 
        printf("[SRCHDEV] Device info:\r\n");
        DS18X20_PrintDeviceInfo(romBits[deviceCount]);
        #endif
        
        deviceCount++;
    }
    while (path && deviceCount <= maxDevices);    
    
    return deviceCount;
}    

void DS18X20_PrintDeviceInfo(unsigned char *romBits)
{
    ROMBytes romBytes;
    unsigned char i;
    
    // Convert ROM bits to bytes
    DS18X20_ROMBitsToROMBytes(romBits, romBytes);
    
    // Print device type
    switch (romBytes[0])
    {
        case 0x10:
            printf("Device type: DS18S20 (Family: 10)\r\n");
            break;
            
        case 0x28:
            printf("Device type: DS18B20 (Family: 28)\r\n");
            break;
            
        default:
            printf("Device type: UNKNOWN (Family: %02X)\r\n", romBytes[0]);
            break; 
    }
    
    // Print serial
    printf("Device serial: ");
    for (i = 1; i < 7; i++)
        printf("%02X ", romBytes[i]);
    
    // Print CRC
    printf("\r\nDevice ROM data CRC: %02X\r\n", romBytes[7]);        
}    
