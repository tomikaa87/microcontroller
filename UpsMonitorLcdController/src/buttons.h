#pragma once

#include <stdint.h>

class Buttons
{
public:
    typedef void(*ButtonPressedCallback)(void);

    Buttons();

    void task();

    void setButton1PressedCallback(ButtonPressedCallback callback);
    void setButton2PressedCallback(ButtonPressedCallback callback);

private:
    ButtonPressedCallback m_button1Callback = nullptr;
    ButtonPressedCallback m_button2Callback = nullptr;
    bool m_buttonPressed = false;
    uint32_t m_lastSampleTime = 0;

    bool isButtonPressed(uint8_t buttonPin);
};