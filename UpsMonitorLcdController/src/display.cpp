#include "display.h"

static const uint8_t RSPin = 2;
static const uint8_t RWPin = 3;
static const uint8_t ENPin = 4;
static const uint8_t D4Pin = 5;
static const uint8_t D5Pin = 6;
static const uint8_t D6Pin = 7;
static const uint8_t D7Pin = 8;
static const uint8_t BacklightPin = 9;

static const uint8_t DisplayCols = 16;
static const uint8_t DisplayRows = 2;

namespace Glyphs
{
    static uint8_t UPS_1[] = {
        0b00001100,
        0b00011111,
        0b00010000,
        0b00010010,
        0b00010111,
        0b00010010,
        0b00010000,
        0b00011111
    };

    static uint8_t UPS_2[] = {
        0b00000110,
        0b00011111,
        0b00000001,
        0b00000001,
        0b00011101,
        0b00000001,
        0b00000001,
        0b00011111
    };

    static uint8_t Battery_1[] = {
        0b00000000,
        0b00011111,
        0b00010000,
        0b00010000,
        0b00010000,
        0b00010000,
        0b00011111,
        0b00000000
    };

    static uint8_t Battery_2[] = {
        0b00000000,
        0b00011110,
        0b00000011,
        0b00000011,
        0b00000011,
        0b00000011,
        0b00011110,
        0b00000000
    };

    static uint8_t Output_1[] = {
        0b00000000,
        0b00011111,
        0b00010001,
        0b00010001,
        0b00010111,
        0b00010001,
        0b00011111,
        0b00000000
    };

    static uint8_t Output_2[] = {
        0b00000000,
        0b00000000,
        0b00001000,
        0b00000100,
        0b00011110,
        0b00000100,
        0b00001000,
        0b00000000
    };
}

Display::Display()
    : m_lcd(RSPin, RWPin, ENPin, D4Pin, D5Pin, D6Pin, D7Pin, BacklightPin, POSITIVE)
{
    m_lcd.begin(DisplayCols, DisplayRows);
    m_lcd.clear();
}

void Display::paintMainScreen()
{
    m_lcd.clear();
    m_lcd.setBacklight(128);

    // Upload custom glyphs
    m_lcd.createChar(0, Glyphs::UPS_1);
    m_lcd.createChar(1, Glyphs::UPS_2);
    m_lcd.createChar(2, Glyphs::Battery_1);
    m_lcd.createChar(3, Glyphs::Battery_2);
    m_lcd.createChar(4, Glyphs::Output_1);
    m_lcd.createChar(5, Glyphs::Output_2);

    // Battery
    m_lcd.setCursor(0, 0);
    m_lcd.print(char(2));
    m_lcd.print(char(3));

    // Output
    m_lcd.setCursor(9, 0);
    m_lcd.print(char(4));
    m_lcd.print(char(5));

    // UPS
    m_lcd.setCursor(0, 1);
    m_lcd.print(char(0));
    m_lcd.print(char(1));
    m_lcd.setCursor(2, 1);
    m_lcd.print("C:     L:    ");
}

void Display::updateBatteryVoltage(long milliVolts)
{
    m_lcd.setCursor(2, 0);
    printVoltage(milliVolts);
}

void Display::updateOutputVoltage(float milliVolts)
{
    m_lcd.setCursor(11, 0);
    printVoltage(milliVolts);
}

void Display::updateUpsBatteryCharge(uint8_t percent)
{
    m_lcd.setCursor(4, 1);
    printPercentage(percent);
}

void Display::updateUpsLoad(uint8_t percent)
{
    m_lcd.setCursor(11, 1);
    printPercentage(percent);
}

void Display::printVoltage(uint16_t milliVolts)
{
    m_lcd.print(milliVolts / 1000);
    m_lcd.print('.');

    uint8_t fractional = milliVolts % 1000 / 10;
    if (fractional < 10)
        m_lcd.print('0');
    m_lcd.print(fractional);
    m_lcd.print('V');
}

void Display::printPercentage(uint8_t value)
{
    if (value > 100)
        value = 100;

    if (value < 10)
        m_lcd.print(' ');

    if (value < 100)
        m_lcd.print(' ');

    m_lcd.print(value);
    m_lcd.print('%');
}
