#pragma once

#include <stdint.h>

class Analog
{
public:
    uint16_t measureVccMilliVolts();
    uint16_t measureBatteryMilliVolts(uint16_t vccMilliVolts);

private:
    uint16_t readVccFromADC();
    uint16_t readBatteryVoltageFromADC(uint16_t vccMilliVolts);
};