// Necessary for successful compilation (PlatformIO bug)
#include <Wire.h>

#include <Arduino.h>

#include "adc.h"
#include "buttons.h"
#include "display.h"
#include "terminal.h"

Analog* g_analog = nullptr;
Buttons* g_buttons = nullptr;
Display* g_display = nullptr;
Terminal* g_terminal = nullptr;

void setup()
{
    g_display = new Display;
    g_display->paintMainScreen();

    g_analog = new Analog;

    g_terminal = new Terminal;
    g_terminal->setUPSDataCallback([](uint8_t charge, uint8_t load) {
        g_display->updateUpsBatteryCharge(charge);
        g_display->updateUpsLoad(load);
    });

    g_display->updateBatteryVoltage(0);
    g_display->updateOutputVoltage(0);
    g_display->updateUpsBatteryCharge(0);
    g_display->updateUpsLoad(0);

    g_buttons = new Buttons;
    
    g_buttons->setButton1PressedCallback([] {
        
    });

    g_buttons->setButton2PressedCallback([] {
        
    });
}

void loop()
{
    static uint32_t lastMillis = 0;

    if (millis() - lastMillis > 1000)
    {
        lastMillis = millis();

        uint16_t vcc = g_analog->measureVccMilliVolts();
        uint16_t batteryVoltage = g_analog->measureBatteryMilliVolts(vcc);

        g_display->updateBatteryVoltage(batteryVoltage);
        g_display->updateOutputVoltage(vcc);
    }

    g_terminal->task();
    g_buttons->task();
}