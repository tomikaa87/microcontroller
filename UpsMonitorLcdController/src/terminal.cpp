#include "terminal.h"

#include <Arduino.h>

static const uint32_t SerialBaudRate = 9600;

Terminal::Terminal()
{
    Serial.begin(SerialBaudRate);
}

void Terminal::task()
{
    if (!Serial.available())
        return;

    if (bufferInput(Serial.read()))
    {
        char* token = strtok(m_buffer, ":");
        uint8_t charge = token ? atoi(token) : 0;

        token = strtok(nullptr, ":");
        uint8_t load = token ? atoi(token) : 0;

        if (m_upsDataCallback)
            m_upsDataCallback(charge, load);

        resetBuffer();
    }
}

void Terminal::setUPSDataCallback(UPSDataCallback callback)
{
    m_upsDataCallback = callback;
}

void Terminal::resetBuffer()
{
    m_bufPtr = 0;
    memset(m_buffer, 0, sizeof(m_buffer));
}

bool Terminal::bufferInput(const char input)
{
    if (input == '*')
    {
        resetBuffer();
    }
    else if (input == ';' && m_bufPtr > 0)
    {
        return true;
    }
    else
    {
        if (m_bufPtr < sizeof(m_buffer) - 2)
        {
            m_buffer[m_bufPtr++] = input;
        }
    }

    return false;
}