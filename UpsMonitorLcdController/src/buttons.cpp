#include "buttons.h"

#include <Arduino.h>

static const uint8_t Button1Pin = 11;
static const uint8_t Button2Pin = 10;

Buttons::Buttons()
{
    pinMode(Button1Pin, INPUT_PULLUP);
    pinMode(Button2Pin, INPUT_PULLUP);
}

void Buttons::task()
{
    if (millis() - m_lastSampleTime < 100)
        return;

    if (isButtonPressed(Button1Pin) && !m_buttonPressed)
    {
        m_buttonPressed = true;
        if (m_button1Callback)
            m_button1Callback();
    }

    if (isButtonPressed(Button2Pin) && !m_buttonPressed)
    {
        m_buttonPressed = true;
        if (m_button2Callback)
            m_button2Callback();
    }

    if (m_buttonPressed && !isButtonPressed(Button1Pin) && !isButtonPressed(Button2Pin))
        m_buttonPressed = false;
}

void Buttons::setButton1PressedCallback(ButtonPressedCallback callback)
{
    m_button1Callback = callback;
}

void Buttons::setButton2PressedCallback(ButtonPressedCallback callback)
{
    m_button2Callback = callback;
}

bool Buttons::isButtonPressed(uint8_t buttonPin)
{
    uint8_t sum = 0;
    for (uint8_t i = 0; i < 8; ++i)
    {
        sum += digitalRead(buttonPin) == 0 ? 1 : 0;
        delay(1);
    }

    return sum > 4;
}
