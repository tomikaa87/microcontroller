#pragma once

#include <stdint.h>

class Terminal
{
public:
    Terminal();

    void task();

    typedef void(*UPSDataCallback)(uint8_t charge, uint8_t load);
    void setUPSDataCallback(UPSDataCallback callback);

private:
    UPSDataCallback m_upsDataCallback = nullptr;
    uint8_t m_bufPtr = 0;
    char m_buffer[64] = { 0 };

    void resetBuffer();
    bool bufferInput(const char input);
};