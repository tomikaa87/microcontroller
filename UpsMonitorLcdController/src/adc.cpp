#include "adc.h"

#include <Wire.h>
#include <Arduino.h>

static const float InternalReferenceVoltage = 1.07;
static const uint8_t SampleCount = 8;

uint16_t Analog::measureVccMilliVolts()
{
    uint32_t sum = 0;
    for (uint8_t i = 0; i < SampleCount; ++i)
        sum += readVccFromADC();
    return sum / SampleCount;
}

uint16_t Analog::measureBatteryMilliVolts(uint16_t vccMilliVolts)
{
    uint32_t sum = 0;
    for (uint8_t i = 0; i < SampleCount; ++i)
        sum += readBatteryVoltageFromADC(vccMilliVolts);
    return sum / SampleCount;
}

uint16_t Analog::readVccFromADC()
{
    double result;

    // Read 1.1V reference against AVcc 
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
    delay(2); // Wait for Vref to settle 
    ADCSRA |= _BV(ADSC); // Convert 
    while (bit_is_set(ADCSRA, ADSC));

    result = ADCL | (ADCH << 8);

    //Serial.print("readVcc ADC result: ");
    //Serial.println(result);

    result = InternalReferenceVoltage * 1023.0 * 1000.0 / result; // Back-calculate AVcc in mV 

    return result;
}

uint16_t Analog::readBatteryVoltageFromADC(uint16_t vccMilliVolts)
{
    float result;

    analogReference(DEFAULT);
    result = analogRead(0);

    //Serial.print("readBattV ADC result: ");
    //Serial.println(result);

    // Result is now referenced to Vcc and must be corrected
    result = vccMilliVolts * result / 1023.0;

    return result;
}
