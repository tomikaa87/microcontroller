#pragma once

#include <stdint.h>
#include <LiquidCrystal.h>

class Display
{
public:
    Display();

    void paintMainScreen();

    void updateBatteryVoltage(long milliVolts);
    void updateOutputVoltage(float milliVolts);
    void updateUpsBatteryCharge(uint8_t percent);
    void updateUpsLoad(uint8_t percent);

private:
    LiquidCrystal m_lcd;

    void printVoltage(uint16_t milliVolts);
    void printPercentage(uint8_t value);
};