/*
 * Weather Station LCD Board
 * T6963C LCD Display Driver
 *
 * Author: Tamas Karpati
 * Date: 2010-09-21
 */

#ifndef __T6963C_H
#define __T6963C_H

#include "HardwareProfile.h"

// Color Constants
#define GLCD_COLOR_BLACK            1
#define GLCD_COLOR_WHITE            0

// Font Constants
#define GLCD_FONT_8x8               0
#define GLCD_FONT_6x8               1

void glcd_initialize(unsigned char font);

void glcd_putc(char c);
void glcd_puts(char *str);

void glcd_set_address_ptr(unsigned int address);

void glcd_write_byte(unsigned char byte);
void glcd_write_byte_inc(unsigned char byte);

void glcd_write_buf(unsigned char invert);

void glcd_clear();
void glcd_clear_buf();

void glcd_gotoxy(int x, int y, unsigned char text);

void glcd_set_pixel(unsigned char x, unsigned char y, unsigned char on);
unsigned char glcd_get_pixel(unsigned char x, unsigned char y);

void glcd_draw_rect(unsigned char x1, unsigned char y1, unsigned char x2, 
        unsigned char y2, unsigned char fill, unsigned char color);
void glcd_draw_image(unsigned char *image, unsigned char width, 
        unsigned char height, unsigned char x, unsigned char y,
        unsigned char invert);
void glcd_draw_bar(unsigned char value, unsigned char max, 
        unsigned char x, unsigned char y, unsigned char width, 
        unsigned char height, unsigned char invert);
void glcd_draw_line(unsigned char x1, unsigned char y1, unsigned char x2,
        unsigned char y2, unsigned char color);
void glcd_draw_rect_inv(unsigned char x1, unsigned char y1, unsigned char x2,
        unsigned char y2);

#endif // __T6963C_H
