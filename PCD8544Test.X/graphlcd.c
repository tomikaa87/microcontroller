/******************************************************************************
 * GLCD library                                                               *
 *                                                                            *
 * \author Tamas Karpati                                                      *
 * \version 1.0.0                                                             *
 * \date 2011-09-29                                                           *
 ******************************************************************************/

#include <stdlib.h>

#include "graphlcd.h"

/**
 * Draws a rectangle on the LCD (or in the screen buffer). The top-left corner
 * of the rectangle is defined by @p x1 and @p y1, the bottom-right corner by
 * @p x2 and @p y2. If the @p fill parameter is 1, the rectangle will be filled
 * with the given @p color, otherwise only the frame will be drawn.
 */
void glcd_draw_rect(unsigned char x1, unsigned char y1, unsigned char x2,
        unsigned char y2, unsigned char fill, unsigned char color)
{
    unsigned char i, j;

    if (fill) {
        for (i = x1; i <= x2; i++)
            for (j = y1; j <= y2; j++)
                glcd_set_pixel(i, j, color);
    } else {
        for (i = x1; i <= x2; i++) {
            glcd_set_pixel(i, y1, color);
            glcd_set_pixel(i, y2, color);
        }
        for (i = y1; i <= y2; i++) {
            glcd_set_pixel(x1, i, color);
            glcd_set_pixel(x2, i, color);
        }
    }
}

/**
 * Draws a picture on the LCD to the given @p x and @p y coordinates. The
 * size of the picture is held by @p width and @p height. If the @p invert is
 * set to 1, the picture will be drawn inverted.
 */
void glcd_draw_image(unsigned char *image, unsigned char width,
        unsigned char height, unsigned char x, unsigned char y,
        unsigned char invert)
{
    unsigned char i, j, k;

    for (i = 0; i < height; i++)
        for (j = 0; j < width; j++) {
            k = j + i * width;
            glcd_set_pixel(j + x, i + y, invert ? 1 - image[k] : image[k]);
        }
}

/**
 * Draws a progess bar to the given @p x and @p y coordinates. The @p value
 * holds the current value of the bar, the @p max determines the maximum value.
 * The @p width and @p height parameters hold the size of the bar. If the
 * @p invert is set to 1, the bar will be drawn inverted.
 */
void glcd_draw_bar(unsigned char value, unsigned char max, unsigned char x,
        unsigned char y, unsigned char width, unsigned char height,
        unsigned char invert)
{
    unsigned char length;

    length = (unsigned char) ((float) value / (float) max * (float) width);

    glcd_draw_rect(x, y, length + x - 1, height + y - 1, 1, 1 - invert);

    if (length < width - 1)
        glcd_draw_rect(x + length, y, x + width - 1, height + y - 1, 1, invert);
}

/**
 * Draws a line using Bresenham's line algorithm from the given @p x1 and @p y1
 * corrdinates to the given @p x2 and @p y2 coordinates. The @p color parameter
 * determines the color of the line (1 = pixel on, 0 = pixel off).
 */
void glcd_draw_line(unsigned char x1, unsigned char y1, unsigned char x2,
        unsigned char y2, unsigned char color)
{
    unsigned char steep;
    unsigned char tmp, delta_x, delta_y, x, y;
    signed char y_step, error;

    steep = abs(y2 - y1) > abs(x2 - x1);

    if (steep) {
        tmp = x1;
        x1 = y1;
        y1 = tmp;

        tmp = x2;
        x2 = y2;
        y2 = tmp;
    }

    if (x1 > x2) {
        tmp = x1;
        x1 = x2;
        x2 = tmp;

        tmp = y1;
        y1 = y2;
        y2 = tmp;
    }

    delta_x = x2 - x1;
    delta_y = abs(y2 - y1);
    error = delta_x / 2;
    y = y1;

    if (y1 < y2)
        y_step = 1;
    else
        y_step = -1;

    for (x = x1; x <= x2; x++) {
        if (steep)
            glcd_set_pixel(y, x, color);
        else
            glcd_set_pixel(x, y, color);

        error = error - delta_y;
        if (error < 0) {
            y = y + y_step;
            error = error + delta_x;
        }
    }
}

/**
 * Draws a filled rectangle on the LCD (or in the screen buffer).
 * The top-left corner of the rectangle is defined by @p x1 and @p y1,
 * the bottom-right corner by @p x2 and @p y2. The color of the rectangle's
 * pixels will be the inverse of the corresponding pixel of the background.
 */
void glcd_draw_rect_inv(unsigned char x1, unsigned char y1, unsigned char x2,
        unsigned char y2)
{
    unsigned char x, y, x_start, x_end, y_start, y_end;

    x_start = x1 <= x2 ? x1 : x2;
    x_end = x1 >= x2 ? x1 : x2;
    y_start = y1 <= y2 ? y1 : y2;
    y_end = y1 >= y2 ? y1 : y2;

    for (x = x_start; x <= x_end; x++)
        for (y = y_start; y <= y_end; y++)
            glcd_set_pixel(x, y, !glcd_get_pixel(x, y));
}