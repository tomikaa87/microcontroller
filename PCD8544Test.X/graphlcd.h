/**
 * Multi-driver Graphic LCD library
 *
 * \author Tamas Karpati
 * \date 2011-09-29
 * \version 1.0.0
 */

#ifndef GRAPHLCD_H
#define GRAPHLCD_H

#include <htc.h>

/*** DRIVER CONFIGURATION *****************************************************/

/*
 * Uncomment the following define corresponding to the type of the LCD module
 */
//#define GLCD_MODULE_T6963C
#define GLCD_MODULE_PCD8544

/*
 * Ucomment the following define to use TEXT mode of T6963C
 */
//#define GLCD_MODULE_T6963C_TEXT_MODE

#ifdef GLCD_MODULE_T6963C_TEXT_MODE
    #define GLCD_MODULE_T6963C_FONT_6x8
#endif

/*
 * Change these values corresponding to the LCD's resolution
 */
//#define GLCD_RESOLUTION_X                   128
//#define GLCD_RESOLUTION_Y                   128
#define GLCD_RESOLUTION_X                   84
#define GLCD_RESOLUTION_Y                   48

/*
 * Uncomment this define to use buffered drawing.
 * Note, that PCD8544 must use a buffer (504 bytes of RAM needed).
 */
#define GLCD_USE_DRAWING_BUFFER

/*** SANITY CHECK *************************************************************/

#ifdef GLCD_MODULE_T6963C
    #ifdef GLCD_MODULE_PCD8544
        #define GLCD_MULTI_MODULE_ERROR
    #endif
#endif

#ifdef GLCD_MODULE_PCD8544
    #ifdef GLCD_MODULE_T6963C
        #define GLCD_MULTI_MODULE_ERROR
    #endif
#endif

#ifdef GLCD_MULTI_MODULE_ERROR
    #error Only one module can be used at a time.
#endif

#ifdef GLCD_MODULE_PCD8544
    #ifndef GLCD_USE_DRAWING_BUFFER
        #define GLCD_USE_DRAWING_BUFFER
    #endif
#endif

#ifdef GLCD_USE_DRAWING_BUFFER
    #define GLCD_BUFFER_SIZE (GLCD_RESOLUTION_X * GLCD_RESOLUTION_Y / 8)
#endif // GLCD_USE_DRAWING_BUFFER

/*** HARDWARE CONFIGURATION ***************************************************/

/**
 * Change this value corresponding to PIC's Fosc.
 */
#ifndef _XTAL_FREQ
    #define _XTAL_FREQ                      64000000ul
#endif

#if (defined GLCD_MODULE_T6963C)
    #define GLCD_DATA                       (LATB)
    #define GLCD_DATA_READ                  (PORTB)
    #define GLCD_DATA_TRIS                  (TRISB)
    #define GLCD_WR                         (LATD0)
    #define GLCD_WR_TRIS                    (TRISD0)
    #define GLCD_RD                         (LATD1)
    #define GLCD_RD_TRIS                    (TRISD1)
    #define GLCD_CE                         (LATD2)
    #define GLCD_CE_TRIS                    (TRISD2)
    #define GLCD_CD                         (LATD3)
    #define GLCD_CD_TRIS                    (TRISD3)
    #define GLCD_RST                        (LATD4)
    #define GLCD_RST_TRIS                   (TRISD4)
    #define GLCD_FS                         (LATD5)
    #define GLCD_FS_TRIS                    (TRISD5)

    #define GLCD_STA0                       (RB0)
    #define GLCD_STA1                       (RB1)
    #define GLCD_STA2                       (RB2)
    #define GLCD_STA3                       (RB3)
    #define GLCD_STA4                       (RB4)
    #define GLCD_STA5                       (RB5)
    #define GLCD_STA6                       (RB6)
    #define GLCD_STA7                       (RB7)
#elif (defined GLCD_MODULE_PCD8544)
    #define PCD_GLCD_USE_HW_SPI

    /**
     * Uncomment the define corresponding to the desired SPI clock frequency.
     * PCD8544 can handle maximum 4 MHz.
     */
    //#define PCD_GLCD_SPI_CLK_FOSC_64
    #define PCD_GLCD_SPI_CLK_FOSC_16
    //#define PCD_GLCD_SPI_CLK_FOSC_4

    #define PCD_GLCD_SCK                    (LATC3)
    #define PCD_GLCD_SCK_TRIS               (TRISC3)
    #define PCD_GLCD_MOSI                   (LATC5)
    #define PCD_GLCD_MOSI_TRIS              (TRISC5)
    #define PCD_GLCD_SCE                    (LATD0)
    #define PCD_GLCD_SCE_TRIS               (TRISD0)
    #define PCD_GLCD_RST                    (LATD1)
    #define PCD_GLCD_RST_TRIS               (TRISD1)
    #define PCD_GLCD_DC                     (LATD2)
    #define PCD_GLCD_DC_TRIS                (TRISD2)
    #define PCD_GLCD_SSPCON1                (SSPCON1)
    #define PCD_GLCD_SSPBUF                 (SSPBUF)
    #define PCD_GLCD_SSPIF                  (SSPIF)
#endif

/*** GLOBAL CONSTANTS *********************************************************/

// Color Constants
#define GLCD_COLOR_BLACK            1
#define GLCD_COLOR_WHITE            0

/*** API FUNCTIONS ************************************************************/

void glcd_initialize();

void glcd_write_buf(unsigned char invert);

void glcd_clear();
void glcd_clear_buf();

void glcd_gotoxy(unsigned char x, unsigned char y, unsigned char text);

void glcd_set_pixel(unsigned char x, unsigned char y, unsigned char on);
unsigned char glcd_get_pixel(unsigned char x, unsigned char y);

void glcd_draw_rect(unsigned char x1, unsigned char y1, unsigned char x2,
        unsigned char y2, unsigned char fill, unsigned char color);
void glcd_draw_image(unsigned char *image, unsigned char width,
        unsigned char height, unsigned char x, unsigned char y,
        unsigned char invert);
void glcd_draw_bar(unsigned char value, unsigned char max,
        unsigned char x, unsigned char y, unsigned char width,
        unsigned char height, unsigned char invert);
void glcd_draw_line(unsigned char x1, unsigned char y1, unsigned char x2,
        unsigned char y2, unsigned char color);
void glcd_draw_rect_inv(unsigned char x1, unsigned char y1, unsigned char x2,
        unsigned char y2);

#endif // GRAPHLCD_H