#ifndef PCD8544_H
#define PCD8544_H

#include <htc.h>

/*** PIC CLOCK FREQUENCY CONFIGURATION ****************************************/

#ifndef _XTAL_FREQ
#define _XTAL_FREQ                      64000000ul
#endif

/*** LCD CONFIGURATION ********************************************************/

/**
 * Uncomment this define to let the driver use PIC's hardware SPI module.
 */
#define PCD_GLCD_USE_HW_SPI

/**
 * Uncomment the define corresponding to the desired SPI clock frequency.
 * PCD8544 can handle maximum 4 MHz.
 */
//#define PCD_GLCD_SPI_CLK_FOSC_64
#define PCD_GLCD_SPI_CLK_FOSC_16
//#define PCD_GLCD_SPI_CLK_FOSC_4

#define PCD_GLCD_SCK                    (LATC3)
#define PCD_GLCD_SCK_TRIS               (TRISC3)
#define PCD_GLCD_MOSI                   (LATC5)
#define PCD_GLCD_MOSI_TRIS              (TRISC5)
#define PCD_GLCD_SCE                    (LATD0)
#define PCD_GLCD_SCE_TRIS               (TRISD0)
#define PCD_GLCD_RST                    (LATD1)
#define PCD_GLCD_RST_TRIS               (TRISD1)
#define PCD_GLCD_DC                     (LATD2)
#define PCD_GLCD_DC_TRIS                (TRISD2)
#define PCD_GLCD_SSPCON1                (SSPCON1)
#define PCD_GLCD_SSPBUF                 (SSPBUF)
#define PCD_GLCD_SSPIF                  (SSPIF)

/*
 * 1 - VCC
 * 2 - GND
 * 3 - SCE  (Chip Select)
 * 4 - RST  (Reset)
 * 5 - D/C  (Data / Command)
 * 6 - MOSI (Serial Data Input)
 * 7 - SCK  (Serial Clock)
 * 8 - LED  (Backlight)
 */

/*** API FUNCTIONS ************************************************************/

void pcd_glcd_write(unsigned char byte, unsigned char is_data);

void pcd_glcd_init();
void pcd_glcd_gotoxy(unsigned char x, unsigned char y);
void pcd_glcd_clear();

void pcd_glcd_test_pattern_1();

#endif // PCD8544_H