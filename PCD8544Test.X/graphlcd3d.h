#ifndef GRAPHLCD3D_H
#define GRAPHLCD3D_H

#define GLCD3D_POLYGON_MAX_VERTICES 6

typedef struct
{
    signed int x;
    signed int y;
    signed int z;
} glcd3d_static_vertex_t;

typedef struct
{
    glcd3d_static_vertex_t v;
    unsigned int screen_x;
    unsigned int screen_y;
} glcd3d_vertex_t;

typedef struct
{
    unsigned char vertex_count;
    glcd3d_vertex_t vertices[GLCD3D_POLYGON_MAX_VERTICES];
    unsigned char color;
} glcd3d_polygon_t;

glcd3d_static_vertex_t glcd3d_static_vertex(signed int x, signed int y, signed int z);

void glcd3d_convert_vertex(glcd3d_vertex_t *vertex,
        unsigned char zoom_factor,
        unsigned int x_origin,
        unsigned int y_origin);
void glcd3d_convert_polygon(glcd3d_polygon_t *polygon,
        unsigned char zoom_factor,
        unsigned int x_origin,
        unsigned int y_origin);

//glcd3d_polygon_t *glcd3d_new_polygon(unsigned char vertex_count,
//        unsigned char color);

void glcd3d_draw_polygon(glcd3d_polygon_t *polygon);

void glcd3d_rotate_vertex(glcd3d_static_vertex_t *orig, glcd3d_static_vertex_t *rotated,
        signed int x_angle, signed int y_angle, signed int z_angle);

#endif