#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
include Makefile

# Environment
MKDIR=mkdir -p
RM=rm -f 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.cof
else
IMAGE_TYPE=production
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.cof
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Object Files
OBJECTFILES=${OBJECTDIR}/graphlcd.p1 ${OBJECTDIR}/graphlcd3d.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/pcd8544.p1 ${OBJECTDIR}/t6963c.p1


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

# Path to java used to run MPLAB X when this makefile was created
MP_JAVA_PATH=C:\\Program\ Files\\Java\\jdk1.6.0_26\\jre/bin/
OS_CURRENT="$(shell uname -s)"
############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
MP_CC=C:\\Program\ Files\\HI-TECH\ Software\\PICC-18\\PRO\\9.65\\bin\\picc18.exe
# MP_BC is not defined
MP_AS=C:\\Program\ Files\\HI-TECH\ Software\\PICC-18\\PRO\\9.65\\bin\\picc18.exe
MP_LD=C:\\Program\ Files\\HI-TECH\ Software\\PICC-18\\PRO\\9.65\\bin\\picc18.exe
MP_AR=C:\\Program\ Files\\HI-TECH\ Software\\PICC-18\\PRO\\9.65\\bin\\picc18.exe
# MP_BC is not defined
MP_CC_DIR=C:\\Program\ Files\\HI-TECH\ Software\\PICC-18\\PRO\\9.65\\bin
# MP_BC_DIR is not defined
MP_AS_DIR=C:\\Program\ Files\\HI-TECH\ Software\\PICC-18\\PRO\\9.65\\bin
MP_LD_DIR=C:\\Program\ Files\\HI-TECH\ Software\\PICC-18\\PRO\\9.65\\bin
MP_AR_DIR=C:\\Program\ Files\\HI-TECH\ Software\\PICC-18\\PRO\\9.65\\bin
# MP_BC_DIR is not defined

.build-conf: ${BUILD_SUBPROJECTS}
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.cof

MP_PROCESSOR_OPTION=18F46K20
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/graphlcd3d.p1: graphlcd3d.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 graphlcd3d.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 graphlcd3d.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/graphlcd3d.p1:\\" > ${OBJECTDIR}/graphlcd3d.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/graphlcd3d.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/graphlcd3d.p1.d
else 
	@cat ${OBJECTDIR}/graphlcd3d.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/graphlcd3d.p1.d
endif
${OBJECTDIR}/pcd8544.p1: pcd8544.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 pcd8544.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 pcd8544.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/pcd8544.p1:\\" > ${OBJECTDIR}/pcd8544.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/pcd8544.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/pcd8544.p1.d
else 
	@cat ${OBJECTDIR}/pcd8544.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/pcd8544.p1.d
endif
${OBJECTDIR}/t6963c.p1: t6963c.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 t6963c.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 t6963c.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/t6963c.p1:\\" > ${OBJECTDIR}/t6963c.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/t6963c.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/t6963c.p1.d
else 
	@cat ${OBJECTDIR}/t6963c.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/t6963c.p1.d
endif
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 main.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 main.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/main.p1:\\" > ${OBJECTDIR}/main.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/main.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/main.p1.d
else 
	@cat ${OBJECTDIR}/main.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/main.p1.d
endif
${OBJECTDIR}/graphlcd.p1: graphlcd.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 graphlcd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 graphlcd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/graphlcd.p1:\\" > ${OBJECTDIR}/graphlcd.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/graphlcd.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/graphlcd.p1.d
else 
	@cat ${OBJECTDIR}/graphlcd.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/graphlcd.p1.d
endif
else
${OBJECTDIR}/graphlcd3d.p1: graphlcd3d.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 graphlcd3d.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 graphlcd3d.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/graphlcd3d.p1:\\" > ${OBJECTDIR}/graphlcd3d.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/graphlcd3d.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/graphlcd3d.p1.d
else 
	@cat ${OBJECTDIR}/graphlcd3d.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/graphlcd3d.p1.d
endif
${OBJECTDIR}/pcd8544.p1: pcd8544.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 pcd8544.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 pcd8544.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/pcd8544.p1:\\" > ${OBJECTDIR}/pcd8544.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/pcd8544.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/pcd8544.p1.d
else 
	@cat ${OBJECTDIR}/pcd8544.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/pcd8544.p1.d
endif
${OBJECTDIR}/t6963c.p1: t6963c.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 t6963c.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 t6963c.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/t6963c.p1:\\" > ${OBJECTDIR}/t6963c.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/t6963c.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/t6963c.p1.d
else 
	@cat ${OBJECTDIR}/t6963c.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/t6963c.p1.d
endif
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 main.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 main.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/main.p1:\\" > ${OBJECTDIR}/main.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/main.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/main.p1.d
else 
	@cat ${OBJECTDIR}/main.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/main.p1.d
endif
${OBJECTDIR}/graphlcd.p1: graphlcd.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR} 
	${MP_CC} --pass1 graphlcd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	${MP_CC} --scandep --pass1 graphlcd.c $(MP_EXTRA_CC_PRE) -q --chip=$(MP_PROCESSOR_OPTION) -P  --outdir=${OBJECTDIR} -N31 --warn=9 --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 --cp=16 -Blarge --double=24  --mode=pro -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s"
	echo " ${OBJECTDIR}/graphlcd.p1:\\" > ${OBJECTDIR}/graphlcd.p1.d
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@cat ${OBJECTDIR}/graphlcd.dep | sed -e 's/^ *//' -e 's/\\/\//g' -e 's/ /\\ /g' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/graphlcd.p1.d
else 
	@cat ${OBJECTDIR}/graphlcd.dep | sed -e 's/^ *//' -e 's/^.*$$/ &\\/g'  >> ${OBJECTDIR}/graphlcd.p1.d
endif
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.cof: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) -odist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.cof -mdist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.map --summary=default,-psect,-class,+mem,-hex --chip=$(MP_PROCESSOR_OPTION) -P --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -D__DEBUG  -N31 --warn=9 --cp=16 -Blarge --double=24  --mode=pro --output=default,-inhx032 -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s" ${OBJECTFILES}  
	${RM} dist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.hex
else
dist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.cof: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) -odist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.cof -mdist/${CND_CONF}/${IMAGE_TYPE}/PCD8544Test.X.${IMAGE_TYPE}.map --summary=default,-psect,-class,+mem,-hex --chip=$(MP_PROCESSOR_OPTION) -P --runtime=default,+clear,+init,+keep,-download,+stackwarn,-config,+clib,-plib --opt=default,+asm,-asmfile,-9 -N31 --warn=9 --cp=16 -Blarge --double=24  --mode=pro --output=default,-inhx032 -g --asmlist "--errformat=%f:%l: error: %s" "--msgformat=%f:%l: advisory: %s" "--warnformat=%f:%l warning: %s" ${OBJECTFILES}  
endif


# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf:
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
