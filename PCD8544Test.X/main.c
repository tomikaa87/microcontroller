#include <htc.h>
#include <stdio.h>
#include <peripheral/EEP.h>

#include "graphlcd.h"
#include "graphlcd3d.h"

__CONFIG(1, RCCLKO);
__CONFIG(2, BORDIS & PWRTEN & WDTDIS);
__CONFIG(3, PBDIGITAL);
__CONFIG(4, LVPDIS);
__CONFIG(5, UNPROTECT);
__CONFIG(6, WPA);
__CONFIG(7, UNPROTECT);

#define LCD_CMD_INIT                    0x01
#define LCD_CMD_CLEAR                   0x02
#define LCD_CMD_ADDR                    0x03   /* Param 1: address, 0x00 - 0xFF */
#define LCD_CMD_GOTOXY                  0x04   /* Param 1: X 0x00 - 0xFF, Param 2: Y, 0x00 - 0xFF*/
#define LCD_CMD_BACKLIGHT               0x05   /* Param 1: level, 0x00 - 0xFF */
#define LCD_CMD_CONTRAST                0x06   /* Param 1: level, 0x00 - 0xFF */
#define LCD_CMD_SHIFT_CUR_LEFT          0x07
#define LCD_CMD_SHIFT_CUR_RIGHT         0x08
#define LCD_CMD_SHIFT_SCR_LEFT          0x09
#define LCD_CMD_SHIFT_SCR_RIGHT         0x0A
#define LCD_CMD_SET_ENTRY_MODE          0x0B   /* Param 1: LCD_ENTRY_MODE_XXX */
#define LCD_CMD_SET_CUR_MODE            0x0C    /* Param 1: LCD_CURSOR_MODE_XXX */
#define LCD_CMD_DEFINE_CUSTOM_CHAR      0x0D   /* Param 1: 0x00 - 0x07, 2-9: 0x00 - 0xF8 */
#define LCD_CMD_PUT_CUSTOM_CHAR         0x0E   /* Param 1: 0x00 - 0x07 */
#define LCD_CMD_SET_BAUD_RATE           0x0F   /* Param 1: LCD_BAUD_RATE_XXX */
#define LCD_CMD_RESET_CPU               0x1F

void setup_osc()
{
    OSCCON = 0b01110000;
    PLLEN = 1;
}

void setup_uart()
{
    TRISC6 = 0;
    //SPBRGH = (unsigned char)(1666u >> 8);
    //SPBRG = (unsigned char)(1666u & 0xFF);
    SPBRG = 103;
    SPBRGH = 0;
    BRG16 = 0;
    BRGH = 0;
    SYNC = 0;
    SPEN = 1;
    TXEN = 1;
}

void putch(char c)
{
    while (!TRMT)
        continue;
    TXREG = c;
}

typedef struct
{
    signed char x;
    signed char y;
    unsigned char x_inc;
    unsigned char y_inc;
    unsigned char x_spd;
    unsigned char y_spd;
} point_t;

#define POINT_COUNT 5
point_t points[POINT_COUNT];

point_t rect_points[2];

unsigned int rnd;
unsigned int random()
{
    unsigned char msb;

    msb = ((rnd >> 0) ^ (rnd >> 2) ^ (rnd >> 3) ^ (rnd >> 5)) & 1;
    rnd = (rnd >> 1) | (msb << 15);

    return rnd;
}

static const glcd3d_vertex_t poly_init[3] = {
    {{1, -50, 4}, 0, 0},
    {{50, 1, 4}, 0, 0},
    {{-50, 1, 4}, 0, 0}
};

void main()
{
    glcd3d_polygon_t poly;
    unsigned int i;
    signed int x_angle, y_angle, z_angle;

    setup_osc();
    setup_uart();
    TRISD3 = 0;
    LATD3 = 1;

    glcd_initialize();
    glcd_clear_buf();

    poly.color = GLCD_COLOR_BLACK;
    poly.vertex_count = 3;
/*
    poly.vertices = poly_init;

    glcd3d_convert_polygon(&poly, 1, GLCD_RESOLUTION_X / 2, GLCD_RESOLUTION_Y / 2);
    glcd3d_draw_polygon(&poly);
    glcd_write_buf(0);
*/

    x_angle = 0;
    y_angle = 0;
    z_angle = 0;

    while (1)
    {
        glcd_clear_buf();

        // Rotate all vertices
        for (i = 0; i < poly.vertex_count; i++)
        {
            glcd3d_rotate_vertex(&(poly_init[i]),
                    &(poly.vertices[i].v),
                    x_angle, y_angle, z_angle);
        }

        glcd3d_convert_polygon(&poly, 1, GLCD_RESOLUTION_X / 2, GLCD_RESOLUTION_Y / 2);
        glcd3d_draw_polygon(&poly);
        glcd_write_buf(0);

        x_angle++;
        //y_angle++;
        //z_angle++;

        if (x_angle == 360)
            x_angle = 0;
        if (y_angle == 360)
            y_angle = 0;
        if (z_angle == 360)
            z_angle = 0;

        for (i = 0; i < 15000; i++)
            __delay_us(20);
    }
}

/*void main()
{
    unsigned int i;
    unsigned char j;
    
    setup_osc();

    TRISD3 = 0;
    LATD3 = 1;

    glcd_initialize();
    glcd_clear_buf();

    rnd = eeprom_read(0) << 8;
    rnd += eeprom_read(1);
    (void)random();
    eeprom_write(0, (unsigned char)(rnd >> 8));
    eeprom_write(1, (unsigned char)(rnd & 0xFF));

    for (j = 0; j < POINT_COUNT; j++)
    {
        points[j].x = random() % 84;
        points[j].y = random() % 48;
        points[j].x_inc = random() % 2;
        points[j].y_inc = random() % 2;
        points[j].x_spd = random() % 2 + 1;
        points[j].y_spd = random() % 2 + 1;
    }

    for (j = 0; j < 2; j++)
    {
        rect_points[j].x = random() % 84;
        rect_points[j].y = random() % 48;
        rect_points[j].x_inc = random() % 2;
        rect_points[j].y_inc = random() % 2;
        rect_points[j].x_spd = random() % 2 + 1;
        rect_points[j].y_spd = random() % 2 + 1;
    }

    while (1)
    {
        for (j = 0; j < POINT_COUNT; j++)
        {
            if (points[j].x_inc)
                points[j].x += points[j].x_spd;
            else
                points[j].x -= points[j].x_spd;

            if (points[j].y_inc)
                points[j].y += points[j].y_spd;
            else
                points[j].y -= points[j].y_spd;

            if (points[j].x >= GLCD_RESOLUTION_X - 1)
            {
                points[j].x = GLCD_RESOLUTION_X - 1;
                points[j].x_inc = 0;
                points[j].x_spd = random() % 2 + 1;
            }
            if (points[j].x <= 0)
            {
                points[j].x = 0;
                points[j].x_inc = 1;
                points[j].x_spd = random() % 2 + 1;
            }

            if (points[j].y >= GLCD_RESOLUTION_Y - 1)
            {
                points[j].y = GLCD_RESOLUTION_Y - 1;
                points[j].y_inc = 0;
                points[j].y_spd = random() % 2 + 1;
            }
            if (points[j].y <= 0)
            {
                points[j].y = 0;
                points[j].y_inc = 1;
                points[j].y_spd = random() % 2 + 1;
            }
        }

        glcd_clear_buf();

        for (j = 0; j < POINT_COUNT; j++)
        {
            if (j == POINT_COUNT - 1)
                glcd_draw_line(points[j].x, points[j].y, 
                        points[0].x, points[0].y,
                        GLCD_COLOR_BLACK);
            else
                glcd_draw_line(points[j].x, points[j].y, 
                        points[j + 1].x, points[j + 1].y,
                        GLCD_COLOR_BLACK);
        }

        for (j = 0; j < 2; j++)
            glcd_set_pixel(points[j].x, points[j].y, GLCD_COLOR_BLACK);

        for (j = 0; j < 2; j++)
        {
            if (rect_points[j].x_inc)
                rect_points[j].x += rect_points[j].x_spd;
            else
                rect_points[j].x -= rect_points[j].x_spd;

            if (rect_points[j].y_inc)
                rect_points[j].y += rect_points[j].y_spd;
            else
                rect_points[j].y -= rect_points[j].y_spd;

            if (rect_points[j].x >= GLCD_RESOLUTION_X - 1)
            {
                rect_points[j].x = GLCD_RESOLUTION_X - 1;
                rect_points[j].x_inc = 0;
                rect_points[j].x_spd = random() % 2 + 1;
            }
            if (rect_points[j].x <= 0)
            {
                rect_points[j].x = 0;
                rect_points[j].x_inc = 1;
                rect_points[j].x_spd = random() % 2 + 1;
            }

            if (rect_points[j].y >= GLCD_RESOLUTION_Y - 1)
            {
                rect_points[j].y = GLCD_RESOLUTION_Y - 1;
                rect_points[j].y_inc = 0;
                rect_points[j].y_spd = random() % 2 + 1;
            }
            if (rect_points[j].y <= 0)
            {
                rect_points[j].y = 0;
                rect_points[j].y_inc = 1;
                rect_points[j].y_spd = random() % 2 + 1;
            }
        }

        glcd_draw_rect_inv(rect_points[0].x, rect_points[0].y,
            rect_points[1].x, rect_points[1].y);

        glcd_write_buf(0);

        for (i = 0; i < 1500; i++)
            __delay_us(20);
    }
}*/