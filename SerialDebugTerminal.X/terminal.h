//
// Serial Debug Terminal
// Terminal module
//
// Author: Tamas Karpati
// This file was created on 2014-04-04
//

#ifndef TERMINAL_H
#define TERMINAL_H

void Terminal_input(char c);
void Terminal_task();

#endif // TERMINAL_H