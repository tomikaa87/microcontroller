//
// Serial Debug Terminal
// Color serial debug terminal for microcontroller projects
//
// Author: Tamas Karpati
// This file was created on 2014-04-03
//
// **
// MPLAB XC8 1.31 notes: don't use %c formatting in printf, because the compiler
// will generate a malfunctioning printf code.
// **
//

#define USE_PIC_CONFIGURATION
#define USE_SERIAL_CONFIGURATION

#include "config.h"
#include "serial.h"
#include "terminal.h"
#include "glcd.h"

#include <xc.h>

#include <stdio.h>

//------------------------------------------------------------------------------
// Interrupt handling
//------------------------------------------------------------------------------

void interrupt isr()
{
    if (SERIAL_RCIE && SERIAL_RCIF)
    {
        Serial_writeToBuffer(SERIAL_RCREG);
    }
}

//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------

void init()
{
    OSCCON = PIC_OSCCON_DEFAULT;
    PLLEN = 1;

    GIE = 1;
    PEIE = 1;
}

void main()
{
    // Dummy instruction to avoid unused variable warning
    Warning_SerialBaudRateErrorTooHigh[0] = 0;
    if (Warning_SerialBaudRateErrorTooHigh[0] == 1)
        return;

    init();
    Serial_init();
    GLCD_init();

    GLCD_drawRect(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, 0x03);

//    GLCD_drawText(0, 0, "Serial Debug Terminal Demo ABCDEFGHIJKLMNOPQRSTUVWXYZ", 0, 20, GLCD_font5x8);

    printf("Serial Debug Terminal started\r\n\r\n");

    static const char teststr[] = "This is a long line. ABCDEFGHIJKLMNOPQRSTUVWXYZ 123456790 Warning_SerialBaudRateErrorTooHigh for (unsigned char i = 0; i < sizeof (teststr); ++i) Terminal_input(teststr[i]); // Color serial debug terminal for microcontroller projects";
    //const char teststr[] = "This is a long line.\nABCDEFGHIJKLMNOPQRSTUVWXYZ\r123456790";
//    const char teststr[] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\b\bBB\f";
    for (unsigned char i = 0; i < sizeof (teststr); ++i)
        Terminal_input(teststr[i]);

    uint8_t counter = 0;

    for (;;)
    {
        if (Serial_bufferHasData())
        {
            counter = 0;
            Terminal_input(Serial_readFromBuffer());
        }
        else if (counter == 255)
        {
            Terminal_task();
        }

        if (counter < 255)
            ++counter;
        
        CLRWDT();
    }
}
