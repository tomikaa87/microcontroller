//
// Serial Debug Terminal
// Serial communications module
//
// Author: Tamas Karpati
// This file was created on 2014-04-03
//

#define USE_SERIAL_CONFIGURATION

#include "config.h"
#include "serial.h"

#include <stdint.h>
#include <stdio.h>

//------------------------------------------------------------------------------
// Globals
//------------------------------------------------------------------------------

volatile static char g_serialRxBuffer[SERIAL_RX_BUFFER_SIZE] = {0,};
volatile static uint8_t g_serialRxBufNextIn = 0;
volatile static uint8_t g_serialRxBufNextOut = 0;

//------------------------------------------------------------------------------
// Ring buffer handling
//------------------------------------------------------------------------------

#define rxBufHasData()      (g_serialRxBufNextIn != g_serialRxBufNextOut)

char Serial_readFromBuffer()
{
    while (!rxBufHasData()) CLRWDT();
    char c = g_serialRxBuffer[g_serialRxBufNextOut];
    g_serialRxBufNextOut = (g_serialRxBufNextOut + 1) % SERIAL_RX_BUFFER_SIZE;
    return c;
}

//--------------------------------------------------------------------
void Serial_writeToBuffer(char c)
{
    g_serialRxBuffer[g_serialRxBufNextIn] = c;
    uint8_t nextIn = g_serialRxBufNextIn;
    g_serialRxBufNextIn = (g_serialRxBufNextIn + 1) % SERIAL_RX_BUFFER_SIZE;
    if (g_serialRxBufNextIn == g_serialRxBufNextOut)
        g_serialRxBufNextIn = nextIn;
}

//------------------------------------------------------------------------------
// API
//------------------------------------------------------------------------------

void Serial_init()
{
    SERIAL_BRG16 = 1;
    // TXEN = 1, BRGH = UART_TX_HIGH_SPEED
    SERIAL_TXSTA = 0b00100000 | (SERIAL_HIGH_SPEED ? 0b100 : 0);
    // SPEN = 1, CREN = 1
    SERIAL_RCSTA = 0b10010000;
    SERIAL_SPBRG = SERIAL_SPBRG_VALUE & 0xFF;
    SERIAL_SPBRGH = (SERIAL_SPBRG_VALUE >> 8) & 0xFF;

    SERIAL_RCIE = 1;
}

char Serial_bufferHasData()
{
    return rxBufHasData();
}

void putch(char c)
{
    while (!SERIAL_TRMT) CLRWDT();
    SERIAL_TXREG = c;
    while (!SERIAL_TRMT) CLRWDT();
}