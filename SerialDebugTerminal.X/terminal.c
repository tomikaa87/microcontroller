//
// Serial Debug Terminal
// Terminal module
//
// Author: Tamas Karpati
// This file was created on 2014-04-04
//

#include "terminal.h"
#include "glcd.h"
#include "config.h"

#include <stdint.h>
#include <stdio.h>

//------------------------------------------------------------------------------
// Globals
//------------------------------------------------------------------------------

static const uint8_t Columns = 34;
static const uint8_t Rows = 16;

static char g_lineBuffer[Columns * Rows] = {' ',};
static uint8_t g_rowsToUpdate[Rows] = {0,};

static uint8_t g_currentColumn = 0;
static uint8_t g_currentRow = 0;

static bit g_isFullUpdateNeeded;
static uint8_t g_currentUpdatedRow = 0;

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------

static void insertChar(char c);
static void insertCR();
static void insertLF();
static void backspace();
static void clear();
static void scrollUp();

//static void dumpLineBuffer();

static void drawChar(uint8_t row, uint8_t column, char c);
//static void drawFullBuffer();

static void updateRow(uint8_t row);

//------------------------------------------------------------------------------
// API
//------------------------------------------------------------------------------

void Terminal_input(char c)
{
    switch (c)
    {
        case '\b':
            backspace();
            break;

        case '\f':
            clear();
            break;

        case '\r':
            insertCR();
            break;

        case '\n':
            insertLF();
            break;

        default:
            if (c >= 32 && c < 127)
                insertChar(c);
            break;
    }
}

void Terminal_task()
{
    if (g_currentUpdatedRow == 0)
    {
        if (!g_isFullUpdateNeeded)
        {
            for (uint8_t row = 0; row < Rows; ++row)
            {
                if (g_rowsToUpdate[row])
                {
                    g_rowsToUpdate[row] = 0;
                    updateRow(row);
                }
            }

            return;
        }
    }

    if (g_currentUpdatedRow == 0)
        g_isFullUpdateNeeded = 0;

    updateRow(g_currentUpdatedRow++);

    if (g_currentUpdatedRow == Rows)
        g_currentUpdatedRow = 0;
}

//------------------------------------------------------------------------------
// Internal stuff
//------------------------------------------------------------------------------

static void insertChar(char c)
{
    uint16_t index = (uint16_t)g_currentRow * (uint16_t)Columns + (uint16_t)g_currentColumn;

    g_lineBuffer[index] = c;
//    drawChar(g_currentRow, g_currentColumn, c);
    g_rowsToUpdate[g_currentRow] = 1;

    ++g_currentColumn;
    if (g_currentColumn == Columns)
        insertLF();

//    dumpLineBuffer();
}

static void insertCR()
{
    g_currentColumn = 0;
}

static void insertLF()
{
    ++g_currentRow;
    if (g_currentRow == Rows)
        scrollUp();
    g_currentColumn = 0;
}

static void backspace()
{
    uint16_t index = (uint16_t)g_currentRow * (uint16_t)Columns + (uint16_t)g_currentColumn;

    if (g_currentColumn > 0)
    {
        --g_currentColumn;
        g_lineBuffer[index] = ' ';
        drawChar(g_currentRow, g_currentColumn, ' ');
    }
    else if (g_currentRow > 0)
    {
        --g_currentRow;
        g_currentColumn = Columns - 1;
        g_lineBuffer[index] = ' ';
        drawChar(g_currentRow, g_currentColumn, ' ');
    }

//    dumpLineBuffer();
}

static void clear()
{
    g_currentColumn = 0;
    g_currentRow = 0;

    for (uint16_t i = 0; i < sizeof (g_lineBuffer); ++i)
        g_lineBuffer[i] = ' ';

    GLCD_clear();
    GLCD_drawRect(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, 0x03);

    printf("Terminal cleared");
}

static void scrollUp()
{
    --g_currentRow;

    for (uint16_t i = 0, j = Columns; i < Columns * Rows; ++i, ++j)
    {
        if (j < Columns * Rows)
            g_lineBuffer[i] = g_lineBuffer[j];
        else
            g_lineBuffer[i] = ' ';
    }

    g_isFullUpdateNeeded = 1;

//    drawFullBuffer();

//    dumpLineBuffer();
}

//static void dumpLineBuffer()
//{
//    printf("** Line buffer state: column: %d, row: %d\r\n",
//           g_currentColumn, g_currentRow);
//
//    for (uint8_t i = 0; i < Columns; ++i)
//        printf("%d", i % 10);
//    printf("\r\n\r\n");
//
//    for (uint8_t row = 0; row < Rows; ++row)
//    {
//        for (uint8_t col = 0; col < Columns; ++col)
//        {
//            char c = g_lineBuffer[row * Columns + col];
//            if (c == 0)
//                break;
//            putch(c);
//            CLRWDT();
//        }
//        printf("\r\n");
//    }
//
//    printf("****\r\n\r\n");
//
//    GLCD_clear();
//    uint8_t y = LCD_HEIGHT - 2 - 8;
//
//    for (uint8_t row = 0; row < Rows; ++row)
//    {
//        char *s = &g_lineBuffer[row * Columns];
//        GLCD_drawTextEx(0, y, s, Columns, 0, 20, GLCD_font5x8);
//        y -= 8;
//        CLRWDT();
//    }
//}

static void drawChar(uint8_t row, uint8_t column, char c)
{
    uint8_t x = 2 + column * 5;
    uint8_t y = LCD_HEIGHT - 2 - (row + 1) * 8;

    GLCD_drawChar(x, y, c, 0x1C, 0, GLCD_font5x8);
}

//static void drawFullBuffer()
//{
//    uint8_t x = 2;
//    uint8_t y = LCD_HEIGHT - 2 - 8;
//
//    for (uint8_t row = 0; row < Rows; ++row)
//    {
//        char *s = &g_lineBuffer[row * Columns];
//        GLCD_drawTextEx(x, y, s, Columns, 0x1C, 0, GLCD_font5x8);
//        y -= 8;
//        CLRWDT();
//    }
//}

static void updateRow(uint8_t row)
{
    g_rowsToUpdate[row] = 0;

    uint8_t x = 2, y = LCD_HEIGHT - 2 - (8 * (row + 1));

    char *s = &g_lineBuffer[row * Columns];
    GLCD_drawTextEx(x, y, s, Columns, 0x1C, 0, GLCD_font5x8);
    CLRWDT();
}