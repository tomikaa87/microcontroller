//
// Serial Debug Terminal
// Serial communications module
//
// Author: Tamas Karpati
// This file was created on 2014-04-03
//

#ifndef SERIAL_H
#define SERIAL_H

void Serial_init();
char Serial_bufferHasData();

char Serial_readFromBuffer();
void Serial_writeToBuffer(char c);

#endif SERIAL_H