//
// Serial Debug Terminal
// Configuration header
//
// Author: Tamas Karpati
// This file was created on 2014-04-03
//

#ifndef CONFIG_H
#define CONFIG_H

#include <xc.h>

#define _XTAL_FREQ              32000000ul

//------------------------------------------------------------------------------
// LS020 GLCD configuration
//------------------------------------------------------------------------------

#ifdef USE_LCD_CONFIGURATION

#define LCD_SSPCON1             SSPCON1
#define LCD_SSPCON1_DEFAULT     0b00100000

#define LCD_PIN_SCK             LATCbits.LATC3
#define LCD_PIN_SDO             LATCbits.LATC5

#define LCD_TRIS_SCK            TRISCbits.TRISC3
#define LCD_TRIS_SDO            TRISCbits.TRISC5

#define LCD_PIN_RS              LATDbits.LATD2
#define LCD_PIN_RST             LATDbits.LATD1
#define LCD_PIN_CS              LATDbits.LATD0

#define LCD_TRIS_RS             TRISDbits.TRISD2
#define LCD_TRIS_RST            TRISDbits.TRISD1
#define LCD_TRIS_CS             TRISDbits.TRISD0

#endif // USE_LCD_CONFIGURATION

#define LCD_WIDTH               176
#define LCD_HEIGHT              132

//------------------------------------------------------------------------------
// Serial configuration
//------------------------------------------------------------------------------

#ifdef USE_SERIAL_CONFIGURATION

#define SERIAL_RX_BUFFER_SIZE       64

#define SERIAL_SPBRG                SPBRG
#define SERIAL_SPBRGH               SPBRGH
#define SERIAL_BRGH                 BRGH
#define SERIAL_BRG16                BRG16
#define SERIAL_TXSTA                TXSTA
#define SERIAL_RCSTA                RCSTA
#define SERIAL_TRMT                 TRMT
#define SERIAL_TXREG                TXREG
#define SERIAL_RCREG                RCREG
#define SERIAL_RCIF                 RCIF
#define SERIAL_RCIE                 RCIE

#define SERIAL_HIGH_SPEED           1
#define SERIAL_BAUD_RATE            9600ul

// FIXME this should be calculated run-time or use auto baud rate

#if (SERIAL_HIGH_SPEED == 1)
    #define SERIAL_BRG_DIVISOR      16ul
#else
    #define SERIAL_BRG_DIVISOR      64ul
#endif
#define SERIAL_SPBRG_VALUE \
    (unsigned char)(_XTAL_FREQ / SERIAL_BAUD_RATE / SERIAL_BRG_DIVISOR - 1)
#define SERIAL_REAL_BAUD_RATE \
    (unsigned long)(_XTAL_FREQ / (SERIAL_BRG_DIVISOR * (SERIAL_SPBRG_VALUE + 1)))
#define SERIAL_BAUD_RATE_ERROR \
    (signed char)((((float)SERIAL_REAL_BAUD_RATE - (float)SERIAL_BAUD_RATE) / (float)SERIAL_BAUD_RATE) * 100.0)

char Warning_SerialBaudRateErrorTooHigh[SERIAL_BAUD_RATE_ERROR <= 3];

#endif // USE_SERIAL_CONFIGURATION

//------------------------------------------------------------------------------
// PIC configuration
//------------------------------------------------------------------------------

#ifdef USE_PIC_CONFIGURATION

#define PIC_OSCCON_DEFAULT      0b01100010

// CONFIG1H
#pragma config FOSC = INTIO67   // Oscillator Selection bits (Internal oscillator block, port function on RA6 and RA7)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 22        // Brown Out Reset Voltage bits (VBOR set to 2.2 V nominal)

// CONFIG2H
#pragma config WDTEN = ON       // Watchdog Timer Enable bit (WDT is always enabled. SWDTEN bit has no effect)
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
#pragma config PBADEN = ON      // PORTB A/D Enable bit (PORTB<4:0> pins are configured as analog input channels on Reset)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config HFOFST = OFF     // HFINTOSC Fast Start-up (The system clock is held off until the HFINTOSC is stable.)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RE3 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection Block 0 (Block 0 (000800-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection Block 3 (Block 3 (00C000-00FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection Block 0 (Block 0 (000800-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection Block 3 (Block 3 (00C000h-00FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection Block 0 (Block 0 (000800-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-0007FFh) not protected from table reads executed in other blocks)
#endif // USE_PIC_CONFIGURATION

#endif // CONFIG_H