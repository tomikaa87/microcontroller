//
// Serial Debug Terminal
// LS020 Graphical LCD Driver module
//
// Author: Tamas Karpati
// This file was created on 2014-04-04
//

#ifndef GLCD_H
#define GLCD_H

#include <stdint.h>

extern const uint8_t GLCD_font5x8[];

void GLCD_init();
void GLCD_clear();

void GLCD_setWindow(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);
void GLCD_setNextPixel(uint8_t color);

void GLCD_setPixel(uint8_t x, uint8_t y, uint8_t color);

void GLCD_set8bitColorMode(uint8_t bgrMode);

uint8_t GLCD_drawChar(uint8_t x, uint8_t y, char c, uint8_t color, 
        uint8_t bg_color, const uint8_t *font);
uint8_t GLCD_drawText(uint8_t x, uint8_t y, const char *s, uint8_t color,
        uint8_t bg_color, const uint8_t *font);
uint8_t GLCD_drawTextEx(uint8_t x, uint8_t y, const char *s, uint8_t length,
        uint8_t color, uint8_t bg_color, const uint8_t *font);

void GLCD_fillRect(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t color);
void GLCD_drawRect(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t color);

#endif // GLCD_H