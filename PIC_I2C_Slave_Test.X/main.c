/* 
 * File:   main.c
 * Author: ToMikaa
 *
 * Created on 2013. november 7., 19:56
 */

#include <stdio.h>
#include <stdint.h>
#include <xc.h>

//--------------------------------------------------------------------------------------------------
// Configuration
//--------------------------------------------------------------------------------------------------

// CONFIG1H
#pragma config OSC = INTIO67    // Oscillator Selection bits (Internal oscillator block, port function on RA6 and RA7)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown Out Reset Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
#pragma config PBADEN = ON      // PORTB A/D Enable bit (PORTB<4:0> pins are configured as analog input channels on Reset)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RE3 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = ON         // Single-Supply ICSP Enable bit (Single-Supply ICSP enabled)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit (Block 0 (000800-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit (Block 3 (00C000-00FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit (Block 0 (000800-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit (Block 3 (00C000-00FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit (Block 0 (000800-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-0007FFh) not protected from table reads executed in other blocks)

#define _XTAL_FREQ              32000000ul

//--------------------------------------------------------------------------------------------------
// Serial
//--------------------------------------------------------------------------------------------------

#define UART_TX_BAUD               9600ul
#define UART_TX_HIGH_SPEED         1

#if (UART_TX_HIGH_SPEED == 1)
    #define UART_TX_DIVIDER        16ul
#else
    #define UART_TX_DIVIDER        64ul
#endif
#define UART_TX_SPBRG \
    (unsigned char)(_XTAL_FREQ / UART_TX_BAUD / UART_TX_DIVIDER - 1)
#define UART_TX_REAL_BAUD \
    (unsigned long)(_XTAL_FREQ / (UART_TX_DIVIDER * (UART_TX_SPBRG + 1)))
#define UART_TX_BAUD_ERROR \
    (signed char)((((float)UART_TX_REAL_BAUD - (float)UART_TX_BAUD) / (float)UART_TX_BAUD) * 100.0)

//--------------------------------------------------------------------------------------------------
void SerialInit()
{
    // BRG16 = 1
    BAUDCON = 0b0000100;
    // TXEN = 1, BRGH = UART_TX_HIGH_SPEED
    TXSTA = 0b00100000 | (UART_TX_HIGH_SPEED ? 0b100 : 0);
    // SPEN = 1, CREN = 1
    RCSTA = 0b10010000;
    // Set SPBRG
    SPBRG = UART_TX_SPBRG & 0xFF;
    // Set SPBRGH
    SPBRGH = (UART_TX_SPBRG >> 8) & 0xFF;

    TRISC6 = 0;
    TRISC7 = 1;

    RCIE = 1;
}

//--------------------------------------------------------------------------------------------------
void putch(char c)
{
    while (!TRMT)
        continue;
    TXREG = c;
    while (!TRMT)
        continue;
}

//--------------------------------------------------------------------------------------------------
char getch()
{
    while (!RCIF)
        continue;
    return RCREG;
}

//--------------------------------------------------------------------------------------------------
// I2C Routines
//--------------------------------------------------------------------------------------------------

void I2CSlaveInit()
{
    SSPM3 = 0;
    SSPM2 = 1;
    SSPM1 = 1;
    SSPM0 = 0;
    SEN = 1;
    SSPIE = 1;
    SSPADD = 0xEE;
    SSPEN = 1;

    TRISC3 = 1;
    TRISC4 = 1;
}

//--------------------------------------------------------------------------------------------------
// Interrupt handler
//--------------------------------------------------------------------------------------------------

static const uint8_t cBufferSize = 10;
volatile uint8_t g_I2CBuffer[cBufferSize] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
uint8_t g_I2CAddress = 0;
bit g_I2CIsAddressByte = 0;

void interrupt InterruptHandler()
{   
    // Print incoming characters on the console
    if (RCIE && RCIF)
    {
        putch('[');
        putch(RCREG);
        putch(']');
    }

    // Handle MSSP interrupts
    if (SSPIE && SSPIF)
    {
        static uint8_t dummy;
        
        if (SSPSTATbits.R_nW)
        {
            // Master read
            if (!SSPSTATbits.D_nA)
            {
                // Address
                //g_I2CAddress = SSPBUF % cBufferSize;
                SSPBUF = g_I2CBuffer[g_I2CAddress++];
            }
            else
            {
                // Data
                SSPBUF = g_I2CBuffer[g_I2CAddress++];
            }
        }
        else
        {
            // Master write
            if (!SSPSTATbits.D_nA)
            {
                // Address
                g_I2CIsAddressByte = 1;
                dummy = SSPBUF;
            }
            else
            {
                // Data
                if (g_I2CIsAddressByte)
                {
                    g_I2CAddress = SSPBUF;
                    g_I2CIsAddressByte = 0;
                }
                else
                {
                    if (g_I2CAddress < cBufferSize)
                    {
                        g_I2CBuffer[g_I2CAddress++] = SSPBUF;
                    }
                    else
                    {
                        dummy = SSPBUF;
                    }
                }

                // Handle collision
                if (SSPCON1bits.WCOL)
                {
                    SSPCON1bits.WCOL = 0;
                    dummy = SSPBUF;
                }
            }
        }

        CKP = 1;
        SSPIF = 0;
    }
}

//--------------------------------------------------------------------------------------------------
// Main
//--------------------------------------------------------------------------------------------------

int main()
{
    OSCCON = 0b01110000;
    PLLEN = 1;

    GIE = 1;
    PEIE = 1;

    SerialInit();
    I2CSlaveInit();

    __delay_ms(10);

    printf("\r\nSystem initialized\r\n");

    for (;;)
    {

    }
}

