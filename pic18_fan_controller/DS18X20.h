/*
 * Weather Station Ethernet Board
 * 1-Wire driver for on-board DS18X20 sensor
 *
 * Author: Tamas Karpati
 * Date: 2010-09-15
 */

#ifndef __DS18X20_H
#define __DS18X20_H

#define TEMP_RESOLUTION 12
//#define DEBUG

typedef unsigned char ROMBits[64];
typedef unsigned char ROMBytes[8];


unsigned char DS18X20_NextDevice(unsigned char *romBits);
unsigned char DS18X20_FirstDevice(unsigned char *romBits);
void DS18X20_ROMBitsToROMBytes(unsigned char *romBits, unsigned char *romBytes);

unsigned char DS18X20_SearchDevices(ROMBits *romBits, unsigned char maxDevices);

signed int DS18X20_Read();
signed int DS18X20_ReadSpecific(unsigned char *romBits);

void DS18X20_PrintDeviceInfo(unsigned char *romBits);

#endif // __DS18X20_H
