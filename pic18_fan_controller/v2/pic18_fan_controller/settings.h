/**
 * Settings storage module that uses PIC's internal EEPROM
 *
 * @file settings.h
 * @author Tamas Karpati
 * @date 2011-02-01
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include "hardware_profile.h"

/*** Type definitions *****************************************************************************/

typedef unsigned char sensor_serial_t[8];   

typedef struct 
{
    sensor_serial_t serial;
    signed char fan;
    signed int max_temp;
    signed int min_temp;
} sensor_assignment_t;    

typedef struct
{
    unsigned char indocator_byte;
    
    // Fans configuration data
    unsigned char min_pwm_values[NUMBER_OF_FANS];
    unsigned char max_pwm_values[NUMBER_OF_FANS];
    unsigned char start_pwm_values[NUMBER_OF_FANS];
    unsigned char stop_pwm_values[NUMBER_OF_FANS];
    unsigned char default_pwm_values[NUMBER_OF_FANS];
    
    // Temperatures configuration data
    /*signed int min_temps[NUMBER_OF_FANS];
    signed int max_temps[NUMBER_OF_FANS];
    signed int min_virtual_temps[NUMBER_OF_FANS];
    signed int max_virtual_temps[NUMBER_OF_FANS];*/
    
    // Sensor <-> Fan assignments data
    /*sensor_serial_t fan_sensor_assign[NUMBER_OF_FANS]; 
    sensor_serial_t fan_virtual_sensor_assign[NUMBER_OF_FANS];*/
    
    // Automatic Fan Control configuration data
    unsigned char afc_states[NUMBER_OF_FANS];
    unsigned char afc_small_delta;
    unsigned char afc_big_delta;
    unsigned char afc_huge_delta;
    sensor_assignment_t afc_assignments[AFC_MAX_SENSOR_ASSIGNMENTS];
    
} config_data_t;


/*** Public API ***********************************************************************************/

void settings_load_from_eeprom();
void settings_save_to_eeprom();
void settings_sanity_check();
signed char settings_add_sensor_assignment(sensor_serial_t serial, 
                                           unsigned char fan,
                                           signed int min_temp,
                                           signed int max_temp);
signed char settings_remove_sensor_assignment(sensor_serial_t serial, unsigned char fan);

#endif // SETTINGS_H
