/**
 * Serial console module
 *
 * @file serial_console.h
 * @author Tamas Karpati
 * @date 2011-02-01
 */
 
#ifndef SERIAL_CONSOLE_H
#define SERIAL_CONSOLE_H

void serial_console_task();

#endif // SERIAL_CONSOLE_H
