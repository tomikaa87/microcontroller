/**
 * Interrupt handler
 *
 * @file interrupt.h
 * @author Tamas Karpati
 * @date 2011-01-26
 */
 
#include "interrupt.h"
#include "usart.h"
#include "hardware_profile.h"

#include <htc.h>

extern unsigned char usart_buf[USART_BUF_SIZE];
extern unsigned char usart_buf_next_in;
extern unsigned char usart_buf_next_out;
extern unsigned char afc_timer_timeout;

/**
 * Interrupt Service Routine that handles hardware interrupts
 */
void interrupt isr(void)
{
    // Timer1 interrupt
    if (TMR1IF && TMR1IE)
    {
        timer_1_value++;

        // Reset preload value and interrupt flag
        TMR1H = 240;
        TMR1L = 96;
        TMR1IF = 0;
    }
    
    // Timer0 interrupt
    if (TMR0IF && TMR0IE)
    {
        static unsigned int counter = 0;
        
        if (counter++ >= 600)
        {
            afc_timer_timeout = 1;
            counter = 0;
        }    
            
        TMR0IF = 0;
    }    
    
    // USART1 receive interrupt
    if (RC1IF)
    {
        unsigned char t;
                
        // Get a byte from the USART buffer register and store it
        // Use USART2 for low level communication
        usart_buf[usart_buf_next_in] = getch();
        t = usart_buf_next_in;
        usart_buf_next_in = (usart_buf_next_in + 1) % USART_BUF_SIZE;
        if (usart_buf_next_in == usart_buf_next_out)
            usart_buf_next_in = t;   
           
        // Clear interrupt flag 
        RC1IF = 0;
    }
}    
