/**
 * PIC18 based automatic fan controller main source file
 *
 * @file main.c
 * @author Tamas Karpati
 * @date 2011-01-26
 */
 
#include <htc.h>
#include <stdio.h>

#include "hardware_profile.h"
#include "usart.h"
#include "fan_control.h"
#include "settings.h"
#include "pwm.h"
#include "ds1820.h"
#include "serial_control.h"
#include "afc.h"
#include "terminal.h"

/*** Global variables *****************************************************************************/

config_data_t config;

extern rom_bits_t sensor_serials[MAX_TEMP_SENSORS];

extern signed int virtual_sensor_temps[MAX_VIRTUAL_TEMP_SENSORS];
extern unsigned char sensor_count;

extern unsigned char usart_buf[USART_BUF_SIZE];
extern unsigned char usart_buf_next_in;
extern unsigned char usart_buf_next_out;

unsigned char afc_timer_timeout = 1;

/*** CPU configuration ****************************************************************************/
__CONFIG(1, 
         FOSC_INTIO67);         // Internal OSC block, RA6 and RA7 are I/Os
__CONFIG(2, 
         PWRTEN_ON &            // Power-up timer on
         BORV_285 &             // Brown-out reset at 2.85 Volts
         WDTEN_OFF);            // Watchdog off
__CONFIG(3, 
         PBADEN_OFF);           // PORTB<5:0> are digital I/Os
__CONFIG(4, 
         LVP_OFF);              // Low voltage programming off
__CONFIG(5,
         CP0_OFF &              // Code protection block 0 off
         CPB_OFF);              // Boot block code protection off
__CONFIG(6, 
         WRT0_OFF &             // Write protection block 0 off
         WRTC_OFF);             // Configuration register write protection off
__CONFIG(7,
         EBTR0_OFF &            // Table read protection block 0 off
         EBTRB_OFF);            // Boot block table read protection off
         
/*** Main *****************************************************************************************/
void main()
{ 
    unsigned int i;
    
    // Setup OSC
    OSCCON = 0b01110000;        // Internal OSC block @16 MHz
    PLLEN = 1;                  // Enable x4 PLL -> 64 MHz

    // Disable ADC
    ADCON0 = 0;
    ADCON2 = 0;
    
    // Make analog ports digital
    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;

    // Disable unused peripherals
    MSSP1MD = 1;
    MSSP2MD = 1;
    CTMUMD = 1;
    CMP2MD = 1;
    CMP1MD = 1;
    ADCMD = 1;

    // Disable secondary oscillators
    SOSCGO = 0;
    T1SOSCEN = 0;
    T3SOSCEN = 0;
    T5SOSCEN = 0;  
    
    // Setup Timer0
    T0CON = 0b11000111;         // 8 bit, 1:256 prescaler, CLKOUT  
    TMR0IE = 1;
    GIE = 1;

    usart_setup();
    delay_msec(500);
    
    settings_load_from_eeprom();

    fan_control_init();
    
    // Load default fan values
    for (i = 0; i < NUMBER_OF_FANS; i++)
        pwm_set_duty_cycle(i + 1, config.default_pwm_values[i]);
    
    // Set all fans to 100%
    //for (i = 1; i <= NUMBER_OF_FANS; i++)
    //    pwm_set_duty_cycle(i, 0xFF);
    
    //printf(";;READY;\r\n");

    terminal_init();
    
    // Main loop
    while (1)
    {        
        // Check if we have received a character
        if (usart_buf_byte_ready)
        {
            char c = usart_buf_getch();

            //serial_control_do(c);
            terminal_char(c);
            terminal_task();
        } 
        
        if (afc_timer_timeout)
        {
            afc_task();
            afc_timer_timeout = 0;   
        }
    }    

}    
