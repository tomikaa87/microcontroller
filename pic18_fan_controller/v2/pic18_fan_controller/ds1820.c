/**
 * Driver for DS18X20 digital temperature sensor
 *
 * @file ds1820.c
 * @author Tamas Karpati
 * @date 2011-01-26
 */

#include "ds1820.h"
#include "one_wire.h"
#include "hardware_profile.h"

#include <stdio.h>

unsigned char last_discrepancy;
bit done_flag;

/**
 * Reads the current temperature value from
 * the DS18X20 1-wire sensor
 *
 * @return The read temperature multiplied by 100
 */
signed int ds1820_read()
{
    unsigned char hi, lo, temp_whole;
    unsigned int temp_i, temp_fraction;
    signed int temp;
    
    one_wire_reset();
    one_wire_write_byte(0xCC);
    one_wire_write_byte(0x44);
    
    delay_10us(20);
    
    one_wire_reset();
    one_wire_write_byte(0xCC);
    one_wire_write_byte(0xBE);
    lo = one_wire_read_byte();
    hi = one_wire_read_byte();
    temp_i = (hi << 8) + lo;
    
    if (temp_i & 0x8000)
    {
        temp_i = ~temp_i + 1;
    }
    
    temp = 0;
    
    temp_whole = temp_i >> (TEMP_RESOLUTION - 8);
    temp += temp_whole * 100;
    
    temp_fraction = temp_i << (4 - (TEMP_RESOLUTION - 8));
    temp_fraction &= 0x000F;
    temp_fraction *= 625;
    temp += temp_fraction / 100;
    
    if (temp_i & 0x8000)
    {
        temp *= -1;
    }

    return temp;
} 

/**
 * Reads the current temperature value from
 * a specific DS18X20 1-wire sensor
 *
 * @param rom_bits The 64 bit serial of the sensor to read temperature from
 * @return The read temperature multiplied by 100
 */
signed int ds1820_read_specific(unsigned char *rom_bits)
{
    unsigned char hi, lo, temp_whole, i;
    unsigned int temp_i, temp_fraction;
    signed int temp;
    
    one_wire_reset();
    
    one_wire_write_byte(0x55);
    // Send 64 bit ROM code
    for (i = 0; i < 64; i++)
    {
        one_wire_write_bit(rom_bits[i]);   
    } 
    
    one_wire_write_byte(0xBE);
    lo = one_wire_read_byte();
    hi = one_wire_read_byte();
    temp_i = (hi << 8) + lo;
    
    if (temp_i & 0x8000)
    {
        temp_i = ~temp_i + 1;
    }
    
    temp = 0;
    
    temp_whole = temp_i >> (TEMP_RESOLUTION - 8);
    temp += temp_whole * 100;
    
    temp_fraction = temp_i << (4 - (TEMP_RESOLUTION - 8));
    temp_fraction &= 0x000F;
    temp_fraction *= 625;
    temp += temp_fraction / 100;
    
    if (temp_i & 0x8000)
    {
        temp *= -1;
    }

    return temp;
}    

/**
 * Instructs all sensors on the bus to start temperature conversion
 */
void ds1820_do_conversion()
{
    if (one_wire_reset() > 0)
        return;
        
    // Send SKIP ROM command
    one_wire_write_byte(0xCC); 
    
    // Send CONVERT T command
    one_wire_write_byte(0x44);
    
    // Wait for CONVERT T command to finish
    delay_10us(100);
}    

/**
 * This function implements SEARCH ROM (Dallas algorithm) command
 *
 * @param rom_bits The 64 bit serial of the next found device
 * @return Error code, 1 if no errors occured, else 0
 */
unsigned char ds1820_next_device(unsigned char *romBits)
{
    unsigned char result;
    unsigned char presenceBit;
    unsigned char romBitIndex;
    unsigned char discrepancyMarker;
    unsigned char bit1, bit2;
    //unsigned char romBits[64];
    
    result = 0;
    
    if (done_flag)
    {
        done_flag = 0;
        return result;
    }
    
    #ifdef DEBUG
    printf("\r\n[SRCHROM] **** Looking for new device ****\r\n");
    #endif

    // Send reset signal
    presenceBit = one_wire_reset() == 0;
    
    #ifdef DEBUG
    printf("[SRCHROM] Presence bit: %d\r\n", presenceBit);
    #endif
    
    if (presenceBit == 0)
    {
        #ifdef DEBUG
        printf("[SRCHROM] Presence bit is '0'\r\n");   
        #endif
        
        last_discrepancy = 0;
        return result;
    }    
    
    romBitIndex = 0;
    discrepancyMarker = 0;
    
    // Send Search ROM command
    #ifdef DEBUG
    printf("[SRCHROM] Sending SEARCH ROM command...\r\n");
    #endif
    
    one_wire_write_byte(0xF0);
    
    while (romBitIndex < 64)
    {
        #ifdef DEBUG
        printf("[SRCHROM] ROM bit index: %d\r\n", romBitIndex);
        printf("[SRCHROM] LastDiscrepancy: %d\r\n", last_discrepancy);
        #endif
        
        bit1 = one_wire_read_bit();
        bit2 = one_wire_read_bit();
        
        #ifdef DEBUG
        printf("[SRCHROM] Read bits: %d, %d\r\n", bit1, bit2);
        #endif
        
        if (bit1 == 1 && bit2 == 1)
        {
            last_discrepancy = 0; 
            
            #ifdef DEBUG
            printf("[SRCHROM] bit1 == bit2 == 1, result = %d\r\n", result);
            #endif
            
            return result;  
        }    
        
        if (bit1 == 0 && bit2 == 0)
        {
            #ifdef DEBUG
            printf("[SRCHROM] bit1 == bit2 == 0, result = %d\r\n", result);
            #endif
            
            if (romBitIndex == last_discrepancy)
            {
                #ifdef DEBUG
                printf("[SRCHROM] romBitIndex == LastDiscrepancy\r\n");
                #endif
                
                romBits[romBitIndex] = 1;
                
                #ifdef DEBUG
                printf("[SRCHROM] Sending bit '%d'\r\n", romBits[romBitIndex]);
                #endif
                
                one_wire_write_bit(romBits[romBitIndex]);
                romBitIndex++;
            }
            else if (romBitIndex > last_discrepancy)
            {
                #ifdef DEBUG
                printf("[SRCHROM] romBitIndex > LastDiscrepancy\r\n");
                #endif
                
                romBits[romBitIndex] = 0;
                discrepancyMarker = romBitIndex;
                
                #ifdef DEBUG
                printf("[SRCHROM] Sending bit '%d'\r\n", romBits[romBitIndex]);
                #endif
                
                one_wire_write_bit(0);
                romBitIndex++;
            }
            else
            {
                if (romBits[romBitIndex] == 0)
                {
                    #ifdef DEBUG
                    printf("[SRCHROM] romBits[romBitIndex] == 0\r\n");
                    #endif
                    
                    discrepancyMarker = romBitIndex;
                }    
                
                #ifdef DEBUG
                printf("[SRCHROM] Sending bit '%d'\r\n", romBits[romBitIndex]);
                #endif
                
                one_wire_write_bit(romBits[romBitIndex]);
                romBitIndex++;
            }          
        }
        else
        {
            #ifdef DEBUG
            printf("[SRCHROM] bit1 != bit2\r\n");
            #endif
            
            romBits[romBitIndex] = bit1;
            
            #ifdef DEBUG
            printf("[SRCHROM] Sending bit '%d'\r\n", romBits[romBitIndex]);
            #endif
            
            one_wire_write_bit(bit1);
            romBitIndex++;
        }        
    } 
    
    #ifdef DEBUG
    printf("[SRCHROM] Discrepancy marker: %d\r\n", discrepancyMarker);
    #endif
    
    last_discrepancy = discrepancyMarker;
    
    if (last_discrepancy == 0)
    {
        #ifdef DEBUG
        printf("[SRCHROM] Setting DONE flag\r\n");
        #endif
        
        done_flag = 1;   
        result = 1;
    }
    else
    {
        result = 1;
    }        

    return result;       
}    

/**
 * This function implements SEARCH ROM (Dallas algorithm) command
 *
 * @param rom_bits The 64 bit serial of the first found device
 * @return Error code, 1 if no errors occured, else 0
 */
unsigned char ds1820_first_device(unsigned char *rom_bits)
{
    last_discrepancy = 0;
    done_flag = 0;
    
    return ds1820_next_device(rom_bits);
}    

/**
 * This function converts rom_bits_t to rom_bytes_t type
 *
 * @param rom_bits The source rom_bits_t variable
 * @param rom_bytes The destination rom_bytes_t variable
 */
void ds1820_rom_bits_to_rom_bytes(unsigned char *rom_bits, unsigned char *rom_bytes)
{
    unsigned char i, j;
    
    for (i = 0; i < 8; i++)
    {
        rom_bytes[i] = 0;
        
        for (j = 0; j < 8; j++)
        {
            if (rom_bits[i * 8 + j])
            {
                rom_bytes[i] += (1 << j);
            }       
        }       
    }     
}    

/**
 * This function converts rom_bytes_t to rom_bits_t type
 *
 * @param rom_bytes The source rom_bytes_t variable
 * @param rom_bits The destination rom_bits_t variable
 */
void ds1820_rom_bytes_to_rom_bits(unsigned char *rom_bytes, unsigned char *rom_bits)
{
    unsigned char i, j, k;
    
    k = 0;
    
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            rom_bits[k] = rom_bytes[i] & (1 << (j)) ? 1 : 0;   
            k++;
        }    
    }    
}    

/**
 * This function checks if the given sensor serial is indicates a virtual sensor.
 * A virtual sensor serial consists of 8 equal bytes (e.g. 01 01 01 01 01 01 01 01).
 * A virtual sensor a software based sensor that reads temperature values from the
 * client software. It can be used to control fans by external sensors like CPU or GPU sensor.
 *
 * @param The serial number bits
 * @return The number of the virtual sensor (0x01 - 0xFF) or 0 if it's not a virtual sensor
 */
 unsigned char ds1820_virtual_sensor(unsigned char *rom_bits)
{
    unsigned char i;
    rom_bytes_t rom_bytes;
    
    ds1820_rom_bits_to_rom_bytes(rom_bits, rom_bytes);
    
    for (i = 1; i < 8; i++)
    {
        if (rom_bytes[i] != rom_bytes[i - 1] || rom_bytes[i] == 0)
            return 0;
    }       
    
    return rom_bytes[0];
}    

/**
 * This function implements an alternative algorithm to find all sensors on the 1-wire bus
 *
 * @param rom_bits A pointer to an array that contains the serial numbers of the found sensors
 * @param max_devices The maximum number of sensors to find
 */
unsigned char ds1820_search_devices(rom_bits_t *rom_bits, unsigned char max_devices)
{
    unsigned int path;
    unsigned int next, pos, count;
    unsigned char bit1, bit2;
    unsigned char device_count;
    
    path = 0;
    device_count = 0;

    do
    {
        if (one_wire_reset())
        {
            #ifdef DEBUG
            printf("[SRCHDEV] Presence signal error\r\n");
            #endif
            
            return 0;   
        }   
            
        #ifdef DEBUG
        printf("[SRCHDEV] Sending SEARCH ROM command\r\n");
        #endif
        one_wire_write_byte(0xF0);
    
        next = 0;
        pos = 1;
        count = 0;
    
        do
        {
            bit1 = one_wire_read_bit();
            bit2 = one_wire_read_bit();
            
            if (bit1 == 0 && bit2 == 0)
            {
                if (pos & path)
                    bit1 = 1;
                else
                    next = (path & (pos - 1)) | pos;
                
                pos <<= 1;   
            }    
            
            one_wire_write_bit(bit1);
            // Save bit1 as ROM value bit
            rom_bits[device_count][count] = bit1;
            
            count++;
        }
        while (count < 64); 
        
        path = next;
        
        #ifdef DEBUG
        printf("[SRCHDEV] Device found.\r\n"); 
        printf("[SRCHDEV] Device info:\r\n");
        ds1820_print_device_info(rom_bits[device_count]);
        #endif
        
        device_count++;
    }
    while (path && device_count <= max_devices);    
    
    return device_count;
}    

/**
 * This function prints detailed information of a sensors given by its serial
 *
 * @param rom_bits The variable that stores the 64 bit serial of the sensor
 */
void ds1820_print_device_info(unsigned char *rom_bits)
{
    rom_bytes_t rom_bytes;
    unsigned char i;
    
    // Convert ROM bits to bytes
    ds1820_rom_bits_to_rom_bytes(rom_bits, rom_bytes);
    
    // Print device type
    switch (rom_bytes[0])
    {
        case 0x10:
            printf("Device type: DS18S20 (Family: 10)\r\n");
            break;
            
        case 0x28:
            printf("Device type: DS18B20 (Family: 28)\r\n");
            break;
            
        default:
            printf("Device type: UNKNOWN (Family: %02X)\r\n", rom_bytes[0]);
            break; 
    }
    
    // Print serial
    printf("Device serial: ");
    for (i = 1; i < 7; i++)
        printf("%02X ", rom_bytes[i]);
    
    // Print CRC
    printf("\r\nDevice ROM data CRC: %02X\r\n", rom_bytes[7]);        
}    
