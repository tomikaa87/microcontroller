/**
 * PWM controller module
 *
 * @file pwm.c
 * @author Tamas Karpati
 * @date 2011-01-26
 */

#include "pwm.h"
#include "interrupt.h"
#include "hardware_profile.h"

#include <htc.h>
#include <stdio.h>

/*** Setup functions ******************************************************************************/

/**
 * This function initializes the PIC's CCP modules and Timer 2
 */
void pwm_setup_modules()
{
    #ifdef DEBUG
    printf("[PWM] Setting up modules...\r\n");
    #endif
    
    // Setup Timer2
    PR2 = 0b00111111;
    T2CON = 0b00000101;
    #ifdef DEBUG
    printf("[PWM] Timer2 registers: PR2 = 0x%02X, T2CON = 0x%02X\r\n", PR2, T2CON);
    #endif
    
    // Setup PWM 1
    #ifdef DEBUG
    printf("[PWM] Setting up PWM 1...\r\n");
    #endif
    FAN_1_PWM_PIN_TRIS = 0;   
    CCP1CON = 0b00001100;
    CCPR1L = 0;    
    
    // Setup PWM 2
    #ifdef DEBUG
    printf("[PWM] Setting up PWM 2...\r\n");
    #endif
    FAN_2_PWM_PIN_TRIS = 0;   
    CCP2CON = 0b00001100;
    CCPR2L = 31;

    // Setup PWM 3
    #ifdef DEBUG
    printf("[PWM] Setting up PWM 3...\r\n");
    #endif
    FAN_3_PWM_PIN_TRIS = 0;   
    CCP3CON = 0b00001100;
    CCPR3L = 0;
    
    // Setup PWM 4
    #ifdef DEBUG
    printf("[PWM] Setting up PWM 4...\r\n");
    #endif
    FAN_4_PWM_PIN_TRIS = 0;   
    CCP4CON = 0b00001100;
    CCPR4L = 0;
    
    // Setup PWM 1
    #ifdef DEBUG
    printf("[PWM] Setting up PWM 5...\r\n");
    #endif
    FAN_5_PWM_PIN_TRIS = 0;   
    CCP5CON = 0b00001100;
    CCPR5L = 0;
}    

/**
 * This function configures the Timer 1 module and fan tacho input pins
 */
void pwm_setup_fan_speed_reading()
{
    #ifdef DEBUG
    printf("[PWM] Setting up fan speed reading...\r\n");
    #endif
    
    //Timer1 Registers Prescaler= 4 - TMR1 Preset = 61536 - Freq = 4000.00 Hz - Period = 0.000250 seconds
    T1CKPS1 = 0;   // bits 5-4  Prescaler Rate Select bits
    T1CKPS0 = 0;   // bit 4
    T1OSCEN = 0;   // bit 3 Timer1 Oscillator Enable Control bit 1 = on
    T1SYNC = 1;    // bit 2 Timer1 External Clock Input Synchronization Control bit...1 = Do not synchronize external clock input
    TMR1CS1 = 0;   // bit 1 Timer1 Clock Source Select bit...00 = Internal clock (FOSC/4)
    TMR1CS0 = 0;
    TMR1ON = 0;    // bit 0 enables timer
    TMR1H = 240;   // preset for timer1 MSB register
    TMR1L = 96;    // preset for timer1 LSB register
    TMR1IE = 1;
    GIE = 1;
    PEIE = 1;
    
    #ifdef DEBUG
    printf("[PWM] Timer1 registers: T1CON = 0x%02X, TMR1L = 0x%02X, TMR1H = 0x%02X\r\n",
           T1CON, TMR1L, TMR1H);
    #endif
    
    #ifdef DEBUG
    printf("[PWM] Setting up tacho input pins...\r\n");
    #endif
    FAN_1_TACHO_TRIS = 1;     // FAN 1 tacho input pin
    FAN_2_TACHO_TRIS = 1;     // FAN 2 tacho input pin
    FAN_3_TACHO_TRIS = 1;     // FAN 3 tacho input pin
    FAN_4_TACHO_TRIS = 1;     // FAN 4 tacho input pin
    FAN_5_TACHO_TRIS = 1;     // FAN 5 tacho input pin
}    

/*** Duty cycle handling **************************************************************************/

/**
 * Changes the duty cycle value for the give CCP module
 *
 * @param module_number The number of the CCP module (1-5 on 18F26K22)
 * @param duty_cycle The duty cycle value
 */
void pwm_set_duty_cycle(unsigned char module_number,
                      unsigned char duty_cycle)
{
    unsigned char ccprl, dcb0, dcb1;
    
    #ifdef DEBUG
    printf("[PWM] Changing PWM %d duty cycle to 0x%02X...\r\n", module_number, duty_cycle);
    #endif
    
    // Calculate register values from dutyCycle
    ccprl = duty_cycle >> 2;
    dcb1 = duty_cycle & 0x02 ? 1 : 0;
    dcb0 = duty_cycle & 0x01;
    
    #ifdef DEBUG
    printf("[PWM] CCPRxL = 0x%02X, DCxB0 = %d, DCxB1 = %d\r\n", ccprl, dcb0, dcb1);
    #endif
    
    switch (module_number)
    {
        case 1:
        {
            CCPR1L = ccprl;
            DC1B1 = dcb1;
            DC1B0 = dcb0;
            break;
        }    
        
        case 2:
        {
            CCPR2L = ccprl;
            DC2B1 = dcb1;
            DC2B0 = dcb0;
            break;
        } 
        
        case 3:
        {
            CCPR3L = ccprl;
            DC3B1 = dcb1;
            DC3B0 = dcb0;
            break;
        } 
        
        case 4:
        {
            CCPR4L = ccprl;
            DC4B1 = dcb1;
            DC4B0 = dcb0;
            break;
        } 
        
        case 5:
        {
            CCPR5L = ccprl;
            DC5B1 = dcb1;
            DC5B0 = dcb0;
            break;
        } 
        
        default:
            break;   
    }       
}

/**
 * Reads the duty cycle value from the given CCP module
 *
 * @param module_number The number of the CCP module (1-5 on 18F26K22)
 * @return The duty cycle value
 */
unsigned char pwm_get_duty_cycle(unsigned char module_number)
{
    unsigned char ccprl, dcb0, dcb1, duty_cycle;
    
    switch (module_number)
    {
        case 1:
        {
            ccprl = CCPR1L;
            dcb0 = DC1B0;
            dcb1 = DC1B1;
            break;
        } 
        
        case 2:
        {
            ccprl = CCPR2L;
            dcb0 = DC2B0;
            dcb1 = DC2B1;
            break;
        }
        
        case 3:
        {
            ccprl = CCPR3L;
            dcb0 = DC3B0;
            dcb1 = DC3B1;
            break;
        }
        
        case 4:
        {
            ccprl = CCPR4L;
            dcb0 = DC4B0;
            dcb1 = DC4B1;
            break;
        }
        
        case 5:
        {
            ccprl = CCPR5L;
            dcb0 = DC5B0;
            dcb1 = DC5B1;
            break;
        }   
        
        default:
            break;   
    }    
    
    duty_cycle = ccprl << 2;
    duty_cycle += dcb0;
    duty_cycle += (dcb1 << 1);
    
    return duty_cycle;
}                              

/*** Fan controller functions *********************************************************************/

/**
 * This function measures the rotational speed of a fan
 *
 * @param fan_number The number of the fan (1-5)
 * @return The fan speed in RPM
 */
unsigned int pwm_read_fan_speed(unsigned char fan_number)
{
    unsigned char counter;
    unsigned int max_timer_value;
    
    counter = 0;
    max_timer_value = 0;
    
    #ifdef DEBUG
    printf("[PWM] Measuring fan %d speed...\r\n", fan_number);
    #endif

    switch (fan_number)
    {
        case 1:
        {
            while (counter < 3)
            {
                TMR1ON = 0;
                TMR1H = 240;
                TMR1L = 96;
                
                timer_1_value = 0;
                TMR1IF = 0;
                TMR1ON = 1;
                while (!FAN_1_TACHO_PIN && timer_1_value < 600) continue;
                while (FAN_1_TACHO_PIN && timer_1_value < 600) continue;
                if (timer_1_value >= 600)
                {
                    TMR1ON = 0;
                    timer_1_value = 0;
                    return 0;
                }
                
                timer_1_value = 0;
            
                while (timer_1_value < 6)
                {
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //            _
                    // Wait for _|
                    while (!FAN_1_TACHO_PIN && timer_1_value < 600) continue;
            
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
            
                    TMR0ON = 0;
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //          _
                    // Wait for  |_
                    while (FAN_1_TACHO_PIN && timer_1_value < 600) continue;
                    
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
                    
                    TMR1ON = 0;
                }   
                
                if (timer_1_value > max_timer_value)
                    max_timer_value = timer_1_value;
                    
                counter++;
            }
            
            break;
        }    
        
        case 2:
        {
            while (counter < 3)
            {
                TMR1ON = 0;
                TMR1H = 240;
                TMR1L = 96;
                
                timer_1_value = 0;
                TMR1IF = 0;
                TMR1ON = 1;
                while (!FAN_2_TACHO_PIN && timer_1_value < 600) continue;
                while (FAN_2_TACHO_PIN && timer_1_value < 600) continue;
                if (timer_1_value >= 600)
                {
                    TMR1ON = 0;
                    timer_1_value = 0;
                    return 0;
                }
                
                timer_1_value = 0;
            
                while (timer_1_value < 6)
                {
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //            _
                    // Wait for _|
                    while (!FAN_2_TACHO_PIN && timer_1_value < 600) continue;
            
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
            
                    TMR0ON = 0;
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //          _
                    // Wait for  |_
                    while (FAN_2_TACHO_PIN && timer_1_value < 600) continue;
                    
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
                    
                    TMR1ON = 0;
                }   
                
                if (timer_1_value > max_timer_value)
                    max_timer_value = timer_1_value;
                    
                counter++;
            }
            
            break;
        }
        
        case 3:
        {
            while (counter < 3)
            {
                TMR1ON = 0;
                TMR1H = 240;
                TMR1L = 96;
                
                timer_1_value = 0;
                TMR1IF = 0;
                TMR1ON = 1;
                while (!FAN_3_TACHO_PIN && timer_1_value < 600) continue;
                while (FAN_3_TACHO_PIN && timer_1_value < 600) continue;
                if (timer_1_value >= 600)
                {
                    TMR1ON = 0;
                    timer_1_value = 0;
                    return 0;
                }
                
                timer_1_value = 0;
            
                while (timer_1_value < 6)
                {
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //            _
                    // Wait for _|
                    while (!FAN_3_TACHO_PIN && timer_1_value < 600) continue;
            
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
            
                    TMR0ON = 0;
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //          _
                    // Wait for  |_
                    while (FAN_3_TACHO_PIN && timer_1_value < 600) continue;
                    
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
                    
                    TMR1ON = 0;
                }   
                
                if (timer_1_value > max_timer_value)
                    max_timer_value = timer_1_value;
                    
                counter++;
            }
            
            break;
        }
        
        case 4:
        {
            while (counter < 3)
            {
                TMR1ON = 0;
                TMR1H = 240;
                TMR1L = 96;
                
                timer_1_value = 0;
                TMR1IF = 0;
                TMR1ON = 1;
                while (!FAN_4_TACHO_PIN && timer_1_value < 600) continue;
                while (FAN_4_TACHO_PIN && timer_1_value < 600) continue;
                if (timer_1_value >= 600)
                {
                    TMR1ON = 0;
                    timer_1_value = 0;
                    return 0;
                }
                
                timer_1_value = 0;
            
                while (timer_1_value < 6)
                {
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //            _
                    // Wait for _|
                    while (!FAN_4_TACHO_PIN && timer_1_value < 600) continue;
            
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
            
                    TMR0ON = 0;
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //          _
                    // Wait for  |_
                    while (FAN_4_TACHO_PIN && timer_1_value < 600) continue;
                    
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
                    
                    TMR1ON = 0;
                }   
                
                if (timer_1_value > max_timer_value)
                    max_timer_value = timer_1_value;
                    
                counter++;
            }
            
            break;
        }
        
        case 5:
        {
            while (counter < 3)
            {
                TMR1ON = 0;
                TMR1H = 240;
                TMR1L = 96;
                
                timer_1_value = 0;
                TMR1IF = 0;
                TMR1ON = 1;
                while (!FAN_5_TACHO_PIN && timer_1_value < 600) continue;
                while (FAN_5_TACHO_PIN && timer_1_value < 600) continue;
                if (timer_1_value >= 600)
                {
                    TMR1ON = 0;
                    timer_1_value = 0;
                    return 0;
                }
                
                timer_1_value = 0;
            
                while (timer_1_value < 6)
                {
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //            _
                    // Wait for _|
                    while (!FAN_5_TACHO_PIN && timer_1_value < 600) continue;
            
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
            
                    TMR0ON = 0;
                    timer_1_value = 0;
                    TMR1IF = 0;
                    TMR1ON = 1;
                    
                    //          _
                    // Wait for  |_
                    while (FAN_5_TACHO_PIN && timer_1_value < 600) continue;
                    
                    // Check if the fan is spining
                    if (timer_1_value >= 600)
                    {
                        TMR1ON = 0;
                        timer_1_value = 0;
                        break;   
                    }
                    
                    TMR1ON = 0;
                }   
                
                if (timer_1_value > max_timer_value)
                    max_timer_value = timer_1_value;
                    
                counter++;
            }
            
            break;
        }
            
        default:
            break;
    }    

    #ifdef DEBUG
    printf("[PWM] Timer1 counter = %u\r\n", max_timer_value);
    #endif
    
    if (timer_1_value > 0)
        return (unsigned int)(60000 / max_timer_value);   
    else
        return 0;
}    
