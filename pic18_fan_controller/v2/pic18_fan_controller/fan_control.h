/**
 * Fan controller that uses fancontrol's algorithm
 *
 * @file fan_control.h
 * @author Tamas Karpati
 * @date 2011-02-01
 */

#ifndef FAN_CONTROL_H
#define FAN_CONTROL_H

/*** Public API ***********************************************************************************/
void fan_control_task();

void fan_control_init();

void fan_control_display_assignments();
void fan_control_display_fan_config();

unsigned char fan_control_calibrate_fan(unsigned char fan_number);

void fan_control_do_fan_control(unsigned char fan_number);

unsigned char fan_speed_percent_to_duty_cycle(signed char percent,
										      unsigned char min_duty,
										      unsigned char max_duty);

#endif // FAN_CONTROL_H
