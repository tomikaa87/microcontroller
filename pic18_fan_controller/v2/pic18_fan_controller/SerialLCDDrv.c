/*** SerialLCD Driver *********************************************************/

#include "SerialLCDDrv.h"
#include "peripheral.h"

#include <htc.h>

/*** CONSTANTS ****************************************************************/

#define LCD_ENTRY_MODE_DEC_NOSHIFT      0x04
#define LCD_ENTRY_MODE_DEC_SHIFT        0x05
#define LCD_ENTRY_MODE_INC_NOSHIFT      0x06
#define LCD_ENTRY_MODE_INC_SHIFT        0x07

#define LCD_CURSOR_MODE_OFF             0x0C
#define LCD_CURSOR_MODE_BLINK           0x0D
#define LCD_CURSOR_MODE_UNDERLINE       0x0E

#define LCD_BAUD_SETUP_DELAY_MS         20
#define LCD_CLEAR_DELAY_MS              0
#define LCD_INIT_DELAY_MS               100
#define LCD_GOTOXY_DELAY_MS             0

#define LCD_BAUD_RATE_300               0x00
#define LCD_BAUD_RATE_1200              0x01
#define LCD_BAUD_RATE_2400              0x02
#define LCD_BAUD_RATE_4800              0x03
#define LCD_BAUD_RATE_9600              0x04
#define LCD_BAUD_RATE_19200             0x05
#define LCD_BAUD_RATE_38400             0x06
#define LCD_BAUD_RATE_57600             0x07
#define LCD_BAUD_RATE_115200            0x08

/*** COMMAND CODES ************************************************************/

#define LCD_CMD_INIT                    0x01
#define LCD_CMD_CLEAR                   0x02
#define LCD_CMD_ADDR                    0x03   /* Param 1: address, 0x00 - 0xFF */
#define LCD_CMD_GOTOXY                  0x04   /* Param 1: X 0x00 - 0xFF, Param 2: Y, 0x00 - 0xFF*/
#define LCD_CMD_BACKLIGHT               0x05   /* Param 1: level, 0x00 - 0xFF */
#define LCD_CMD_CONTRAST                0x06   /* Param 1: level, 0x00 - 0xFF */
#define LCD_CMD_SHIFT_CUR_LEFT          0x07
#define LCD_CMD_SHIFT_CUR_RIGHT         0x08
#define LCD_CMD_SHIFT_SCR_LEFT          0x09
#define LCD_CMD_SHIFT_SCR_RIGHT         0x0A
#define LCD_CMD_SET_ENTRY_MODE          0x0B   /* Param 1: LCD_ENTRY_MODE_XXX */
#define LCD_CMD_SET_CUR_MODE            0x0C    /* Param 1: LCD_CURSOR_MODE_XXX */
#define LCD_CMD_DEFINE_CUSTOM_CHAR      0x0D   /* Param 1: 0x00 - 0x07, 2-9: 0x00 - 0xF8 */
#define LCD_CMD_PUT_CUSTOM_CHAR         0x0E   /* Param 1: 0x00 - 0x07 */
#define LCD_CMD_SET_BAUD_RATE           0x0F   /* Param 1: LCD_BAUD_RATE_XXX */
   
#define LCD_CMD_RESET_CPU               0x1F

/*** FUNCTIONS ****************************************************************/

/*void lcd_init();
void lcd_gotoxy(unsigned char x, unsigned char y);
void lcd_putc(char c);
void lcd_puts(char *s);
void lcd_shift_screen(unsigned char left);
void lcd_shift_cursor(unsigned char left);
void lcd_set_entry_mode(unsigned char mode);
void lcd_set_cursor_mode(unsigned char mode);
void lcd_define_custom_char(unsigned char place, unsigned char *cust_char_bytes);
void lcd_clear();
void lcd_set_baud_rate(unsigned char baud_rate);
void lcd_set_backlight(unsigned char level);
void lcd_set_contrast(unsigned char level);
void lcd_reset_cpu();*/

/*** GLOBAL VARIABLES *********************************************************/

unsigned char current_baud_rate = LCD_BAUD_RATE_9600;

/*** IMPLEMENTATION ***********************************************************/

void delay_ms(unsigned int val)
{
    unsigned int i;
    
    for (i = 0; i < val; i++)
        __delay_ms(1);       
} 

void lcd_init()
{
    delay_ms(250);
    putch(LCD_CMD_INIT);
    delay_ms(LCD_INIT_DELAY_MS);
}

void lcd_gotoxy(unsigned char x, unsigned char y)
{
    putch(LCD_CMD_GOTOXY);
    putch(x - 1);
    putch(y - 1);
    delay_ms(LCD_GOTOXY_DELAY_MS);
}

void lcd_putc(char c)
{
    if (c >= 0x20)
        putch(c);
    else
        if (c <= 0x07)
        {
            putch(LCD_CMD_PUT_CUSTOM_CHAR);
            putch(c);
        }
}

void lcd_puts(char *s)
{
    while (*s)
        lcd_putc(*s++);
}

void lcd_shift_screen(unsigned char left)
{
    if (left)
        putch(LCD_CMD_SHIFT_SCR_LEFT);
    else
        putch(LCD_CMD_SHIFT_SCR_RIGHT);
}

void lcd_shift_cursor(unsigned char left)
{
    if (left)
        putch(LCD_CMD_SHIFT_CUR_LEFT);
    else
        putch(LCD_CMD_SHIFT_CUR_RIGHT);
}

void lcd_set_entry_mode(unsigned char mode)
{
    putch(LCD_CMD_SET_ENTRY_MODE);
    putch(mode);
}

void lcd_cnd_set_cursor_mode(unsigned char mode)
{
    putch(LCD_CMD_SET_CUR_MODE);
    putch(mode);
}

void lcd_define_custom_char(unsigned char place, unsigned char *cust_char_bytes)
{
    if (place <= 0x07)
    {
        unsigned char i;
        putch(LCD_CMD_DEFINE_CUSTOM_CHAR);
        putch(place);
        for (i = 0; i < 8; i++)
        {
            putch(cust_char_bytes[i]);
            delay_ms(1);
        }
    }
}

void lcd_clear()
{
    putch(LCD_CMD_CLEAR);
    delay_ms(LCD_CLEAR_DELAY_MS);
}

void lcd_set_baud_rate(unsigned char baud_rate)
{
    if (baud_rate <= LCD_BAUD_RATE_115200)
    {
        putch(LCD_CMD_SET_BAUD_RATE);
        putch(baud_rate);
        current_baud_rate = baud_rate;
        delay_ms(LCD_BAUD_SETUP_DELAY_MS);
    }
}

void lcd_set_backlight(unsigned char level)
{
    putch(LCD_CMD_BACKLIGHT);
    putch(level);
}

void lcd_set_contrast(unsigned char level)
{
    putch(LCD_CMD_CONTRAST);
    putch(level);
}

void lcd_reset_cpu()
{
    putch(LCD_CMD_RESET_CPU);
}
