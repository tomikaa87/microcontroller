/**
 * Serial communications module
 *
 * @file usart.c
 * @author Tamas Karpati
 * @date 2011-01-26
 */
 
#include "usart.h"
#include "hardware_profile.h"

#include <htc.h>

/**
 * This function configures the USART modules of the PIC
 */
void usart_setup()
{
    // Setup USART module #1
    TX91 = 0;
    SYNC1 = 0;
    BRGH1 = 1;
    SPBRG1 = 33; //103;
    SPBRGH1 = 0;
    SPEN1 = 1;
    CREN1 = 1;
    TXEN1 = 1;
    TRISC7 = 1;
    TRISC6 = 0; 
    
    // Setup USART module #2
    TX92 = 0;
    SYNC2 = 0;
    BRGH2 = 0;
    SPBRG2 = 103;
    SPBRGH2 = 0;
    SPEN2 = 1;
    TXEN2 = 1;
    TRISB7 = 1;
    TRISB6 = 0;  
    
    GIE = 1;
    PEIE = 1;
    RC1IE = 1;
} 

/**
 * Sends a byte trough USART
 *
 * @param byte The byte to send
 */
void putch(unsigned char byte) 
{
    // Wait for TX to become ready
	while(!TRMT1)
		continue;
		
	// Write the byte
	TXREG1 = byte;
}   

/**
 * Sends a byte trough USART2 module
 *
 * @param byte The byte to send
 */
void putch2(unsigned char byte) 
{
    // Wait for TX to become ready
	while(!TRMT2)
		continue;
		
	// Write the byte
	TXREG2 = byte;
}

/**
 * Reads a byte from the USART1 module's buffer register
 *
 * @return The read byte
 */
unsigned char getch(void)
{
    // Wait for incoming data
    while (!RC1IF)
        continue;

    // Read it out from the buffer
    return RCREG1;
}

/**
 * Reads a byte from the USART2 buffer's register
 *
 * @return The read byte
 */
unsigned char getch2(void)
{
    // Wait for incoming data
    while (!RC2IF)
        continue;

    // Read it out from the buffer
    return RCREG2;
}

/**
 * This function takes a byte from the USART buffer
 *
 * @return The read byte
 */
unsigned char usart_buf_getch()
{
    unsigned char c;
    
    // Wait untit a byte is ready to read from the USART buffer
    while (!usart_buf_byte_ready)
        continue;       
    
    c = usart_buf[usart_buf_next_out];
    usart_buf_next_out = (usart_buf_next_out + 1) % USART_BUF_SIZE;
    
    return c;
}
