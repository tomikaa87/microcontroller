/**
 * Interrupt handler
 *
 * @file interrupt.h
 * @author Tamas Karpati
 * @date 2011-01-26
 */

#ifndef INTERRUPT_H
#define INTERRUPT_H

unsigned int timer_1_value;

void interrupt isr();

#endif // INTERRUPT_H
