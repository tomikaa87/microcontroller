/*
 * 1-Wire driver for on-board DS18X20 sensor
 *
 * @file one_wire.h
 * @author: Tamas Karpati
 * @date: 2011-01-26
 */
 
 #include "hardware_profile.h"

#ifndef ONE_WIRE_H
#define ONE_WIRE_H

#ifndef ONE_WIRE_PIN
    #error "OneWire I/O Pin undefined! You must define ONE_WIRE_PIN to make 1-Wire work."
#endif

#ifndef ONE_WIRE_TRIS
    #error "OneWire I/O Pin TRIS undefined! You must define ONE_WIRE_TRIS to make 1-Wire work."
#endif

#define OW_FLOAT()  (ONE_WIRE_TRIS = 1)
#define OW_LOW()    (ONE_WIRE_PIN = 0, ONE_WIRE_TRIS = 0)
#define OW_HIGH()   (ONE_WIRE_PIN = 1, ONE_WIRE_TRIS = 0)

unsigned char one_wire_reset();
void one_wire_write_bit(unsigned char b);
void one_wire_write_byte(unsigned char b);
unsigned char one_wire_read_bit();
unsigned char one_wire_read_byte();

#endif // ONE_WIRE_H
