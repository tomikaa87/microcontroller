/**
 * Automatic Fan Control (AFC) module
 *
 * Target PWM calculations are based on fancontrol's (lm-sensors) formula
 *
 * @file afc.c
 * @author Tamas Karpati
 * @date 2011-08-17
 */

#include "afc.h"
#include "hardware_profile.h"
#include "usart.h"
#include "settings.h"
#include "ds1820.h"
#include "serial_control.h"
#include "pwm.h"

#include <stdio.h>
#include <string.h> // memcmp()

extern config_data_t config;
extern rom_bits_t sensor_serials[MAX_TEMP_SENSORS];
extern signed int virtual_sensor_temps[MAX_VIRTUAL_TEMP_SENSORS];
extern unsigned char sensor_count;

/*
signed int afc_min_temp[AFC_MAX_SENSOR_ASSIGNMENTS];
signed int afc_max_temp[AFC_MAX_SENSOR_ASSIGNMENTS];
signed int afc_current_temp[AFC_MAX_SENSOR_ASSIGNMENTS];
unsigned char afc_target_pwm[AFC_MAX_SENSOR_ASSIGNMENTS];
*/

void afc_task()
{
    unsigned char assign_indexes[AFC_MAX_SENSOR_ASSIGNMENTS], assign_count;
    unsigned char target_pwms[AFC_MAX_SENSOR_ASSIGNMENTS];
    unsigned char fan, i, j;

    if (sensor_count > 0)
    {
#ifdef DEBUG
        printf("[AFC] Performing temperature conversion of DS18x20 sensors\r\n");
#endif
        ds1820_do_conversion();
    }

    // Go through all fans
    for (fan = 1; fan <= NUMBER_OF_FANS; fan++)
    {
#ifdef DEBUG
        printf("\r\n[AFC] ** Fan = %d, MinPWM = %d, MaxPWM = %d\r\n", fan,
                config.min_pwm_values[fan - 1],
                config.max_pwm_values[fan - 1]);
#endif
        assign_count = 0;
        memset(assign_indexes, 0, sizeof (assign_indexes));

        for (i = 0; i < AFC_MAX_SENSOR_ASSIGNMENTS; i++)
        {
            if (config.afc_assignments[i].fan == fan)
            {
#ifdef DEBUG
                printf("[AFC]   Assignment found. Index = %d, MinTemp = %d, MaxTemp = %d\r\n", i,
                        config.afc_assignments[i].min_temp,
                        config.afc_assignments[i].max_temp);
#endif
                assign_indexes[assign_count++] = i;
            }
        }

        if (assign_count == 0)
        {
#ifdef DEBUG
            printf("[AFC]   No assignments found for this fan.\r\n");
#endif
            continue;
        }

        for (i = 0; i < assign_count; i++)
        {
            rom_bytes_t rom_bytes;
            signed int temperature;

            // Read sensor ROM code from config
            memcpy(rom_bytes, config.afc_assignments[assign_indexes[i]].serial,
                    sizeof (rom_bytes_t));

            // Check if ROM code belogns to a virtual sensor
            for (j = 1; j < sizeof (rom_bytes_t); j++)
                if (rom_bytes[j - 1] != rom_bytes[j])
                    break;
            if (j == 8)
            {
                // Read virtual sensor temperature
                temperature = virtual_sensor_temps[rom_bytes[0]];
#ifdef DEBUG
                printf("[AFC]   Virtual sensor #%d temperature = %d\r\n", rom_bytes[0],
                       temperature);
#endif
            }
            else
            {
                // Read DS18x20 sensor
                rom_bits_t rom_bits;

                ds1820_rom_bytes_to_rom_bits(rom_bytes, rom_bits);
                temperature = ds1820_read_specific(rom_bits);
            }

            target_pwms[i] = calculate_target_pwm(temperature,
                    config.afc_assignments[assign_indexes[i]].min_temp,
                    config.afc_assignments[assign_indexes[i]].max_temp,
                    config.min_pwm_values[fan - 1],
                    config.max_pwm_values[fan - 1]);
#ifdef DEBUG
            printf("[AFC]   Calculated PWM value = %d, Diff = \r\n", target_pwms[i],
                    pwm_get_duty_cycle(fan));
#endif
        }
    }

    
    // Check if there are sensors connected
    //if (sensor_count == 0)
      //  return;
        
    /*printf("afc {\r\n");
        
    // Initiate temperature conversion on sensors
    ds1820_do_conversion();
    
    // Run through all fans
    for (fan = 0; fan < NUMBER_OF_FANS; fan++)
    {
        // Control the fan only if the AFC is enabled for it
        if (config.afc_states[fan] == 1)   
        {
            unsigned char min_pwm, max_pwm, pwm, temp_count, i, j, max_target_pwm;
            
            printf("  fan=%d {\r\n", fan);
            
            // Load PWM values from the configuration
            min_pwm = config.min_pwm_values[fan];
            max_pwm = config.max_pwm_values[fan];
            
            // Sanity check
            if (min_pwm > max_pwm || max_pwm == 0)
                continue;
            
            // Read all assigned sensors
            temp_count = 0;
            for (i = 0; i < AFC_MAX_SENSOR_ASSIGNMENTS; i++)
            {
                rom_bits_t rom_bits;
                rom_bytes_t rom_bytes;
                unsigned char virtual_sensor;
                
                // Check if the assignment is valid
                if (config.afc_assignments[i].fan < 1)
                    continue;
                    
                // Convert sensor serial bytes to bits
                memcpy(rom_bytes, config.afc_assignments[i].serial, sizeof (rom_bytes));
                ds1820_rom_bytes_to_rom_bits(rom_bytes, rom_bits);
                
                // Check if the assigned sensor is virtual and read its temperature
                virtual_sensor = ds1820_virtual_sensor(rom_bits);
                if (virtual_sensor)
                {
                    afc_current_temp[temp_count] = virtual_sensor_temps[virtual_sensor];
                    afc_min_temp[temp_count] = config.min_virtual_temps[virtual_sensor];
                    afc_max_temp[temp_count] = config.max_virtual_temps[virtual_sensor];
                    temp_count++;
                    continue;    
                }   
                    
                // Search and read corresponding sensor
                for (j = 0; j < sensor_count; j++)
                {
                    // Skip if sensor serial bits don't match
                    if (memcmp(sensor_serials[j], rom_bits, sizeof (rom_bits)) > 0)
                        continue;
                        
                    afc_current_temp[temp_count] = ds1820_read_specific(rom_bits);
                    afc_min_temp[temp_count] = config.afc_assignments[i].min_temp;
                    afc_max_temp[temp_count] = config.afc_assignments[i].max_temp;
                    temp_count++;
                }
            }    
            
            // Check if the fan has sensors assigned
            if (temp_count == 0)
                continue;
                
            // Calculate target PWM values and find the largest
            for (i = 0; i < temp_count; i++)
            {
                // Sanity check
                if (afc_min_temp[i] == afc_max_temp[i] || afc_min_temp[i] >= afc_max_temp[i])
                {
                    //afc_target_pwm[i] = 0;
                    continue;
                }    
                
                afc_target_pwm[i] pwm = calculate_target_pwm(afc_current_temp[i], afc_min_temp[i], 
                                                         afc_max_temp[i], min_pwm, max_pwm);
                if (pwm > max_target_pwm)
                    max_target_pwm = pwm;
            }   
            
            // Print some debug
            printf("    tempc=%d minpwm=%d maxpwm=%d target=%d\r\n",
                   fan, temp_count, min_pwm, max_pwm, max_target_pwm);
            printf("    temps: ");
            for (i = 0; i < temp_count; i++)
                printf("%d ", afc_current_temp[i]);
            printf("\r\n  }\r\n");
        }    
    }   
    
    printf("}\r\n"); */
}    

unsigned char calculate_target_pwm(signed int temp, signed int min_temp, signed int max_temp,
                                   unsigned char min_pwm, unsigned char max_pwm)
{
    // pwmval="(${tval}-${mint})*(${maxpwm}-${minso})/(${maxt}-${mint})+${minso}"
    
    if (temp <= min_temp)
        return min_pwm;
    else if (temp >= max_temp)
        return max_pwm;
    else 
    {
        // Linear PWM calculation
        return (unsigned char)(((float)temp - (float)min_temp) * (float)max_pwm / 
            ((float)max_temp - (float)min_temp));   
    }    
}    
