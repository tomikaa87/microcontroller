/*
 * Driver for DS18X20 digital temperature sensor
 *
 * Author: Tamas Karpati
 * Date: 2011-01-26
 */

#ifndef DS1820_H
#define DS1820_H

#define TEMP_RESOLUTION 12
//#define DEBUG

typedef unsigned char rom_bits_t[64];
typedef unsigned char rom_bytes_t[8];


unsigned char ds1820_next_device(unsigned char *rom_bits);
unsigned char ds1820_first_device(unsigned char *rom_bits);

void ds1820_rom_bits_to_rom_bytes(unsigned char *rom_bits, unsigned char *rom_bytes);
void ds1820_rom_bytes_to_rom_bits(unsigned char *rom_bytes, unsigned char *rom_bits);

unsigned char ds1820_search_devices(rom_bits_t *rom_bits, unsigned char max_devices);

void ds1820_do_conversion();

signed int ds1820_read();
signed int ds1820_read_specific(unsigned char *rom_bits);

void ds1820_print_device_info(unsigned char *rom_bits);

unsigned char ds1820_virtual_sensor(unsigned char *rom_bits);

#endif // DS1820_H
