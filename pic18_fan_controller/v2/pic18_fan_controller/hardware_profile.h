/**
 * Header for all device-dependent parameters
 *
 * @file hardware_profile.h
 * @author Tamas Karpati
 * @date 2011-01-26
 */

#ifndef HARDWARE_PROFILE_H
#define HARDWARE_PROFILE_H

#include <htc.h>

//#define DEBUG

#define VERSION_STRING              "0.9.0"

#define SETTINGS_STRUCT_EEPROM_ADDR 0x0000

#define USART_BUF_SIZE              128

#define _XTAL_FREQ                  64000000ul

#define ONE_WIRE_PIN                (PORTAbits.RA0)
#define ONE_WIRE_TRIS               (TRISA0)

#define FAN_1_PWM_PIN_TRIS          (TRISC2)
#define FAN_1_TACHO_PIN             (PORTCbits.RC3)
#define FAN_1_TACHO_TRIS            (TRISC3)
#define FAN_2_PWM_PIN_TRIS          (TRISC1)
#define FAN_2_TACHO_PIN             (PORTCbits.RC0)
#define FAN_2_TACHO_TRIS            (TRISC0)
#define FAN_3_PWM_PIN_TRIS          (TRISB5)
#define FAN_3_TACHO_PIN             (PORTBbits.RB4)
#define FAN_3_TACHO_TRIS            (TRISB4)
#define FAN_4_PWM_PIN_TRIS          (TRISB0)
#define FAN_4_TACHO_PIN             (PORTCbits.RC5)
#define FAN_4_TACHO_TRIS            (TRISC5)
#define FAN_5_PWM_PIN_TRIS          (TRISA4)
#define FAN_5_TACHO_PIN             (PORTCbits.RC4)
#define FAN_5_TACHO_TRIS            (TRISC4)

#define MAX_TEMP_SENSORS            8 // The maximum number of DS1820 temperature sensors
#define MAX_VIRTUAL_TEMP_SENSORS    8 // The maxumum number of software sensors
#define NUMBER_OF_FANS              5 // The maximum number of fans

#if (NUMBER_OF_FANS > 9)
    #error "Maximum fan number is 9. For more fans, you have to modify the source significantly."
#endif

// AFC settings
#define AFC_DEFAULT_SMALL_DELTA     2
#define AFC_DEFAULT_BIG_DELTA       5
#define AFC_DEFAULT_HUGE_DELTA      10
#define AFC_MAX_SENSOR_ASSIGNMENTS  30

// Delay routines
// PIC_CLK = 64MHz
#define dly125n asm("nop"); asm("nop");
#define dly500n dly125n;dly125n;dly125n;dly125n
#define dly1u	dly500n;dly500n
#define dly2u	dly1u;dly1u
#define dly3u	dly2u;dly1u
#define dly4u	dly2u;dly2u
#define dly5u	dly2u;dly2u;dly1u
#define dly9u   dly4u;dly5u
#define dly10u  dly5u;dly5u
#define dly20u  dly10u;dly10u
#define dly50u  dly10u;dly10u;dly10u;dly10u;dly10u

#define delay_10us(x) unsigned int _dcnt = x; while (_dcnt) { dly9u; dly500n; _dcnt--; }

#define delay_msec(x) \
    do \
    { \
        unsigned long _dcnt; \
        _dcnt = x * ((unsigned long)(0.0001/(1.0/_XTAL_FREQ)/6)) - 100; \
        while (_dcnt--); \
    } while (0)    

#endif // HARDWARE_PROFILE_H


