/* 
 * File:   terminal.h
 * Author: ToMikaa
 *
 * Created on 2012. szeptember 19., 17:20
 */

#ifndef TERMINAL_H
#define	TERMINAL_H

void terminal_init();
void terminal_char(char c);
void terminal_task();

#endif	/* TERMINAL_H */

