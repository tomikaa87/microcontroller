/**
 * PWM controller module
 *
 * @file pwm.h
 * @author Tamas Karpati
 * @date 2011-01-26
 */
 
#ifndef PWM_H
#define PWM_H

//#define DEBUG

void pwm_setup_modules();
void pwm_setup_fan_speed_reading();

void pwm_set_duty_cycle(unsigned char module_number,
                      unsigned char duty_cycle);
unsigned char pwm_get_duty_cycle(unsigned char module_number);
                      
unsigned int pwm_read_fan_speed(unsigned char fan_number);
                      

#endif // PWM_H
