/**
 * Serial control module
 *
 * @file serial_control.c
 * @author Tamas Karpati
 * @date 2011-03-08
 */
 
#include "serial_control.h"
#include "usart.h"
#include "fan_control.h"
#include "pwm.h"
#include "settings.h"

#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

extern config_data_t config;

typedef void (*func_ptr)();

typedef struct
{
    char opcode;                    // The opcode for the function
    func_ptr f;                     // Pointer to the function
    unsigned char param_bytes;      // Parameter bytes count needed by the function
}
func_desc;    

#define OP_CODE_COUNT   21
const func_desc OpFunctions[OP_CODE_COUNT] = {
    {'A', &send_version_number, 0},
    {'B', &search_one_wire_devices, 0},
    {'C', &send_one_wire_rom_codes, 0},
    {'D', &do_sensors_temp_conversion, 0},
    {'E', &set_virtual_sensor_temp, 21},        // Param: ROM(16 HEX)SIGN(+|-)TEMP(4 INT)
    {'F', &send_sensor_temp, 16},               // Param: ROM(16 HEX)
    {'G', &calibrate_fans, 0},
    {'H', &calibrate_fan, 1},                   // Param: FAN(1 INT)
    {'I', &read_fan_speed, 1},                  // Param: FAN(1 INT)
    {'J', &set_fan_pwm, 3},                     // Param: FAN(1 INT)DUTY(2 HEX)
    {'K', &read_fan_pwm, 1},                    // Param: FAN(1 INT)
    {'L', &assign_sensor, 27},                  // Param: FAN(1 INT)ROM(16 HEX)SIGN(+|-)MIN(4 INT)SIGN(+|-)MAX(4 INT)
    {'M', &delete_sensor_assignment, 17},       // Param: FAN(1 INT)ROM(16 HEX)
    {'N', &send_sensor_assignments, 0},
    //{'O', &send_virtual_sensor_assignments, 0},
    {'P', &save_settings, 0},
    {'Q', &set_fan_pwm_values, 9},              // Param: FAN(1 INT)MIN(2 HEX)MAX(2 HEX)START(2 HEX)STOP(2 HEX)
    //{'R', &set_sensor_temp_values, 11},         // Param: ROM(16 HEX)SIGN(+|-)MIN(4 INT)SIGN(+|-)MAX(4 INT)
    {'S', &set_afc_state, 2},                   // Param: FAN(1 INT)STATE(1|0)
    {'T', &get_afc_states, 0},
    {'U', &set_fan_default_pwm, 3},             // Param: FAN(1 INT)DUTY(2 HEX)
    {'Z', &reset_device, 0},
    {'?', &send_usage, 0}
};  

#define PARAM_BUF_SIZE  28
char param_buf[PARAM_BUF_SIZE];  

/*** Public functions *****************************************************************************/

/**
 * Checks if the given character is an opcode or not.
 *
 * @return -1 if the character is not an opcode, else the number of the opcode
 */
int serial_control_is_opcode(char c)
{
    unsigned char i = 0;
    
    while (OpFunctions[i].opcode != c && i < OP_CODE_COUNT)
        i++; 
    
    if (i == OP_CODE_COUNT)
        return -1;
        
    return (int)i;
}    

/**
 * Executes the command associated to the given \p opcode.
 * @param opcode The opcode of the command.
 */
void serial_control_do(char opcode)
{
    int opcode_index;
    func_desc *f_d;
    
    opcode_index = serial_control_is_opcode(opcode);
    
    if (opcode_index >= 0) 
    {
        f_d = (func_desc *)&(OpFunctions[opcode_index]);
        
        switch (read_param_bytes(f_d->param_bytes))
        {
            case 0:
                printf(";;ERROR:%c:0:%s;\r\n", opcode, param_buf);
                break;
                
            case 1:
                f_d->f();
                break;
                
            default:
                // Reset
                memset(param_buf, 0, sizeof(param_buf));
        }    
    }         
}    

/*** Utility functions ****************************************************************************/

/**
 * Reads 'n' parameter bytes from the USART and puts them in the parameter buffer.
 *
 * @param n The number of requested bytes
 * @return 0 on error, 1 on success
 */
unsigned char read_param_bytes(const unsigned char n)
{
    unsigned char i;
    char c;
    
    // Check if n is greater than 0
    if (n == 0)
        return 1;
        
    // Check if requested data fits in the parameter buffer
    if (n > PARAM_BUF_SIZE)
        return 0;
        
    c = usart_buf_getch();
    
    // Reset serial control if first character is a '#'
    if (c == '#')
        return 2;
        
    // Wait for ':' start character
    if (c != ':')
        return 0;
        
    // Clear the buffer
    memset(param_buf, 0, sizeof(param_buf));
        
    // Read n bytes
    for (i = 0; i < n; i++)
    {
        c = usart_buf_getch();
        
        // Reset serial control if received character is '#'
        if (c == '#')
            return 2;
        
        // Check if the data stream is prematurely ended
        if (c == ';')
            return 0;
            
        param_buf[i] = c;
    }    
    
    c = usart_buf_getch();
    
    // Reset serial control if last received character is '#'
    if (c == '#')
        return 2;
        
    // Wait for ';' end character
    if (c != ';')
        return 0;
        
    return 1;
}  

/**
 * Reads a sensor ROM code from the given \p buf buffer. The begin index of the code is held 
 * by \p from.
 * If the read code is invalid, 0 will be returned.
 *
 * @param buf The input buffer that contains the string.
 * @param from The index of the first character of the string.
 * @param rom_bytes The ROM code parsed out from the string.
 * @return 1 if the parsing was successful, otherwise 0.
 */
unsigned char read_rom_bytes(const char *buf, 
                             const unsigned char from,
                             rom_bytes_t rom_bytes)
{
    unsigned char i, j;
    
    if (from + sizeof (rom_bytes_t) > strlen(buf))
    {
        #ifdef DEBUG
        printf("[SERIAL] read_rom_bytes: 'from' index invalid (from = %d, buflen = %d, reqlen = %d)\r\n", 
            from, strlen(buf), sizeof (rom_bytes_t) + from);
        #endif
        
        return 0; 
    }    
    
    for (i = 0; i < sizeof (rom_bytes_t); i++)
    {
        unsigned char byte = 0;
              
        // Read a byte (two hexadecimal characters)  
        for (j = 0; j < 2; j++)
        {
            char c = param_buf[from + i * 2 + j];
            c = tolower(c);

            if (c >= '0' && c <= '9')
                byte += c - '0';
            else if (c >= 'a' && c <= 'f')
                byte += c - 'a' + 10;
            else
                return 0;   
            
            if (j == 0)
                byte <<= 4;
        }   

        rom_bytes[i] = byte;
    }
    
    #ifdef DEBUG
    printf("[SERIAL] read_rom_bytes: ROM code = ");
    for (i = 0; i < sizeof (rom_bytes_t); i++)
        printf("%02X", rom_bytes[i]);
    printf("\r\n");
    #endif
    
    return 1;
} 

/**
 * Reads a temperature string from the given buffer.
 *
 * @param buf The input buffer that contains the temperature string.
 * @param from The index of the first character of the string.
 * @param temp The temperature parsed out from the string.
 * @return 1 if the parsing was successful, otherwise 0.
 */
unsigned char read_temperature(const char *buf,
                               const unsigned char from,
                               signed int *temp)
{
    signed int temperature;
    unsigned char i, j;
    
    if (from + 5 > strlen(buf))
    {
        #ifdef DEBUG
        printf("[SERIAL] read_temperature: 'from' index invalid (from = %d, buflen = %d, reqlen = %d)\r\n", 
            from, strlen(buf), from + 5);
        #endif
        
        return 0; 
    }   
    
    temperature = 0; 
    
    #ifdef DEBUG
    printf("[SERIAL] read_temperature: buffer = ");
    #endif
    
    for (i = 0; i < 4; i++)
    {
        char c;
        unsigned int tmp;
        
        c = param_buf[i + from + 1];
        
        #ifdef DEBUG
        putch(c);
        #endif
        
        if (c >= '0' && c <= '9')
            tmp = c - '0';
        else
           return 0;  
        
        for (j = i; j < 3; j++)
            tmp *= 10;  

        temperature += tmp; 
    }
    
    #ifdef DEBUG
    printf(", sign = '%c'\r\n", buf[from]);
    #endif
    
    // Read sign
    if (buf[from] == '-')
        temperature *= -1;
    else if (buf[from] != '+')
        return 0;
        
    #ifdef DEBUG
    printf("[SERIAL] read_temperature: temp=%0.2f\r\n", (float)temperature / 100.0);
    #endif
    
    *temp = temperature;
        
    return 1;
}

/**
 * Reads a hexadecimal byte from the given buffer.
 *
 * @param buf The input buffer that contains the hexadecimal byte string.
 * @param from The index of the first character of the string.
 * @param byte The byte parsed out from the string.
 * @return 1 if the parsing was successful, otherwise 0.
 */
unsigned char read_hex_byte(const char *buf,
                            const unsigned char from,
                            unsigned char *byte)
{
    unsigned char i, b;
    
    if (from + 2 > strlen(buf))
    {
        #ifdef DEBUG
        printf("[SERIAL] read_hex_byte: 'from' index invalid (from = %d, buflen = %d, reqlen = %d)\r\n", 
            from, strlen(buf), from + 2);
        #endif
        
        return 0;   
    }    
    
    b = 0;            
    for (i = 0; i < 2; i++)
    {
        char c = param_buf[i + from];
        c = tolower(c);

        if (c >= '0' && c <= '9')
            b += c - '0';
        else if (c >= 'a' && c <= 'f')
            b += c - 'a' + 10;
        else
            return 0;   
        
        if (i == 0)
            b <<= 4;
    }
    
    #ifdef DEBUG
    printf("[SERIAL] read_hex_byte: value = 0x%02X\r\n", b);
    #endif
    
    *byte = b;
    
    return 1;
}   

/**
 * Reads a fan number byte from the given buffer.
 *
 * @param buf The input buffer that contains the number string.
 * @param from The index of the first character of the string.
 * @param fan The fan number parsed out from the string.
 * @return 1 if the parsing was successful, otherwise 0.
 */
unsigned char read_fan_number(char *buf,
                              const unsigned char from,
                              unsigned char *fan)
{ 
    if (from > strlen(buf))
    {
        #ifdef DEBUG
        printf("[SERIAL] read_fan_number: 'from' index invalid (from = %d, buflen = %d, reqlen = %d)\r\n", 
            from, strlen(buf), from);
        #endif
        
        return 0;   
    }    
    
    if (!(buf[from] >= '1' && buf[from] <= NUMBER_OF_FANS + '0'))
        return 0;
        
    *fan = buf[from] - '0';
    
    #ifdef DEBUG
    printf("[SERIAL] read_fan_number: value = %d\r\n", *fan);
    #endif
    
    return 1;
}                                                                                                     

/*** Serial control functions *********************************************************************/  

/**
 * Writes the firmware version number to the console.
 */
void send_version_number()
{
    printf(";;OK:A:0:%s;\r\n", VERSION_STRING);
}

/**
 * Looks for connected 1-Wire sensors.
 */
void search_one_wire_devices()
{
    // Look for connected DS18X20 sensors
    sensor_count = ds1820_search_devices(sensor_serials, MAX_TEMP_SENSORS);
    
    printf(";;OK:B:0:%u;\r\n", sensor_count);
}

/**
 * Writes ROM codes of the connected sensors to the console.
 */
void send_one_wire_rom_codes()
{
    unsigned char i, j;
    rom_bytes_t rom_bytes;
    
    if (sensor_count == 0)
    {
        printf(";;ERROR:C:0;\r\n");   
    }    
    else
    {
        printf(";;OK:C:0:");
        for (i = 0; i < sensor_count; i++)
        {
            ds1820_rom_bits_to_rom_bytes(sensor_serials[i], &rom_bytes);
            for (j = 0; j < 8; j++)
                printf("%02X", rom_bytes[j]);  
            
            if (i < sensor_count - 1)
                putch(':');
        }    
        putch(';');
        putch('\r');
        putch('\n');
    }
}  

/**
 * Initiates a temperature conversion on the 1-Wire sensors.
 */
void do_sensors_temp_conversion()
{
    // Instruct all sensors to do temperature conversion
    ds1820_do_conversion();
    
    printf(";;OK:D:0;\r\n");   
}      

/**
 * Reads the temperature of a 1-Wire sensor and writes the value to the console.
 */
void send_sensor_temp()
{
    unsigned char i, j;
    rom_bytes_t rom_bytes;
    rom_bits_t rom_bits;
    
    // Read ROM code
    if (!read_rom_bytes(param_buf, 0, rom_bytes))
    {
        printf(";;ERROR:F:0;\r\n");  
        return; 
    }       
    
    // Read the sensor corresponding to the ROM code
    ds1820_rom_bytes_to_rom_bits(rom_bytes, &rom_bits);
    
    printf(";;OK:F:0:");
    
    // Send ROM bytes
    for (i = 0; i < 8; i++)
        printf("%02X", rom_bytes[i]); 
    
    // Send temperature
    printf(":%d;\r\n", ds1820_read_specific(rom_bits));
}   

/**
 * Changes the stored temperature of a virtual sensor.
 */
void set_virtual_sensor_temp()
{
    unsigned char i, j;
    signed int temperature;
    rom_bytes_t rom_bytes;
    
    // Read ROM code and temperature
    if (!read_rom_bytes(param_buf, 0, rom_bytes) ||
            !read_temperature(param_buf, 16, &temperature))
    {
        printf(";;ERROR:E:0;\r\n");  
        return; 
    }

    // Check if all ROM bytes are equal
    for (i = 1; i < 8; i++)
        if (rom_bytes[i] != rom_bytes[i - 1])
            break;   
    
    // Check if the virtual ROM code is really virtual
    if (i != 8 || rom_bytes[0] >= MAX_VIRTUAL_TEMP_SENSORS ||
        rom_bytes[0] == 0)
    {
        printf(";;ERROR:E:%d;\r\n", rom_bytes[0]);
        return;
    }
        
    // Set the virtual sensor's value
    virtual_sensor_temps[rom_bytes[0]] = temperature;
    
    printf(";;OK:E:%d;\r\n", rom_bytes[0]);
}     

/**
 * Initiates a fan speed calibration on all connected fans.
 */
void calibrate_fans()
{
    unsigned char i, results[NUMBER_OF_FANS];
    
    // Iterate through all fans and calibrate them
    for (i = 1; i <= NUMBER_OF_FANS; i++)
        results[i - 1] = fan_control_calibrate_fan(i);   
    
    printf(";;OK:G:0:");
    for (i = 0; i < NUMBER_OF_FANS; i++)
    {
        printf("%d", results[i]);
        if (i < NUMBER_OF_FANS - 1)
            printf(":");
    }
    printf(";\r\n");         
}

/**
 * Initiates a fan speed calibration on a specific fan.
 */
void calibrate_fan()
{
    char c;
    
    c = param_buf[0];
    
    if (!(c >= '1' && c <= NUMBER_OF_FANS + '0'))
    {
        printf(";;ERROR:H:%c;\r\n", c);
        return;
    }
    
    printf(";;OK:H:%d;\r\n", fan_control_calibrate_fan(c - '0'));    
}

/**
 * Reads the speed of a specific fan and writes the RPM value to the console.
 */
void read_fan_speed()
{
    char c;
    
    c = param_buf[0];
    
    if (!(c >= '1' && c <= NUMBER_OF_FANS + '0'))
    {
        printf(";;ERROR:I:%c;\r\n", c);
        return;
    }
    
    printf(";;OK:I:%c:%04u;\r\n", c, pwm_read_fan_speed(c - '0'));
}

/**
 * Changes a specific fan's PWM duty cycle.
 */
void set_fan_pwm()
{
    unsigned char fan, pwm;
    
    if (!read_fan_number(param_buf, 0, &fan) ||
            !read_hex_byte(param_buf, 1, &pwm))
    {
        printf(";;ERROR:J:%d;\r\n", fan);
        return;
    }      
    
    pwm_set_duty_cycle(fan, pwm);
    
    printf(";;OK:J:%d;\r\n", fan); 
}    

/**
 * Reads a specific fan's PWM duty cycle and write it to the console.
 */
void read_fan_pwm()
{
    unsigned char fan;
    
    if (!read_fan_number(param_buf, 0, &fan))
    {
        printf(";;ERROR:K:%d;\r\n", fan);
        return;
    }
    
    printf(";;OK:K:%d:%02X;\r\n", fan, pwm_get_duty_cycle(fan));
}    

/**
 * Assigns a specific temperature sensor to a fan.
 */
void assign_sensor()
{
    unsigned char fan;
    rom_bytes_t rom_bytes;
    signed int min_temp, max_temp, i;
    
    // Read ROM code, fan number and temperatures
    if (!read_fan_number(param_buf, 0, &fan) ||
            !read_rom_bytes(param_buf, 1, rom_bytes) ||
            !read_temperature(param_buf, 17, &min_temp) ||
            !read_temperature(param_buf, 22, &max_temp))
    {
        printf(";;ERROR:L:0;\r\n");  
        return; 
    } 
    
    // Store the assignment
    i = settings_add_sensor_assignment(rom_bytes, fan, min_temp, max_temp);
    if (i == -1)
    {
        printf(";;ERROR:L:0;\r\n");
        return;   
    }      

    printf(";;OK:L:%d:", fan);
    
    // Send ROM bytes
    for (i = 0; i < 8; i++)
        printf("%02X", rom_bytes[i]);
    
    printf(":%d:%d;\r\n", min_temp, max_temp);
} 

/**
 * Removes a fan - sensor assignment.
 */
void delete_sensor_assignment()
{
    unsigned char fan;
    rom_bytes_t rom_bytes;
    
    if (!read_fan_number(param_buf, 0, &fan) ||
            !read_rom_bytes(param_buf, 1, rom_bytes) ||
            settings_remove_sensor_assignment(rom_bytes, fan) == -1)
    {
        printf(";;ERROR:M:%d;\r\n", fan);
        return;
    }   
    
    printf(";;OK:M:%d;\r\n", fan);             
}         

/**
 * Writes the fan - sensor assignments to the console.
 */
void send_sensor_assignments()
{
    unsigned char i, j;
    
    printf(";;OK:N:0:");
    for (i = 0; i < AFC_MAX_SENSOR_ASSIGNMENTS; i++)
    {
        // Skip assignment if it's invalid
        if (config.afc_assignments[i].fan < 0)
            continue;
        
        // Send fan number
        putch('0' + i + 1);
        
        // Send ROM code
        for (j = 0; j < 8; j++)
            printf("%02X", config.afc_assignments[i].serial[j]);
            
        // Send temperatures
        printf("%c%d%c%d", config.afc_assignments[i].min_temp >= 0 ? '+' : '-',
                           config.afc_assignments[i].min_temp, 
                           config.afc_assignments[i].max_temp >= 0 ? '+' : '-',
                           config.afc_assignments[i].max_temp);

        if (i < NUMBER_OF_FANS - 1)
            putch(':');
    }
    printf(";\r\n");
}     

/**
 * Writes the current configuration to the internal EEPROM.
 */
void save_settings()
{
    settings_save_to_eeprom();  
    printf(";;OK:P:0;\r\n"); 
}    

void set_fan_pwm_values()
{
    unsigned char fan, i, pwm;
    
    // Read fan number
    if (!read_fan_number(param_buf, 0, &fan))
    {
        printf(";;ERROR:Q:%d;\r\n", fan);
        return;
    }
    
    // Read PWM values
    for (i = 0; i < 4; i++)
    {
        if (!read_hex_byte(param_buf, i * 2 + 1, &pwm))
        {
            printf(";;ERROR:Q:%d;\r\n", fan);
            return;
        }    
        
        // Set the corresponding configuration parameter
        switch (i)
        {
            case 0: config.min_pwm_values[fan - 1] = pwm; break;
            case 1: config.max_pwm_values[fan - 1] = pwm; break;
            case 2: config.start_pwm_values[fan - 1] = pwm; break;
            case 3: config.stop_pwm_values[fan - 1] = pwm; break;
        }          
    }   
    
    printf(";;OK:Q:%d;\r\n", fan);
}      

void set_afc_state()
{
	char c;
    unsigned char fan_number;
 
    // Read fan number
    c = param_buf[0];
    if (!(c >= '1' && c <= NUMBER_OF_FANS + '0'))
    {
        printf(";;ERROR:S:%c;\r\n", c);
        return;
    }
    fan_number = c - '0';

    // Read state value
    c = param_buf[1];
    if (!(c >= '0' && c <= '1'))
    {
		printf(";;ERROR:S:%d;\r\n", fan_number);
		return;    
	} 
	
	config.afc_states[fan_number - 1] = c == '0' ? 0 : 1;
	
	printf(";;OK:S:%d;\r\n", fan_number);
}	

void get_afc_states()
{
	unsigned char i;
	
	printf(";;OK:T:0:");
	
	for (i = 0; i < NUMBER_OF_FANS; i++)
	{
		config.afc_states[i] == 1 ? putch('1') : putch('0');
		
		if (i < NUMBER_OF_FANS - 1)
			putch(':');
	}			
	
	printf(";\r\n");
} 

/**
 * Changes the default (start-up) PWM duty cycle of a specific fan.
 */
void set_fan_default_pwm()
{
    unsigned char fan, pwm;
    
    if (!read_fan_number(param_buf, 0, &fan) ||
            !read_hex_byte(param_buf, 1, &pwm))
    {
        printf(";;ERROR:U:%d;\r\n", fan);
        return;
    }  
    
    config.default_pwm_values[fan - 1] = pwm;
    
    printf(";;OK:U:%d;\r\n", fan); 
} 

void reset_device()
{
    printf(";;OK:Z:0;\r\n\r\n");
    asm("RESET");   
}       	

void send_usage()
{  
    printf("Hardware informations:\r\n");
    printf("    Firmware version: %s\r\n", VERSION_STRING);
    printf("    CPU clock frequency: %u MHz\r\n", _XTAL_FREQ / 1000000u);
    printf("    Maximum number of fans: %d\r\n", NUMBER_OF_FANS);
    printf("    Maximum number of temperature sensors: %d\r\n", MAX_TEMP_SENSORS);
    printf("    Maximum number of virtual temperature sensors: %d\r\n", MAX_VIRTUAL_TEMP_SENSORS);
    
    printf("\r\nAvailable command codes:\r\n");
    printf("    ?   - Show this help screen\r\n");
    printf("    #   - Reset the serial control interface\r\n");
    printf("    A   - Show version number\r\n");
    printf("    B   - Search 1-Wire temperature sensors\r\n");
    printf("    C   - Show 1-Wire temperature sensors' ROM codes\r\n");
    printf("    D   - Start temperature conversion on 1-Wire temperature sensors\r\n");
    printf("    E   - Set virtual sensor temperature\r\n"
           "          Parameters: ROM(16 HEX)SIGN(+|-)TEMP(4 INT)\r\n");
    printf("    F   - Show 1-Wire sensor temperature\r\n"
           "          Parameters: ROM(16 HEX)\r\n");
    printf("    G   - Calibrate all connected fans\r\n");
    printf("    H   - Calibrate a fan\r\n"
           "          Parameters: FAN(1 INT)\r\n");
    printf("    I   - Show fan speed (RPM)\r\n"
           "          Parameters: FAN(1 INT)\r\n");
    printf("    J   - Set fan PWM duty cycle\r\n"
           "          Parameters: FAN(1 INT)DUTY(2 HEX)\r\n");
    printf("    K   - Show fan PWM duty cycle\r\n"
           "          Parameters: FAN(1 INT)\r\n");
    printf("    L   - Assign a sensor to a fan\r\n"
           "          Parameters: FAN(1 INT)ROM(16 HEX)SIGN(+|-)MIN(4 INT)SIGN(+|-)MAX(4 INT)\r\n");
    printf("    M   - Remove sensor assignment\r\n"
           "          Parameters: FAN(1 INT)ROM(16 HEX)\r\n");
    printf("    N   - Show 1-Wire temperature sensor assignments\r\n");
    printf("    O   - Show virtual temperature sensor assignments\r\n");
    printf("    P   - Save settings to the EEPROM\r\n");
    printf("    Q   - Set a fan's PWM duty cycle values\r\n"
           "          Parameters: FAN(1 INT)MIN(2 HEX)MAX(2 HEX)START(2 HEX)STOP(2 HEX)\r\n");
    //printf("    R   - Set a fan's temperature values\r\n"
    //       "          Parameters: FAN(1 INT)SIGN(+|-)MIN(4 INT)SIGN(+|-)MAX(4 INT)\r\n");
    printf("    S   - Set a fan's Automatic Fan Control (AFC) state (enabled/disabled)\r\n"
           "          Parameters: FAN(1 INT)STATE(1|0)\r\n");
    printf("    T   - Show AFC states\r\n");
    printf("    U   - Set fan default PWM duty cycle value\r\n");
    printf("          Parameters: FAN(1 INT)DUTY(2 HEX)\r\n");
    printf("    Z   - Reset device\r\n");
    printf("\r\nCommand syntax for commands with parameters:\r\n"
           "    COMMAND_CODE:PARAMETERS;\r\n"
           "    For example: R:1-1000+2345;\r\n");
}	
