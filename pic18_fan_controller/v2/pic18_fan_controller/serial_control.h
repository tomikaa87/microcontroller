/**
 * Serial control module
 *
 * @file serial_control.h
 * @author Tamas Karpati
 * @date 2011-03-08
 */
 
#ifndef SERIAL_CONTROL_H
#define SERIAL_CONTROL_H

#include "hardware_profile.h";
#include "ds1820.h"

unsigned char sensor_count = 0;
rom_bits_t sensor_serials[MAX_TEMP_SENSORS];
signed int virtual_sensor_temps[MAX_VIRTUAL_TEMP_SENSORS];

/*** Public API ***********************************************************************************/

int serial_control_is_opcode(char c);
void serial_control_do(char opcode);


/*** Private functions ****************************************************************************/

unsigned char read_param_bytes(const unsigned char n);
unsigned char read_rom_bytes(const char *buf, 
                             const unsigned char from, 
                             rom_bytes_t rom_bytes);
unsigned char read_temperature(const char *buf,
                               const unsigned char from,
                               signed int *temp);
unsigned char read_hex_byte(const char *buf,
                            const unsigned char from,
                            unsigned char *byte);
unsigned char read_fan_number(char *buf,
                              const unsigned char from,
                              unsigned char *fan);

void send_version_number();
void search_one_wire_devices();
void send_one_wire_rom_codes();
void do_sensors_temp_conversion();
void send_sensor_temp();
void set_virtual_sensor_temp();
void calibrate_fans();
void calibrate_fan();
void read_fan_speed();
void set_fan_pwm();
void read_fan_pwm();
void assign_sensor();
void delete_sensor_assignment();
void send_sensor_assignments();
void save_settings();
void set_fan_pwm_values();
void set_afc_state();
void get_afc_states();
void set_fan_default_pwm();

void reset_device();
void send_usage();

#endif // SERIAL_CONTROL_H
