/**
 * Automatic Fan Control (AFC) module
 *
 * @file afc.h
 * @author Tamas Karpati
 * @date 2011-08-17
 */

#ifndef AFC_H
#define AFC_H

void afc_task();
unsigned char calculate_target_pwm(signed int temp, 
        signed int min_temp, signed int max_temp, unsigned char min_pwm,
        unsigned char max_pwm);

#endif
