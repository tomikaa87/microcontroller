/**
 * Settings storage module the uses PIC's internal EEPROM
 *
 * @file settings.c
 * @author Tamas Karpati
 * @date 2011-02-01
 */
 
#include "settings.h"
#include "hardware_profile.h"

//#include <peripheral/EEP.h>
#include <string.h> // memcpy()       

extern config_data_t config;

#ifdef DEBUG
#   include "usart.h"
#   include <stdio.h>  
#endif // DEBUG

/*** Internal EEPROM handling routines ************************************************************/

/**
 * Writes the given \p data byte to the EEPROM at the given \p addr address.
 */
void eep_write(unsigned int addr, unsigned char data)
{
    EEADRH = (addr >> 8) & 0x03;
	EEADR = (addr & 0x0FF);
	EEDATA = data;
	EEPGD = 0;
	CFGS = 0;
	WREN = 1;
	GIE = 0;
	EECON2 = 0x55;
	EECON2 = 0x0AA;
	EECON1bits.WR = 1;
	while (EECON1bits.WR);
	GIE = 1;
}

/**
 * Reads a byte from the EEPROM located at the given \p addr address.
 */
unsigned char eep_read(unsigned int addr)
{
    EEADRH = (addr >> 8) & 0x03;
	EEADR = (addr & 0x0FF);
  	CFGS = 0;
	EEPGD = 0;
	EECON1bits.RD = 1;

    return EEDATA;   
} 
/**************************************************************************************************/

/**
 * Reads the configuration data from the EEPROM
 */
void settings_load_from_eeprom()
{    
    unsigned int addr, idx;
    unsigned char *p_data, byte;
    
    p_data = (unsigned char *)(&config);
    
    #ifdef DEBUG
    printf("settings_load {\r\n");
    #endif
    
    // Check indicator bytes
    byte = eep_read(SETTINGS_STRUCT_EEPROM_ADDR);
    
    #ifdef DEBUG
    printf("  indicator = 0x%02X", byte);
    #endif
    
    idx = 0;
    if (byte != 0xFC)
    {
        #ifdef DEBUG
        printf("\r\n  indicator mismatch, loading defaults ");
        #endif
        
        memset(p_data, 0xFF, sizeof (config_data_t));
        p_data[0] = 0xFC;
        
        for (addr = SETTINGS_STRUCT_EEPROM_ADDR; addr < sizeof (config_data_t); addr++)
        {
            #ifdef DEBUG
            if (addr % 10 == 0)
                printf(".");
            #endif
            
            eep_write(addr, p_data[idx]);
            idx++;
        }       
    }  
    
    #ifdef DEBUG
    printf("\r\n  read: ");  
    #endif
    
    /*
     * Write the struct byte-by-byte
     */
    idx = 0;
    for (addr = SETTINGS_STRUCT_EEPROM_ADDR; addr < sizeof(config_data_t); addr++)
    {
        #ifdef DEBUG
        if (addr % 10 == 0)
            printf(".");
        #endif
            
        p_data[idx] = eep_read(addr);
        idx++;
    }
    
    // Verify
    #ifdef DEBUG
    printf("\r\n  verify: ");
    #endif
    
    idx = 0;
    for (addr = SETTINGS_STRUCT_EEPROM_ADDR; addr < sizeof (config_data_t); addr++)
    {
        #ifdef DEBUG
        if (addr % 10 == 0)
            printf(".");
        #endif
        
        if (eep_read(addr) != p_data[idx])
        {
            #ifdef DEBUG
            printf("\r\n  verify error at 0x%04X (eeprom: 0x%02X != config: 0x%02X)\r\n", 
                   addr, eep_read(addr), p_data[idx]);
            #endif
            
            break;   
        }   
        idx++;    
    }    
    #ifdef DEBUG
    printf("\r\n");
    #endif
    
    #ifdef DEBUG
    printf("  eeprom {");
    for (addr = SETTINGS_STRUCT_EEPROM_ADDR; addr < sizeof (config_data_t); addr++)
    {
        if (addr % 32 == 0)
            printf("\r\n    ");
        printf("%02X ", eep_read(addr));
    }    
    printf("\r\n  }\r\n");
    
    printf("\r\n}\r\n");
    #endif
    
    settings_sanity_check();
}
    
/**
 * Saves the current configuration data to EEPROM
 */
void settings_save_to_eeprom()
{ 
    unsigned int addr, idx;
    unsigned char *p_data;
    
    settings_sanity_check();
    
    p_data = (unsigned char *)(&config);
    
    #ifdef DEBUG
    printf("settings_save {\r\n");
    printf("  config {\r\n");
    printf("    size: %d (0x%04X)", sizeof (config_data_t), sizeof (config_data_t));    
    idx = 0;
    for (addr = 0; addr < sizeof (config_data_t); addr++)
    {
        if (addr % 32 == 0)
            printf("\r\n    ");
        printf("%02X ", p_data[idx++]);
    }    
    printf("\r\n  }\r\n");
    #endif
    
    /*
     * Read the struct byte-by-byte
     */
    idx = 0;
    for (addr = SETTINGS_STRUCT_EEPROM_ADDR; addr < sizeof(config_data_t); addr++)
    {
        #ifdef DEBUG
        printf("  addr: 0x%04X\r", addr);
        #endif
        
	    // Simple EEPROM wear leveling, write only the changed bytes
	    if (eep_read(addr) != p_data[idx])
	    {
    	    #ifdef DEBUG
        	printf("  write: 0x%02X -> 0x%04X\r\n", p_data[idx], addr);
        	#endif
        	
        	eep_write(addr, p_data[idx]);
        }   	
        idx++;
    }
    
    // Verify
    #ifdef DEBUG
    printf("  verify: ");
    idx = 0;
    for (addr = SETTINGS_STRUCT_EEPROM_ADDR; addr < sizeof (config_data_t); addr++)
    {
        if (addr % 10 == 0)
            printf(".");
        if (eep_read(addr) != p_data[idx])
        {
            printf("\r\n  verify error at 0x%04X (eeprom: 0x%02X != config: 0x%02X)\r\n", 
                   addr, eep_read(addr), p_data[idx]);
            break;   
        }    
        idx++;   
    }    
    printf("\r\n");
    
    printf("  eeprom {");
    for (addr = SETTINGS_STRUCT_EEPROM_ADDR; addr < sizeof (config_data_t); addr++)
    {
        if (addr % 32 == 0)
            printf("\r\n    ");
        printf("%02X ", eep_read(addr));
    }    
    printf("\r\n  }\r\n");
    
    printf("}\r\n");
    #endif
}    

/**
 * Checks some essential configuration values and corrects them if necessary.
 */
void settings_sanity_check()
{
    // Check AFC delta values
    if (config.afc_small_delta == 0 || config.afc_small_delta >= config.afc_big_delta ||
            config.afc_small_delta >= config.afc_huge_delta)
        config.afc_small_delta = AFC_DEFAULT_SMALL_DELTA;
        
    if (config.afc_big_delta == 0 || config.afc_big_delta <= config.afc_small_delta ||
            config.afc_big_delta >= config.afc_huge_delta)
        config.afc_big_delta = AFC_DEFAULT_BIG_DELTA;
        
    if (config.afc_huge_delta == 0 || config.afc_huge_delta <= config.afc_small_delta ||
            config.afc_huge_delta <= config.afc_big_delta)
        config.afc_huge_delta = AFC_DEFAULT_HUGE_DELTA;
}    

signed char settings_add_sensor_assignment(sensor_serial_t serial, 
                                           unsigned char fan,
                                           signed int min_temp,
                                           signed int max_temp)
{
    unsigned char i;
    
    #ifdef DEBUG
    printf("add_assign {\r\n");
    printf("  sensor: ");
    for (i = 0; i < sizeof (sensor_serial_t); i++)
        printf("%02X", serial[i]);
    printf(" -> fan: %d\r\n", fan);
    printf("  temp range: %d .. %d\r\n", min_temp, max_temp);
    #endif
    
    // Find an empty place in the assignment table, return -1 if no empty place was found
    i = 0;
    while (config.afc_assignments[i].fan != -1 && i < AFC_MAX_SENSOR_ASSIGNMENTS)
    {
        #ifdef DEBUG
        printf("  search index: %d\r", i++);
        #else
        i++;
        #endif
    }    
    if (i == AFC_MAX_SENSOR_ASSIGNMENTS)
    {
        #ifdef DEBUG
        printf("\r\n  assigment table full\r\n}\r\n");
        #endif
        
        return -1;
    }    
        
    // Insert the new assignment into the table, table row index is held by 'i'
    memcpy(config.afc_assignments[i].serial, serial, sizeof (sensor_serial_t));
    config.afc_assignments[i].fan = fan;
    config.afc_assignments[i].min_temp = min_temp;
    config.afc_assignments[i].max_temp = max_temp;
    
    #ifdef DEBUG
    printf("\r\n  assigment saved at %d\r\n}\r\n", i);
    #endif
    
    return i;
}
    
signed char settings_remove_sensor_assignment(sensor_serial_t serial, unsigned char fan)
{
    unsigned char i, j;
    signed char index_of_deleted;
    
    // This variable contains the index of the last deleted assignment
    index_of_deleted = -1;
    
    // Find the corresponding assignment in the assignments table
    for (i = 0; i < AFC_MAX_SENSOR_ASSIGNMENTS; i++)
    {
        // Skip the table entry if fan number doesn't match
        if (config.afc_assignments[i].fan != fan)
            continue;
            
        // If fan number matches, check the serial
        j = 0;
        while (serial[j] == config.afc_assignments[i].serial[j] && j < 8)
            j++;
        if (j == 8)
        {
            // Serial matches, replace fan number with '-1'
            config.afc_assignments[i].fan = -1;
            index_of_deleted = i;   
        }    
    }    
    
    return index_of_deleted;
}    
