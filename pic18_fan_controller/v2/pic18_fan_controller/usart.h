/**
 * Serial communications module
 *
 * @file usart.h
 * @author Tamas Karpati
 * @date 2011-01-26
 */

#ifndef USART_H
#define USART_H

#include "hardware_profile.h"

unsigned char usart_buf[USART_BUF_SIZE];
unsigned char usart_buf_next_in = 0;
unsigned char usart_buf_next_out = 0;

#define usart_buf_byte_ready (usart_buf_next_in != usart_buf_next_out)

void usart_setup();

void putch(unsigned char byte);
unsigned char getch(void);

void putch2(unsigned char byte);
unsigned char getch2(void);

unsigned char usart_buf_getch();

#endif // USART_H
