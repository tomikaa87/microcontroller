/**
 * Fan controller that uses fancontrol's algorithm
 *
 * @file fan_control.c
 * @author Tamas Karpati
 * @date 2011-02-01
 */

#include "fan_control.h"
#include "pwm.h"
#include "ds1820.h"
#include "hardware_profile.h"
#include "settings.h"

#include <stdio.h>

/*** Global variables *****************************************************************************/

extern signed int virtual_sensor_temps[MAX_VIRTUAL_TEMP_SENSORS];
extern config_data_t config;


/*** Public API functions *************************************************************************/

/**
 * The main task of the Fan Controller, needed to run as often as possible
 */
void fan_control_task()
{
    // Instruct all sensors to do temperature conversion
    ds1820_do_conversion();
    fan_control_do_fan_control(1);
}    

/**
 * This function initializes the fan controller subsystem
 */
void fan_control_init()
{
    pwm_setup_modules();
    pwm_setup_fan_speed_reading();   
}    

/**
 * This function prints the fan <-> sensor assignments on the serial console
 */
void fan_control_display_assignments()
{
    /*unsigned char i, j;
    
    for (i = 0; i < NUMBER_OF_FANS; i++)
    {
        printf("Fan #%d <-> Sensor S/N: ", i + 1);
        for (j = 0; j < 8; j++)
            printf("%02X ", config.fan_sensor_assign[i][j]);
        printf("\r\n");
    }*/    
}    

/**
 * This function prints the fan configurations on the serial console
 */
void fan_control_display_fan_config()
{
    unsigned char i;
    
    for (i = 0; i < NUMBER_OF_FANS; i++)
    {
        printf("Fan #%d\r\n", i + 1);
        printf("  Min PWM:   %d\r\n", config.min_pwm_values[i]);
        printf("  Max PWM:   %d\r\n", config.max_pwm_values[i]);
        printf("  Start PWM: %d\r\n", config.start_pwm_values[i]);
        printf("  Stop PWM:  %d\r\n", config.stop_pwm_values[i]);
        printf("\r\n");   
    }       
}    

/**
 * This function calibrates a fan and makes pwm <-> speed correlation
 *
 * @param fan_number The index of the fan
 */
unsigned char fan_control_calibrate_fan(unsigned char fan_number)
{
    unsigned char duty_cycle;
    unsigned char min_stop, min_start;
    unsigned int fan_rpm;
    
    if (fan_number < 1 || fan_number > NUMBER_OF_FANS)
        return 0;
    
    duty_cycle = 255;
    
    //printf("Calibrating fan #%d...\r\n", fan_number);
    
    // Give some time to fan to reach its maximum speed
    pwm_set_duty_cycle(fan_number, 255);
    delay_msec(3000);
    
    if (pwm_read_fan_speed(fan_number) == 0)
    {
        return 0;   
    }    
    
    //printf(" Finding min stop value\r\n");
    // Find min stop duty cycle
    do
    {
        // Change the fan's speed
        pwm_set_duty_cycle(fan_number, duty_cycle);
        
        // Wait for the fan to change its speed
        delay_msec(2000);
        
        // Read fan speed
        fan_rpm = pwm_read_fan_speed(fan_number);
        
        // Display current RPM
        //printf("  Fan speed at duty cycle %03d: %04u RPM\r\n", duty_cycle, fan_rpm);
        
        // Check if the fan is stopped
        if (fan_rpm == 0)
        {
            min_stop = duty_cycle;
            //printf("  Fan stopped at %03d\r\n", min_stop);
            break;
        }    
        
        if (duty_cycle >= 25)
            duty_cycle -= 10;
        else
            duty_cycle -= 1;
    }      
    while (duty_cycle < 255); // duty_cycle will underflow that indicates the end of the cycle
    
    duty_cycle = 0;
    min_start = 0;
    
    //printf(" Finding min start value\r\n");
    // Find min start duty cycle
    do
    {
        // Change the fan's speed
        pwm_set_duty_cycle(fan_number, duty_cycle);
        
        // Wait for the fan to change its speed
        delay_msec(2000);
        
        // Read fan speed
        fan_rpm = pwm_read_fan_speed(fan_number);
        
        // Display current RPM
        //printf("  Fan speed at duty cycle %03d: %04u RPM\r\n", duty_cycle, fan_rpm);
        
        // Check if the fan is stopped
        if (fan_rpm > 0)
        {
            min_start = duty_cycle;
            //printf("  Fan started at %03d\r\n", min_start);
        }
        
        duty_cycle++;
    }
    while (min_start == 0 && duty_cycle < 255); 
    
    config.max_pwm_values[fan_number - 1] = 255;
    config.min_pwm_values[fan_number - 1] = min_start;
    config.start_pwm_values[fan_number - 1] = min_start;
    config.stop_pwm_values[fan_number - 1] = min_stop;  
    
    //printf("Calibration finished.\r\n");
    
    return 1;
}    

/**
 * This function controls the given fan by its assigned temperature sensor
 *
 * @param fan_number The number of the fan (1..NUMBER_OF_FANS)
 */
void fan_control_do_fan_control(unsigned char fan_number)
{
    unsigned char min_pwm, max_pwm, current_pwm, start_pwm, stop_pwm;
    signed int sensor_temp, min_temp, max_temp, min_virtual_temp, max_virtual_temp;
    rom_bits_t sensor_serial_bits;
    unsigned char virtual_sensor;
    unsigned char i;
    signed int new_pwm;
    
    // Check if the fan number is valid
    if (fan_number < 1 || fan_number > NUMBER_OF_FANS)
        return;
        
    // Check Automatic Fan Control state
    if (config.afc_states[fan_number] == 0)
    	// AFC disabled
    	return;
        
    // Read fan PWM values from config
    min_pwm = config.min_pwm_values[fan_number - 1];
    max_pwm = config.max_pwm_values[fan_number - 1]; 
    start_pwm = config.start_pwm_values[fan_number - 1];
    stop_pwm = config.stop_pwm_values[fan_number - 1];
    
    // Read minimum and maximum temperature values from config
    /*min_temp = config.min_temps[fan_number - 1];
    max_temp = config.max_temps[fan_number - 1];
    min_virtual_temp = config.min_virtual_temps[fan_number - 1];
    max_virtual_temp = config.max_virtual_temps[fan_number - 1];*/
    
    // Read sensor serial from config and check if it's a virtual sensor or not
    /*ds1820_rom_bytes_to_rom_bits(config.fan_sensor_assign[fan_number - 1] , sensor_serial_bits);
    virtual_sensor = ds1820_virtual_sensor(sensor_serial_bits);*/

    // Read temperature from the specific (virtual) sensor
    if (virtual_sensor > 0)
        sensor_temp = virtual_sensor_temps[virtual_sensor - 1];
    else
        sensor_temp = ds1820_read_specific(sensor_serial_bits); 
     
    /*printf("Current temp: %05d\r\n", sensor_temp);
    printf("Min temp: %05d\r\n", min_temp);
    printf("Max temp: %05d\r\n", max_temp);
    printf("Min pwm: %03d\r\n", min_pwm);
    printf("Max pwm: %03d\r\n", max_pwm);
    printf("Start pwm: %03d\r\n", start_pwm);
    printf("Stop pwm: %03d\r\n", stop_pwm);*/
        
    /**** Fan Control Algorithm ****/
    
    // Check min and max PWM values
    if (min_pwm > max_pwm)
    {
    	pwm_set_duty_cycle(fan_number, 0xFF);
    	return;
    }
    
    // Check temperature settings
    if ((min_temp == max_temp && min_temp > 0) || min_temp > max_temp)
    {
		pwm_set_duty_cycle(fan_number, 0xFF);
		return;
    }
    
    if ((min_virtual_temp == max_virtual_temp && min_virtual_temp > 0) 
        || min_virtual_temp > max_virtual_temp)
    {
		pwm_set_duty_cycle(fan_number, 0xFF);
		return;
    }
    
    
}    

/**
 * This function calculates the PWM duty cycle value corresponding to the given percentage
 *
 * @param percent The fan speed percentage (0-100)
 * @param min_duty The minimum duty cycle
 * @param max_duty The maximum duty cycle
 * @return The calculated PWM duty cycle
 */
unsigned char fan_speed_percent_to_duty_cycle(signed char percent,
										      unsigned char min_duty,
										      unsigned char max_duty)
{
	unsigned int duty_square, duty_linear;
	unsigned char result_duty;

	// Check if the percentage value is between 0 and 100
	if (percent <= 0)
		return min_duty;
	if (percent >= 100)
		return max_duty;
		
	// Calculate fan duty cycle using a square function
	duty_square = (unsigned int)percent * (unsigned int)percent * 350 / 10000;
	
	// Calculate fan duty cycle using a linear function
	duty_linear = (unsigned int)percent * 255 / 100;
	
	// The smaller value will be the result
	if (duty_square > duty_linear)
		result_duty = duty_linear;
	else
		result_duty = duty_square;
		
	// Ensure that the result will be a value between [min_duty] and [max_duty]
	if (result_duty < min_duty)
		return min_duty;
	if (result_duty > max_duty)
		return max_duty;
		
	return result_duty;
}	
