/*
 * File:   terminal.h
 * Author: ToMikaa
 *
 * Created on 2012. szeptember 19., 17:20
 */

#include "terminal.h"
#include "usart.h"
#include "hardware_profile.h"
#include "settings.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*** Globals ******************************************************************/

static char buffer[128];
static char buffer_idx = 0;
#define BUF_IDX_MAX                     sizeof (buffer) - 1

bit flag_new_line = 0;

/*** Help text ****************************************************************/

static const char TXT_HELP[] =
    "\r\nFan Control Help\r\n\r\n" \
    "help                        - show this screen\r\n" \
    "version                     - display the firmware version number\r\n" \
    "sensor scan                 - scan the 1-wire bus for temperature sensors\r\n" \
    "sensor list                 - list found 1-wire temperature sensors\r\n" \
    "sensor read                 - read all temperature sensors\r\n" \
    "sensor read {nn}            - read sensor by its number\r\n" \
    "sensor read {serial}        - read sensor by its 64-bit unique serial\r\n" \
    "fan set min {fan} {pwm}     - set minimum PWM value of a fan\r\n" \
    "fan set max {fan] {pwm}     - set maximum PWM value of a fan\r\n" \
    "fan set start {fan} {pwm}   - set start PWM value of a fan\r\n" \
    "fan set stop {fan} {pwm}    - set stop PWM value of a fan\r\n" \
    "fan set default {fan} {pwm} - set default PWM value of a fan\r\n" \
    "fan read rpm {fan}          - read fan speed in RPM\r\n" \
    "fan read pwm {fan}          - read fan's current PWM value\r\n" \
    "fan calibrate               - calibrate all fans\r\n" \
    "fan calibrate {fan}         - calibrate a specific fan\r\n" \
    "afc assign {nn} {fan}       - assign a sensor given by its number to a fan\r\n" \
    "                              to be controlled automatically by AFC\r\n" \
    "afc assign {serial} {fan}   - assign a sensor given by its serial to a fan\r\n" \
    "afc show                    - show all assignments\r\n" \
    "\r\n\0";

/*** Forward declarations of private functions ********************************/

void reset_buffer();

void command_help();
void command_version();

void command_sensor_scan();
void command_sensor_list();
void command_sensor_read_all();
void command_sensor_read_by_num(char num);
void command_sensor_read_by_sn(char *serial);

void command_fan_set_min(char fan, unsigned char pwm);
void command_fan_set_max(char fan, unsigned char pwm);
void command_fan_set_start(char fan, unsigned char pwm);
void command_fan_set_stop(char fan, unsigned char pwm);
void command_fan_set_default(char fan, unsigned char pwm);
void command_fan_read_rpm(char fan);
void command_fan_read_pwm(char fan);
void command_fan_calibrate_all();
void command_fan_calibrate(char fan);

void command_afc_assign_by_num(char num, char fan);
void command_afc_assign_by_serial(char *serial, char fan);

void warn_arg_missing();
void warn_arg_unknown(char *arg);

/*** API functions ************************************************************/

void terminal_init()
{
    printf("\r\n");
    reset_buffer();
}

/**
 * Saves a character in the buffer.
 * @param c
 */
void terminal_char(char c)
{
    if (c == 8)
        return;

    // If buffer is full, automatically process it
    if (buffer_idx > BUF_IDX_MAX) {
        flag_new_line = 1;
        return;
    }

    // On new line character, immediately process the buffer
    if (c == '\n' || c == '\r') {
        printf("\r\n");
        if (buffer_idx == 0) {
            reset_buffer();
            return;
        }
        flag_new_line = 1;
        return;
    }

    // Save the character in the buffer
    buffer[buffer_idx] = c;
    buffer_idx++;
    putch(c);
}

/**
 * This function makes the terminal work, should be called frequently.
 */
void terminal_task()
{
    char *token, *tokens[10];
    char token_len[10], token_cnt, i;

    // Check if the buffer has a new line
    if (flag_new_line == 0)
        return;

    // Parse command line
    token_cnt = 0;
    token = strtok(buffer, " ");
    while (token) {
        tokens[token_cnt] = token;
        token_cnt++;
        token = strtok(NULL, " ");
    }

    // If the command line is empty, reset the buffer
    if (token_cnt == 0) {
        reset_buffer();
        return;
    }

    // Calculate length of tokens
    for (i = 0; i < token_cnt; i++)
        token_len[i] = strlen(tokens[i]);    

    // Command: help
    if (strncmp(tokens[0], "help", token_len[0]) == 0) {
        command_help();
    }
    // Command: version
    else if (strncmp(tokens[0], "version", token_len[0]) == 0) {
        command_version();
    }
    // Command: sensor
    else if (strncmp(tokens[0], "sensor", token_len[0]) == 0) {
        if (token_cnt == 1 || token_len[1] < 2) {
            warn_arg_missing();
        } else {
            // Argument: scan
            if (strncmp(tokens[1], "scan", token_len[1]) == 0) {
                command_sensor_scan();
            }
            // Argument: list
            else if (strncmp(tokens[1], "list", token_len[1]) == 0) {
                command_sensor_list();
            }
            // Argument: read
            else if (strncmp(tokens[1], "read", token_len[1]) == 0) {
                if (token_cnt == 2) {
                    // No argument -> read all sensors
                    command_sensor_read_all();
                } else {
                    // Argument found -> must be a sensor serial
                    if (token_len[2] != 16) {
                        if (token_len[2] <= 2) {
                            command_sensor_read_by_num(atoi(tokens[2]));
                        } else {
                            printf("Invalid sensor serial\r\n");
                        }
                    } else {
                        command_sensor_read_by_sn(tokens[2]);
                    }
                }
            }
            // Unknown argument
            else {
                warn_arg_unknown(tokens[1]);
            }
        }
    }
    // Command: fan
    else if (strncmp(tokens[0], "fan", token_len[0]) == 0) {

    }
    // Unknown command
    else {
        printf("No such command: %s. See 'help'.\r\n", tokens[0]);
    }

    reset_buffer();
}

/*** Private functions ********************************************************/

void reset_buffer()
{
    memset(buffer, 0, sizeof (buffer));
    buffer_idx = 0;
    flag_new_line = 0;
    printf("FanControl> ");
}

void command_help()
{
    char *s = TXT_HELP;
    while (*s)
        putch(*s++);
}

void command_version()
{
    printf(VERSION_STRING "\r\n");
}

void command_sensor_scan()
{
    printf("Scanning 1-Wire temperature sensors...\r\n");
}

void command_sensor_list()
{
    printf("Listing temperature sensors...\r\n");
}

void command_sensor_read_all()
{
    printf("Reading all sensors...\r\n");
}

void command_sensor_read_by_num(char num)
{
    printf("Reading sensor %d ...\r\n", num);
}

void command_sensor_read_by_sn(char *serial)
{
    printf("Reading sensor %s ...\r\n", serial);
}

void warn_arg_missing()
{
    printf("Argument is missing\r\n");
}

void warn_arg_unknown(char* arg)
{
    printf("Unknown argument: %s. See 'help'.\r\n");
}