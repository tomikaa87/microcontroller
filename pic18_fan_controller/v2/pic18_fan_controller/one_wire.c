/*
 * 1-Wire driver for on-board DS18X20 sensor
 *
 * @file one_wire.c
 * @author: Tamas Karpati
 * @date: 2010-01-26
 */
 
 #include "one_wire.h"
 #include "hardware_profile.h"

/**
 * Initializes the 1-wire bus
 * 
 * @return 0 if devices is connected, 1 if not connected or 2 on bus error
 */
unsigned char one_wire_reset()
{
    unsigned char presence, temp;
    
    OW_FLOAT();
    OW_LOW();
    delay_10us(60);
    OW_FLOAT();
    delay_10us(8);
    presence = ONE_WIRE_PIN;
    delay_10us(60);
    temp = ONE_WIRE_PIN;
    
    return (!temp ? 2 : presence);
}

/**
 * Writes a bit to the 1-wire bus
 *
 * @param b The bit to write (1 or 0)
 */
void one_wire_write_bit(unsigned char b)
{
    OW_FLOAT();
    OW_LOW();
    dly5u;
    if (b)
        OW_FLOAT();
    delay_10us(6);
    OW_HIGH();
    dly5u;    
}    

/**
 * Writes a data byte to the 1-wire bus
 *
 * @param b The byte to write
 */
void one_wire_write_byte(unsigned char b)
{
    unsigned char i = 8;
    
    while (i--)
    {
        one_wire_write_bit(b & 0x01);
        b >>= 1;   
    }    
}

/**
 * Reads a bit from the 1-wire bus
 *
 * @return The read bit (1 or 0)
 */
unsigned char one_wire_read_bit()
{
    unsigned char data;
    
    OW_FLOAT();
    OW_LOW();
    dly10u;
    OW_FLOAT();
    dly10u;
    data = ONE_WIRE_PIN;
    dly20u;
    dly20u;
    
    return data;   
}    

/**
 * Reads a data byte from the 1-wire bus
 *
 * @param The read data byte
 */
unsigned char one_wire_read_byte()
{
    unsigned char data, i;
    
    data = 0;
    for (i = 0; i < 8; i++)
    {
        if (one_wire_read_bit())
            data |= (0x01 << i);  
    }   
    
    return data; 
} 
