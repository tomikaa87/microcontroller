#ifndef PERIPHERAL_H
#define PERIPHERAL_H

/*volatile unsigned char CMCON @ 0x9C;
volatile unsigned char CVRCON @ 0x9D;*/

#define _XTAL_FREQ 64000000L

void setup_usart();
void putch(unsigned char byte); 
unsigned char getch(void);

#endif // PERIPHERAL_H
