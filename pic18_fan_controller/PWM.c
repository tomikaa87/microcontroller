#include "PWM.h"
#include "Interrupt.h"
#include "HardwareProfile.h"

#include <htc.h>
#include <stdio.h>

/*** Setup functions ******************************************************************************/

void PWM_SetupModules()
{
    #ifdef DEBUG
    printf("[PWM] Setting up modules...\r\n");
    #endif
    
    // Setup Timer2 for 251 kHz PWM operation
    PR2 = 0b00111111;
    T2CON = 0b00000101;
    #ifdef DEBUG
    printf("[PWM] Timer2 registers: PR2 = 0x%02X, T2CON = 0x%02X\r\n", PR2, T2CON);
    #endif
    
    // Setup PWM 1
    #ifdef DEBUG
    printf("[PWM] Setting up PWM 1...\r\n");
    #endif
    FAN_1_PWM_PIN_TRIS = 0;   
    CCP1CON = 0b00001100;
    CCPR1L = 0;    
}    

void PWM_SetupFanSpeedReading()
{
    #ifdef DEBUG
    printf("[PWM] Setting up fan speed reading...\r\n");
    #endif
    
    //Timer1 Registers Prescaler= 4 - TMR1 Preset = 61536 - Freq = 4000.00 Hz - Period = 0.000250 seconds
    T1CKPS1 = 0;   // bits 5-4  Prescaler Rate Select bits
    T1CKPS0 = 0;   // bit 4
    T1OSCEN = 1;   // bit 3 Timer1 Oscillator Enable Control bit 1 = on
    T1SYNC = 1;    // bit 2 Timer1 External Clock Input Synchronization Control bit...1 = Do not synchronize external clock input
    TMR1CS1 = 0;   // bit 1 Timer1 Clock Source Select bit...00 = Internal clock (FOSC/4)
    TMR1CS0 = 0;
    TMR1ON = 0;    // bit 0 enables timer
    TMR1H = 240;   // preset for timer1 MSB register
    TMR1L = 96;    // preset for timer1 LSB register
    TMR1IE = 1;
    GIE = 1;
    PEIE = 1;
    
    #ifdef DEBUG
    printf("[PWM] Timer1 registers: T1CON = 0x%02X, TMR1L = 0x%02X, TMR1H = 0x%02X\r\n",
           T1CON, TMR1L, TMR1H);
    #endif
    
    #ifdef DEBUG
    printf("[PWM] Setting up tacho input pins...\r\n");
    #endif
    FAN_1_TACHO_PIN = 1;     // FAN 1 tacho input pin
    
    TRISA1 = 0;
}    

/*** Duty cycle handling **************************************************************************/

void PWM_SetDutyCycle(unsigned char moduleNumber,
                      unsigned char dutyCycle)
{
    unsigned char ccprl, dcb0, dcb1;
    
    #ifdef DEBUG
    printf("[PWM] Changing PWM %d duty cycle to 0x%02X...\r\n", moduleNumber, dutyCycle);
    #endif
    
    // Calculate register values from dutyCycle
    ccprl = dutyCycle >> 2;
    dcb1 = dutyCycle & 0x02 ? 1 : 0;
    dcb0 = dutyCycle & 0x01;
    
    #ifdef DEBUG
    printf("[PWM] CCPRxL = 0x%02X, DCxB0 = %d, DCxB1 = %d\r\n", ccprl, dcb0, dcb1);
    #endif
    
    switch (moduleNumber)
    {
        case 1:
        {
            CCPR1L = ccprl;
            DC1B1 = dcb1;
            DC1B0 = dcb0;
            break;
        }    
        
        default:
            break;   
    }       
}

unsigned char PWM_GetDutyCycle(unsigned char moduleNumber)
{
    unsigned char ccprl, dcb0, dcb1, dutyCycle;
    
    switch (moduleNumber)
    {
        case 1:
        {
            ccprl = CCPR1L;
            dcb0 = DC1B0;
            dcb1 = DC1B1;
            break;
        }    
        
        default:
            break;   
    }    
    
    dutyCycle = ccprl << 2;
    dutyCycle += dcb0;
    dutyCycle += (dcb1 << 1);
    
    return dutyCycle;
}                              

/*** Fan controller functions *********************************************************************/

unsigned char PWM_ReadPin()
{
    unsigned char result;
    
    //CCP1CON &= 0b11110000; // Turn off PWM
    //PORTCbits.RC2 = 1;
    PORTAbits.RA1 = 1;
    result = PORTCbits.RC3;
    PORTAbits.RA1 = 0;
    //CCP1CON |= 0b00001100; // Turn on PWM
    
    return result;
}  

#define PWM_WAIT() while (!PORTCbits.RC2) continue;

unsigned int PWM_ReadFanSpeed(unsigned char fanNumber)
{
    unsigned char counter;
    unsigned int maxTimerValue;
    
    counter = 0;
    maxTimerValue = 0;
    
    while (counter < 3)
    {
        TMR1ON = 0;
        TMR1H = 240;
        TMR1L = 96;
        
        PORTAbits.RA1 = 0;
        
        timer1Value = 0;
        TMR1IF = 0;
        TMR1ON = 1;
        while (!PORTCbits.RC3 && timer1Value < 600) continue;
        while (PORTCbits.RC3 && timer1Value < 600) continue;
        if (timer1Value >= 600)
        {
            TMR1ON = 0;
            timer1Value = 0;
            return 0;
        }
        
        timer1Value = 0;
    
        while (timer1Value < 6)
        {
            timer1Value = 0;
            TMR1IF = 0;
            TMR1ON = 1;
            
            //            _
            // Wait for _|
            while (!PORTCbits.RC3 && timer1Value < 600) continue;
            //while (!PORTCbits.RC3 && timer1Value < 600) continue;
    
            // Check if the fan is spining
            if (timer1Value >= 600)
            {
                TMR1ON = 0;
                timer1Value = 0;
                break;   
            }
    
            PORTAbits.RA1 = 1;
            TMR0ON = 0;
            timer1Value = 0;
            TMR1IF = 0;
            TMR1ON = 1;
            
            //          _
            // Wait for  |_
            //while (!PORTCbits.RC3) continue;
            while (PORTCbits.RC3) continue;
            
            TMR1ON = 0;
            PORTAbits.RA1 = 0;
        }   
        
        if (timer1Value > maxTimerValue)
            maxTimerValue = timer1Value;
            
        counter++;
    }     

    /*switch (fanNumber)
    {
        case 1:
        {
            failCounter = 40000;
            
            //CCP1CON &= 0b11110000; // Turn off PWM
            //PORTCbits.RC2 = 1;
            
            // Wait for first falling edge
            PWM_WAIT();
            while (!PORTCbits.RC3 && --failCounter > 0) 
                PWM_WAIT();
                
            if (failCounter == 0)
            {
                timer1Value = 0;
                break;
            }        
            
            // Wait for first rising edge
            failCounter = 40000;
            PWM_WAIT();
            while (PORTCbits.RC3 && --failCounter > 0)
                PWM_WAIT();
            
            // Turn on Timer1
            TMR1ON = 1;
            
            // Wait for second rising edge
            failCounter = 40000;
            PWM_WAIT();
            while (!PORTCbits.RC3 && --failCounter > 0) 
                PWM_WAIT();
                
            // Wait for second falling edge
            failCounter = 40000;
            PWM_WAIT();
            while (PORTCbits.RC3 && --failCounter > 0) 
                PWM_WAIT();
            
            TMR1ON = 0;
            //CCP1CON |= 0b00001100; // Turn on PWM
            
            if (failCounter == 0)
                timer1Value = 0;
            
            break; 
        }       
        
        default:
            break;
    }*/

    #ifdef DEBUG
    printf("[PWM] Timer1 counter = %u\r\n", maxTimerValue);
    #endif
    
    if (timer1Value > 0)
        return (unsigned int)(60000 / maxTimerValue);   
    else
        return 0;
}    
