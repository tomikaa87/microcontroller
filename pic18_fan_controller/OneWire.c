/*
 * Weather Station Ethernet Board
 * 1-Wire driver for on-board DS18X20 sensor
 *
 * Author: Tamas Karpati
 * Date: 2010-09-15
 */
 
 #include "OneWire.h"
 #include "HardwareProfile.h"

/*
 * Initialize 1-wire bus
 * Return value:
 *   0 = Device connected
 *   1 = Device not connected
 *   2 = Bus short circuit
 */
unsigned char OneWireReset()
{
    unsigned char presence, temp;
    
    OW_FLOAT();
    OW_LOW();
    Delay10us(60);
    OW_FLOAT();
    Delay10us(8);
    presence = ONE_WIRE_PIN;
    Delay10us(60);
    temp = ONE_WIRE_PIN;
    
    return (!temp ? 2 : presence);
}

/*
 * Writes a bit to the 1-wire bus
 */
void OneWireWriteBit(unsigned char b)
{
    OW_FLOAT();
    OW_LOW();
    dly5u;
    if (b)
        OW_FLOAT();
    Delay10us(6);
    OW_HIGH();
    dly5u;    
}    

/*
 * Write a data byte to the 1-wire bus
 */
void OneWireWriteByte(unsigned char b)
{
    unsigned char i = 8;
    
    while (i--)
    {
        OneWireWriteBit(b & 0x01);
        b >>= 1;   
    }    
}

/*
 * Reads a bit from the 1-wire bus
 */
unsigned char OneWireReadBit()
{
    unsigned char data;
    
    OW_FLOAT();
    OW_LOW();
    dly10u;
    OW_FLOAT();
    dly10u;
    data = ONE_WIRE_PIN;
    dly20u;
    dly20u;
    
    return data;   
}    

/*
 * Reads a data byte from the 1-wire bus
 */
unsigned char OneWireReadByte()
{
    unsigned char data, i;
    
    data = 0;
    for (i = 0; i < 8; i++)
    {
        if (OneWireReadBit())
            data |= (0x01 << i);  
    }   
    
    return data; 
} 
