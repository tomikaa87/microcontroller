#include "peripheral.h"

#include <htc.h> 

/*
 * Sends a byte trough USART
 */
void putch(unsigned char byte) 
{
    // Wait for TX to become ready
	while(!TRMT1)
		continue;
		
    if (byte == 0x0A || byte == 0x0D)
    {
        unsigned long i;
        
        for (i = 0; i < 100; i++)
        __delay_ms(1);    
    }    
		
	// Write the byte
	TXREG1 = byte;
}

/*
 * Reads a byte from the USART buffer register
 */
unsigned char getch(void)
{
    // Wait for incoming data
    while (!RC1IF)
        continue;
    // Read it out from the buffer
    return RCREG1;
} 
