#include "Usart.h"
#include "HardwareProfile.h"

#include <htc.h>

/**
 * This function configures the USART modules of the CPU
 */
void setupUsart()
{
    // Setup USART module #1
    TX91 = 0;
    SYNC1 = 0;
    BRGH1 = 0;
    SPBRG1 = 103;
    SPBRGH1 = 0;
    SPEN1 = 1;
    CREN1 = 1;
    TXEN1 = 1;
    TRISC7 = 1;
    TRISC6 = 0; 
    
    // Setup USART module #2
    TX92 = 0;
    SYNC2 = 0;
    BRGH2 = 0;
    SPBRG2 = 103;
    SPBRGH2 = 0;
    SPEN2 = 1;
    TXEN2 = 1;
    TRISB7 = 1;
    TRISB6 = 0;  
} 

/**
 * Sends a byte trough USART
 *
 * @param byte The byte to send
 */
void putch(unsigned char byte) 
{
    // Wait for TX to become ready
	while(!TRMT1)
		continue;
		
	// Write the byte
	TXREG1 = byte;
}   

/**
 * Sends a byte trough USART
 *
 * @param byte The byte to send
 */
void putch2(unsigned char byte) 
{
    // Wait for TX to become ready
	while(!TRMT2)
		continue;
		
    if (byte == 0x0A || byte == 0x0D)
    {
        unsigned long i;
        
        for (i = 0; i < 100; i++)
        __delay_ms(1);    
    }    
		
	// Write the byte
	TXREG2 = byte;
}

/**
 * Reads a byte from the USART buffer register
 */
unsigned char getch(void)
{
    // Wait for incoming data
    while (!RC1IF)
        continue;
    // Read it out from the buffer
    return RCREG1;
}

/**
 * Reads a byte from the USART buffer register
 */
unsigned char getch2(void)
{
    // Wait for incoming data
    while (!RC2IF)
        continue;
    // Read it out from the buffer
    return RCREG2;
}
