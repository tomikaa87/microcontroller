#ifndef __HARDWARE_PROFILE_H
#define __HARDWARE_PROFILE_H

#include <htc.h>

#define _XTAL_FREQ          64000000ul

#define ONE_WIRE_PIN            (PORTAbits.RA0)
#define ONE_WIRE_TRIS           (TRISA0)

#define FAN_1_PWM_PIN_TRIS      (TRISC2)
#define FAN_1_TACHO_PIN         (PORTCbits.RC3)

// Delay routines
// PIC_CLK = 64MHz
#define dly125n asm("nop"); asm("nop");
#define dly500n dly125n;dly125n;dly125n;dly125n
#define dly1u	dly500n;dly500n
#define dly2u	dly1u;dly1u
#define dly3u	dly2u;dly1u
#define dly4u	dly2u;dly2u
#define dly5u	dly2u;dly2u;dly1u
#define dly9u   dly4u;dly5u
#define dly10u  dly5u;dly5u
#define dly20u  dly10u;dly10u
#define dly50u  dly10u;dly10u;dly10u;dly10u;dly10u

#define Delay10us(x) unsigned int _dcnt = x; while (_dcnt) { dly9u; dly500n; _dcnt--; }

#define DelayMsec(x) \
    do \
    { \
        unsigned long _dcnt; \
        _dcnt = x * ((unsigned long)(0.0001/(1.0/_XTAL_FREQ)/6)) - 100; \
        while (_dcnt--); \
    } while (0)    

#endif // __HARDWARE_PROFILE_H


