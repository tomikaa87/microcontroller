#include "Interrupt.h"

#include <htc.h>

void interrupt isr(void)
{
    if (TMR1IF && TMR1IE)
    {
        timer1Value++;

        TMR1H = 240;
        TMR1L = 96;
        TMR1IF = 0;
    }
}    
