#include <htc.h>
#include <stdio.h>

#include "HardwareProfile.h"
#include "SerialLCDDrv.h"
#include "Usart.h"
#include "DS18X20.h"
#include "PWM.h"
#include "Interrupt.h"

#define PROGRAM_VERSION "0.1"
#define MAX_SENSORS_COUNT   8

/*** Global variables *****************************************************************************/

signed int temperature = 0;
ROMBits deviceRomBits[MAX_SENSORS_COUNT];
ROMBytes deviceRomBytes[MAX_SENSORS_COUNT];
unsigned char deviceCount = 0;

/*** CPU configuration ****************************************************************************/
__CONFIG(1, 
         FOSC_INTIO67);         // Internal OSC block, RA6 and RA7 are I/Os
__CONFIG(2, 
         PWRTEN_ON &            // Power-up timer on
         BORV_285 &             // Brown-out reset at 2.85 Volts
         WDTEN_OFF);            // Watchdog off
__CONFIG(3, 
         PBADEN_OFF);           // PORTB<5:0> are digital I/Os
__CONFIG(4, 
         LVP_OFF);              // Low voltage programming off
__CONFIG(5,
         CP0_OFF &              // Code protection block 0 off
         CPB_OFF);              // Boot block code protection off
__CONFIG(6, 
         WRT0_OFF &             // Write protection block 0 off
         WRTC_OFF);             // Configuration register write protection off
__CONFIG(7,
         EBTR0_OFF &            // Table read protection block 0 off
         EBTRB_OFF);            // Boot block table read protection off
         
/*** Main *****************************************************************************************/
void main()
{ 
    unsigned char i;
    
    // Setup OSC
    OSCCON = 0b01110000;        // Internal OSC block @16 MHz
    PLLEN = 1;                  // Enable x4 PLL -> 64 MHz
    
    // Disable ADC
    ADCON0 = 0;
    ADCON2 = 0;
    
    // Make analog ports digital
    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;

    // Disable unused peripherals
    MSSP1MD = 1;
    MSSP2MD = 1;
    CTMUMD = 1;
    CMP2MD = 1;
    CMP1MD = 1;
    ADCMD = 1;
    
    TRISB = 0xFF;
    
    setupUsart();
    DelayMsec(500);
    
    printf("\r\n**********************\r\n");
    printf("* PIC Fan Controller *\r\n");
    printf("*--------------------*\r\n");
    printf("* Version: %s       *\r\n", PROGRAM_VERSION);
    printf("**********************\r\n\r\n");
    
    printf("Searching devices...\r\n");
    deviceCount = DS18X20_SearchDevices(deviceRomBits, MAX_SENSORS_COUNT);
    printf("Found devices: %d\r\n", deviceCount);
    
    printf("Setting up PWM modules...\r\n");
    PWM_SetupModules();
    
    printf("Setting up fan speed reading...\r\n");
    PWM_SetupFanSpeedReading();
    
    printf("Changing PWM1 duty cycle...\r\n");
    PWM_SetDutyCycle(1, 0x80);

    while (1)
    {
        printf("Temperatures: "); 
        for (i = 0; i < deviceCount; i++)
        {
            temperature = DS18X20_ReadSpecific(deviceRomBits[i]);
            printf("%d.%02d C  ", temperature / 100, temperature % 100);   
        }  
        printf("\r\n");
        
        printf("Fan 1 speed: %u RPM\r\n", PWM_ReadFanSpeed(1));
        
        if (RC1IF)
            PWM_SetDutyCycle(1, RCREG1); 
        
        //PWM_SetDutyCycle(1, getch()); 
      
        DelayMsec(1000);
    }    
}    
