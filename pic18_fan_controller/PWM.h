#ifndef PWM_H
#define PWM_H

#define DEBUG

void PWM_SetupModules();
void PWM_SetupFanSpeedReading();

void PWM_SetDutyCycle(unsigned char moduleNumber,
                      unsigned char dutyCycle);
unsigned char PWM_GetDutyCycle(unsigned char moduleNumber);
                      
unsigned int PWM_ReadFanSpeed(unsigned char fanNumber);
                      

#endif PWM_H
