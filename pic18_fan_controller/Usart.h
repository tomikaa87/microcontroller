#ifndef USART_H
#define USART_H

void setupUsart();

void putch(unsigned char byte);
unsigned char getch(void);

void putch2(unsigned char byte);
unsigned char getch2(void);

#endif // USART_H
